#ifndef UTILS_H_
#define UTILS_H_

#include <OpenSim/OpenSim.h>

#define STATE_UPD_STEPSIZE 0.001

// added by JW

std::string convertInt(int number);
/*
void writeVarToFile(std::string const filename, double data);
void writeVarToFile(std::string const filename, std::vector<double> data);
void writeDoubleToFile(std::string const name, double& data);
void writeDoubleToFile(std::string const name, int& data);
void writeDoubleToFile(std::string const name, bool& data);

void writeVectorToFile(std::string const name, std::vector<double>* const data_object, int const length);
void writeVectorToFile(std::string const name, std::vector<double> const data_object);
void writeVectorToFile(std::string const name, double* const data_object, int const length);

void readVectorFile(std::string const filename, std::vector<double> &output);
void readDoubleFromFile(std::string const name, double &output);
void readDoubleFromFile(std::string const name, int &output);
void readDoubleFromFile(std::string const name, bool &output);
void readVarFromFile(std::string const name, std::vector<double> output[], int const length);
void readVarFromFile(std::string const name, double output[], int const length);
void readVarFromFile(std::string const name, std::vector<double> &output);

// testing
void readVectorArray(std::string const name, std::vector<double> output[], int const length);
void readVector(std::string const name, std::vector<double> &output);
*/


// Original
void updateParams(const OpenSim::Array<double>& input,
	std::string ctrlPrefix, OpenSim::Array<double>& output);

void convertFromOptInput( const SimTK::Vector& input, 
	std::string ctrlPrefix, OpenSim::Array<double>& output ); 

void convertToOptInput( const OpenSim::Array<double>& input, 
	std::string ctrlPrefix, 
	SimTK::Vector& output, 
	SimTK::Vector& lower_bound, 
	SimTK::Vector& upper_bound
	 ); 

const static SimTK::Vec3 UnitX(1.0, 0.0, 0.0); 
const static SimTK::Vec3 UnitY(0.0, 1.0, 0.0); 
const static SimTK::Vec3 UnitZ(0.0, 0.0, 1.0);

void getUpperBodyCOM( const OpenSim::Model& model, const SimTK::State& s, SimTK::Vec3& result ); 
void getSagCorNormals( const OpenSim::Model& model, const SimTK::State& s, 
		SimTK::Vec3& sagN, SimTK::Vec3& corN ); 
void getUpVectorInGround( const OpenSim::Model& model, const SimTK::State& s, 
	const OpenSim::Body& b, SimTK::Vec3& result ); 
void getFrontVectorInGround(const OpenSim::Model& model, const SimTK::State& s,
	 const OpenSim::Body& b, SimTK::Vec3& result );

void setControl(const OpenSim::Actuator* act, double val, SimTK::Vector& controls);  
double getPreviousDouble( const std::vector<double>* queuePtr, double delta_t ); 

void getPreviousDouble( const std::vector<double>* queuePtr, double delta_t, 
		double& theta, double& thetav ); 

// this transforms the convension used in Humanoid2D to one assumed for the 
// SimpleMuscle model
void getTransformedCoordValue( const SimTK::State& s, 
	const OpenSim::Coordinate* coord, double& val, double& speedVal ); 

#endif // UTILS_H_
