#ifndef EVENTHANDLERS_H_
#define EVENTHANDLERS_H_

#include <OpenSim/OpenSim.h>
#include "GeyerHerrController.h"

//==============================================================================
// EVENT HANDLER: UserInputHandler
// Check for user input. If there has been some, process it.
//==============================================================================
class UserInputHandler : public SimTK::PeriodicEventHandler {
public:
    UserInputHandler(SimTK::Visualizer::InputSilo& silo, OpenSim::BodyWatcher& bw, SimTK::Real interval); 

    void handleEvent(SimTK::State& s, SimTK::Real accuracy, bool& shouldTerminate) const;
private:
    SimTK::Visualizer::InputSilo& _silo;
    OpenSim::BodyWatcher& _bodyWatcher;
};

namespace OpenSim {
class ControlStateHandler : public SimTK::PeriodicEventHandler {
public:
    ControlStateHandler(Model& m, SimTK::Real interval);

    void handleEvent(SimTK::State& s, SimTK::Real accuracy, bool& shouldTerminate) const;
private:
    Model& _model;
};
}

#endif // EVENTHANDLERS_H_
