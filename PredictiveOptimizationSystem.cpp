#include "PredictiveOptimizationSystem.h"
#include "GeyerHerrController.h"
#include "EventHandlers.h"
#include "Utils.h"
#include "SimpleMuscle.h"
#include "SoftCoordinateLimitForce.h"
#include <cmath>
#include <vector>
#include <algorithm>
// Not sure why I need to do this but I can't access the function otherwise
#include <OpenSim/Common/PiecewiseConstantFunction.h> 

// Todo:
//		If possible Load modelFile and controls only once and reuse.


#define USE_GEYER_HIP_MOMARM
#define MAX_DEPTH 0.01
//#define PRINTMUSCLEINFO
#define MUSCLE_CURVE_PATH "muscleCurves/"
#define TARGET_VELOCITY_PENALTY_WEIGHT 0.01
#define COORDINATE_LIMIT_PENALTY_WEIGHT 0.005
#define GRF_IMPULSE_PENALTY_WEIGHT 0.000000000
#define HEAD_VELOCITY_WEIGHT 0.005
#define TORSO_ORIENTATION_PENALTY_WEIGHT 0.1    // not used in calculation


#define USE_MTU
using namespace OpenSim;
using namespace SimTK;
using namespace std;

// Used to Save Controller State to File
bool foundTime = false;
int optRuns = 0;

// Used for Supraspinal Ctrl
int changeAt = 2;
int didChange = 0;
Storage walkSettings1;
Storage walkSettings2;
Storage walkSettings3;
Storage walkSettings4;


SimTK::Visualizer* myVisualizer;

double Q(double d, double eps) {
	if (fabs(d) < eps) 
		return 0; 
	else 
		return d*d; 
}


// Performs simulation and evaulation of the simulated outcome
// Params are normalized controls from -cf
int PredictiveOptimizationSystem::objectiveFunc(const SimTK::Vector& params,
	bool reporting, SimTK::Real& f) const {
	bool useViz = _visualize;

	// Counting how many optimizations has been run so far (used to generate unique filename for save_all_sim
	optRuns++;

	// Loading modelFile for every simulation. Can this be uptimized?
	Model model(_modelFile);

#ifdef USE_MTU
	//addAllMuscles(model); 
#endif

	//std::cout << "OptAccel: " << _optAccel << endl;
	//std::cout << "OptDelay: " << _optDelay << endl;

	// This Storage shouldn't change during sim. Maybe can be moved outside for single init pr. core?
	// Using [prefix]_values.sto to initialize size of allSimParams array (77 actual values, 400 entries)
	Storage allCtrls(_ctrlPrefix + "_values.sto");
	Array<double>& allSimParams = allCtrls.getLastStateVector()->getData();
	// Updating allSimParams by overwriting activeParameters from params - ctrlFile (70 active params - what are the other 7?)
	// Only copying over activeParams - this could be why I can't set the full state
	convertFromOptInput(params, _ctrlPrefix, allSimParams);

	//std::cout << "Size of params: " << params.size() << ", allSimParams: " << allSimParams.getSize() << endl;

	model.setUseVisualizer(useViz);

	// Override the backpack load if necessary
	if (_backpackLoad > 0) {
		model.updBodySet().get("backpack").setMass(_backpackLoad);
		//cout << "Set backpack load to " << _backpackLoad << " kg." << endl;
	}

	// Reading HuntCrossley data from [_ctrlPredix]_values.sto and setting them in the model object
	// Does this need to be done every time objectiveFunc is called?
	if (typeid(model.getForceSet()[0]) == typeid(OpenSim::HuntCrossleyForce)) {
		OpenSim::HuntCrossleyForce* hcf[2];
		hcf[0] =
			static_cast<OpenSim::HuntCrossleyForce*>(&model.getForceSet()[0]);
		hcf[1] =
			static_cast<OpenSim::HuntCrossleyForce*>(&model.getForceSet()[1]);

		for (int i = 0; i < 2; i++) {
			int stIdx = allCtrls.getStateIndex("stiffness");
			if (stIdx >= 0)
				hcf[i]->setStiffness(allSimParams.get(stIdx));
			stIdx = allCtrls.getStateIndex("dissipation");
			if (stIdx >= 0)
				hcf[i]->setDissipation(allSimParams.get(stIdx));
			stIdx = allCtrls.getStateIndex("static_friction");
			if (stIdx >= 0) {
				double static_fric = allSimParams.get(stIdx);
				hcf[i]->setStaticFriction(static_fric);
				stIdx = allCtrls.getStateIndex("dynamic_friction_mult");
				if (stIdx >= 0)
					hcf[i]->setDynamicFriction(static_fric*allSimParams.get(stIdx));
				else {
					stIdx = allCtrls.getStateIndex("dynamic_friction");
					if (stIdx >= 0)
						hcf[i]->setDynamicFriction(allSimParams.get(stIdx));
				}
			}

			stIdx = allCtrls.getStateIndex("viscous_friction");
			if (stIdx >= 0)
				hcf[i]->setViscousFriction(allSimParams.get(stIdx));
			stIdx = allCtrls.getStateIndex("transition_velocity");
			if (stIdx >= 0)
				hcf[i]->setTransitionVelocity(allSimParams.get(stIdx));
		}
	}


	SimTK::State& init_s = model.initSystem();
	// All active states right now. Can probably be set after this point
	//cout << "All active states right now: " << endl << model.getStateVariableNames() << endl;
	SimTK::MultibodySystem& mbs = model.updMultibodySystem();
	SimTK::SimbodyMatterSubsystem& matter = model.updMatterSubsystem();
	SimTK::GeneralForceSubsystem& forces = model.updForceSubsystem();


	double CaptureVelocity = 0.001; // What's this?


	// Choosing integrator type

	//SimTK::ExplicitEulerIntegrator integrator(mbs);
	SimTK::SemiExplicitEuler2Integrator integrator(mbs);
	//SimTK::SemiExplicitEulerIntegrator integrator(mbs, 0.0001);
	//SimTK::RungeKuttaMersonIntegrator integrator(mbs);
	//SimTK::CPodesIntegrator integrator(mbs);
	//integrator.setOrderLimit(2); 

	SimTK::TimeStepper ts(mbs, integrator);


	if (useViz) {
		const BodySet& bodySet = model.getBodySet();
		const Body& pelvis = bodySet.get("pelvis");
		SimTK::Visualizer& viz = model.updVisualizer().updSimbodyVisualizer();
		viz.setWindowTitle("Predictive Simulation");
		viz.setBackgroundType(viz.GroundAndSky);
		viz.setGroundHeight(0.0);
		viz.setShowShadows(true);
		viz.setShowFrameRate(true);
		viz.setShowSimTime(true);
		viz.setShowFrameNumber(false);
		OpenSim::BodyWatcher* bodyWatch = new BodyWatcher(matter.getMobilizedBody(pelvis.getIndex()), 4, -0.3, 0.0);
		viz.addFrameController(bodyWatch);

		//		model.updMatterSubsystem().setShowDefaultGeometry(true);
		SimTK::Visualizer::InputSilo& silo = model.updVisualizer().updInputSilo();
		UserInputHandler* userInput = new UserInputHandler(silo, *bodyWatch, 0.001);
		model.updMultibodySystem().addEventHandler(userInput);
		const ContactGeometrySet& contactSet = model.getContactGeometrySet();
		if (contactSet.getSize() > 0) {
			double contactRad =
				static_cast<ContactSphere*>(&contactSet[1])->getRadius();

			model.updVisualizer().getGeometryDecorationGenerator()->setDispMarkerRadius(contactRad);
			//		model.updVisualizer().getGeometryDecorationGenerator()->setDispMarkerRadius(0.01);
		}
		else {
			model.updVisualizer().getGeometryDecorationGenerator()->setDispMarkerRadius(0.01);
		}

		myVisualizer = &viz;
	}

	// Setting integrator settings
//	integrator.setMaximumStepSize(0.005);
	integrator.setMinimumStepSize(1e-6);
	integrator.setAccuracy(1e-2);
	//	integrator.setConstraintTolerance(1e-3);
	//	integrator.setAllowInterpolation(true);
	integrator.setReturnEveryInternalStep(true);
	integrator.setFinalTime(_finalTime);

	// This handles the controls by event - which events?
	ControlStateHandler* ctrlStateHandler = new ControlStateHandler(model, STATE_UPD_STEPSIZE);
	model.updMultibodySystem().addEventHandler(ctrlStateHandler);


	// Casting controllerSet 0 from the model into a GeyerHerrController
	// Can't tell why this needs to happen
	// Maybe this is where the muscles are generated?
	GeyerHerrController* ghc = static_cast<GeyerHerrController*>(&model.getControllerSet()[0]);

	init_s = model.initializeState();

	_systemMass = matter.calcSystemMass(init_s);
	//double mass = 80;  
	//cout << "Body mass = " << mass << " kg" << endl;
	//cout << "Whole system mass = " << _systemMass << " kg" << endl;


	// Updating model initialize state with allCtrls
	///////////////////////////////////////////////////////////////////////////////////////////
	//// for all stateNames in model, updateY of init_system 
	//// setting each modelStateVariables to the corresponding value from [_ctrlPredix]_values.sto (which includes params from ctrlFile)
	const Array<std::string>& stateNames = model.getStateVariableNames();
	for (int i = 0; i < stateNames.getSize(); i++) {
		int stIdx = allCtrls.getStateIndex(stateNames[i]);
		if (stIdx >= 0) {
			double out = allSimParams[stIdx];
			int simbody_stIdx = model.getStateVariableSystemIndex(stateNames[i]);
			init_s.updY()[simbody_stIdx] = out;
		}
	}
	/////////////////////////////////////////////////////////////////////////////////////////////

#ifdef USE_MTU
	// Setting up all muscle connection and drawing parameters
	// Setting parameters for prescribed perturbation 
	initAllMuscles(model, _pertAmp); // has to happen after initSystem
	//printMuscleMasses(model);
	//system("pause");
#endif
	if (_printResults)
		setupResultStorage(model);

#ifdef PRINTMUSCLEINFO
	printMuscleCurves(model, init_s, "ILPSO_r", "hip_r_flexion");
	printMuscleCurves(model, init_s, "GMAX_r", "hip_r_flexion");
	printMuscleCurves(model, init_s, "HAMS_r", "hip_r_flexion");
	printMuscleCurves(model, init_s, "RF_r", "hip_r_flexion");
	printMuscleCurves(model, init_s, "HAMS_r", "knee_r_extension");
	printMuscleCurves(model, init_s, "RF_r", "knee_r_extension");
	printMuscleCurves(model, init_s, "VAS_r", "knee_r_extension");
	printMuscleCurves(model, init_s, "GAS_r", "knee_r_extension");
	printMuscleCurves(model, init_s, "GAS_r", "ankle_r_dorsiflexion");
	printMuscleCurves(model, init_s, "SOL_r", "ankle_r_dorsiflexion");
	printMuscleCurves(model, init_s, "TA_r", "ankle_r_dorsiflexion");

	printMuscleCurves(model, init_s, "ILPSO_l", "hip_l_flexion");
	printMuscleCurves(model, init_s, "GMAX_l", "hip_l_flexion");
	printMuscleCurves(model, init_s, "HAMS_l", "hip_l_flexion");
	printMuscleCurves(model, init_s, "RF_l", "hip_l_flexion");
	printMuscleCurves(model, init_s, "HAMS_l", "knee_l_extension");
	printMuscleCurves(model, init_s, "RF_l", "knee_l_extension");
	printMuscleCurves(model, init_s, "VAS_l", "knee_l_extension");
	printMuscleCurves(model, init_s, "GAS_l", "knee_l_extension");
	printMuscleCurves(model, init_s, "GAS_l", "ankle_l_dorsiflexion");
	printMuscleCurves(model, init_s, "SOL_l", "ankle_l_dorsiflexion");
	printMuscleCurves(model, init_s, "TA_l", "ankle_l_dorsiflexion");
#endif

	// Setting initial control values in the GeyerHerr controller
	// Could maybe set history and other parameters as well for a complete initiation of a previously known state
	ghc->setControlParams(allCtrls);

	// TIMESTEPPER
	ts.setReportAllSignificantStates(true);
	ts.initialize(init_s);       // set integrator's initial state



	const ContactGeometrySet& contactSet = model.getContactGeometrySet();
	try {
		mbs.realize(init_s, SimTK::Stage::Position);
	}
	catch (...) {
		f = HUGE_VAL;
		return 0;
	}

	// Initial checking if contacts are beneath ground?
	double depth = 0.0;
	double contactRad =
		static_cast<ContactSphere*>(&contactSet[1])->getRadius();
	for (int i = 1; i < contactSet.getSize(); i++) { // 0 is ground
		SimTK::Vec3 lp = contactSet.get(i).getLocation();
		const Body& b = contactSet.get(i).getBody();

		SimTK::Vec3 p = matter.getMobilizedBody(b.getIndex()).findStationLocationInGround(init_s, lp);

		if (p[1] - contactRad < depth)
			depth = p[1] - contactRad;
	}
	depth *= -1;

	if (depth > MAX_DEPTH) {
		f = HUGE_VAL;
		return 0;
	}


	const double realStart = SimTK::realTime();    // start the timer.
	const double cpuStart = SimTK::cpuTime();	   // start the timer.
	bool unknownException = false;

	int numEvents = 0;
	SimTK::Integrator::SuccessfulStepStatus status;
	/*  (1) ReachedReportTime --------- stopped only to report; state might be interpolated.
		(2) ReachedEventTrigger ------- localized an event; this is the before state (interpolated).
		(3) ReachedScheduledEvent ----- reached the limit provided in stepTo() (scheduled event).
		(4) TimeHasAdvanced ----------- user requested control whenever an internal step is successful.
		(5) ReachedStepLimit ---------- took a lot of internal steps but didn't return control yet.
		(6) EndOfSimulation ----------- termination; don't call again.
		(7) StartOfContinuousInterval - the beginning of a continuous interval: either the start of
		the simulation, or t_high after an event handler has modified
		the state.
	 */
	 //	try {


	if (_supraCtrl) {
		/////////////// SUPRASPINAL_CTRL STUFF ////////////////////

		walkSettings1.makeObjectFromFile("walk1d5_perturb0d2.sto");
		walkSettings2.makeObjectFromFile("walk1d5_perturb0d4.sto");
		walkSettings3.makeObjectFromFile("walk1d5_perturb0d8.sto");
		walkSettings4.makeObjectFromFile("walk1d5_perturb1d2.sto");

		///////////////////////////////////////////////////////////
	}


	////////////// LOADING CONTROLLER STATE AND HISTORY FROM FILES ////////////
	if (_load_ctrl_values){
		cout << "Loading System State" << endl;
		ghc->loadControllerState();
		ghc->loadMuscleStates(model);

		// For testing purposes
		SimpleMuscle& musc = (SimpleMuscle&)(model.updForceSet().get("GMAX_l"));
		musc.printInnerValues();
	}
	///////////////////////////////////////////////////////////////////////////	

	double print_step = 0.1;
	int step_count = 0;
	double currTime = init_s.getTime();
	/////////////////// SIMULATING! ////////////////////////
	while (!integrator.isSimulationOver()) {
		// Put custom stuff in here that needs to be performed when time advances
		if (status == SimTK::Integrator::TimeHasAdvanced || currTime == 0) {
			#if 0
			if (ts.getTime() >= print_step*step_count) {
				cout << "Time: " << ts.getTime() << " - ";
				ghc->printInnerState();
				step_count++;
			}
			#endif

			if (_save_ctrl_values && !foundTime && currTime >= _save_ctrl_at_time) {
				cout << endl << "Saving Controller Values at time " << currTime << endl;
				ghc->saveControllerState(init_s);
				foundTime = true;

				//// Printing state before simu starts
				ghc->saveMuscleStates(model, init_s);

				// For testing purposes
				SimpleMuscle& musc = (SimpleMuscle&)(model.updForceSet().get("GMAX_l"));
				musc.printInnerValues();
			}
		}


		// Integrate until a significant step has been taken
		// (because both setReportAllSignificantStates and
		// integrator.setReturnEveryInternalStep is set to true).
		// A significant step includes all step statuses above.
		status = ts.stepTo(_finalTime);

		// Get the latest state and increment counter.
		init_s = ts.getState();
		currTime = init_s.getTime();
		// Do the following only when time has advanced in the simulation:
		//    1) Update the model cache;
		//    2) Append the state to a special time-advancing state storage.
		//
		// Note: advancing time is defined by the TimeHasAdvanced || 
		//       EndOfSimulation || StartOfContinuousInterval status
		//       and the number of states here should be the same as
		//       that reported by the integrator for the simulation.
		//       Note also that the STATE_POLLING_INTERVAL in Globals.h
		//       will affect the periodicity of the states reported
		//       due to StartOfContinuousInterval.
		if (_printResults && (status == SimTK::Integrator::TimeHasAdvanced 
				|| status == SimTK::Integrator::EndOfSimulation
				|| status == SimTK::Integrator::StartOfContinuousInterval)) {
            addToResultStorage(model, init_s);
		}

		if (_supraCtrl) {
			// Update states here while simulating
			if (status == SimTK::Integrator::TimeHasAdvanced) {
				if (ts.getTime() > changeAt*(didChange + 1)) {
					//Array<double>& walk1d0Params = walk1d0.getLastStateVector()->getData();
					//updateParams(walk1d0Params, _ctrlPrefix, allSimParams);
					//ghc->setControlParams(allCtrls);
					//Storage test = walkSettings[didChange - 1];

					/*
					if (test.getSize() > didChange) {
						Storage temp = test[didChange];
						ghc->setControlParams(temp);
						std::cout << "Set new controls to " << didChange << endl;
						didChange++;
					}
					*/

					//if (didChange == 0) { ghc->setControlParams(test[0]); }
					if (didChange == 0) { ghc->setControlParams(walkSettings1); }
					if (didChange == 1) { ghc->setControlParams(walkSettings2); }
					if (didChange == 2) { ghc->setControlParams(walkSettings3); }
					if (didChange == 3) { ghc->setControlParams(walkSettings4); }

					std::cout << "Set new controls to " << didChange << endl;
					didChange++;
				}
			}
		}

		if (status == SimTK::Integrator::ReachedEventTrigger) 
			numEvents++; 
	}   
//	}
//	catch (...) {
//		std::cout << "An exception occured during the simulation: \n\t" << std::endl;
//	}

    double human_time = SimTK::realTime() - realStart;   
    double cpu_time = SimTK::cpuTime() - cpuStart;   
    const double sim_time = integrator.getTime();
    const double avrFunctEvalsPerstep = (double)integrator.getNumRealizations()/(double)integrator.getNumStepsTaken();

	if (useViz) { 
		std::cout << "Simulation (" << sim_time << " sec) completed in " << human_time << " sec." << std::endl;
		std::cout << "human_time/sim_time ratio = " << human_time/sim_time << "   (ratio < 1 indicates realtime)." << std::endl;

		std::cout << "Integrated using " << integrator.getMethodName() << " at accuracy " << integrator.getAccuracyInUse() << std::endl;
		std::cout << integrator.getNumStepsTaken() << " integration steps taken (" << integrator.getNumStepsTaken()/sim_time << " steps/sec)" << std::endl;

		std::cout << "Average step size: " << (1000*sim_time)/integrator.getNumStepsTaken() << " ms." << std::endl;
		std::cout << "Average step computation time: " << (1000*cpu_time)/integrator.getNumStepsTaken() << " ms." << std::endl;
		std::cout << "Average function evaluations (realizations) per step: " << avrFunctEvalsPerstep << std::endl;

		std::cout << "# STEPS/ATTEMPTS = "  << integrator.getNumStepsTaken() << " / " << integrator.getNumStepsAttempted() << std::endl;
		std::cout << "# ERR TEST FAILS = "  << integrator.getNumErrorTestFailures() << std::endl;
		std::cout << "# REALIZE/PROJECT = " << integrator.getNumRealizations() << " / " << integrator.getNumProjections() << std::endl;

		std::cout << "# Events triggered = " << numEvents << std::endl;
	}
	const std::vector<SimTK::State>& ss = ghc->getStateHistory();
	const std::vector<SimTK::State>& contact_s = ghc->getContactStateHistory();
	int cN = contact_s.size(); 
	int N = ss.size();
	if (N <= 1) {
		f = HUGE_VAL; 
		return 0;
	}
//	std::cout << "N = " << N << std::endl;

	
	// Realize all states in one bulk go.
	try {
		for (int i = 0; i < N; i++) { mbs.realize(ss[i], SimTK::Stage::Dynamics); }
		for (int i = 0; i < cN; i++) { mbs.realize(contact_s[i], SimTK::Stage::Dynamics); }
	} 
	catch (...) {
		f = HUGE_VAL; 
		return 0; 
	}


	int postureFrames = 0;
	double Khead = 0.0; 
	double Ktorso = 0.0; 
	double Kvel = 0.0;
	double Kacc = 0.0;
	double Kheadv = 0.0;
	double Kfoot = 0.0; 
    double KGRF = 0.0;
	const BodySet& bodySet = model.getBodySet();
    double stepPeriod = 0.0;
    double stepLength = 0.0;

	try {
		const Body& pelvis = bodySet.get("pelvis");
		double lastGRF[2];
		lastGRF[0] = NaN;
		lastGRF[1] = NaN;
		int numGRFFramesExeedingImpulseThreshold[2];
		numGRFFramesExeedingImpulseThreshold[0] = 0;
		numGRFFramesExeedingImpulseThreshold[1] = 0;

		for (int i = 0; i < N; i++) {
			// cN = number of contacts
			//cout << "cN = " << cN << endl;
			if (cN < 5 || (ss[i].getTime() < contact_s[4].getTime())) 
				continue; 
			postureFrames++; 
			

			//// Ktorso ////
			SimTK::Vec3 pelvisUp;
			getUpVectorInGround(model, ss[i], pelvis, pelvisUp);
			double Theta = -(acos(SimTK::dot(pelvisUp.normalize(), SimTK::Vec3(1.0, 0.0, 0.0))) - SimTK::Pi/2); 
			Ktorso += TORSO_ORIENTATION_PENALTY_WEIGHT*Q(Theta, 0.1); 
			//std::cout << Theta << std::endl;
			//// end ////




			//// Kheadv ////
			SimTK::Vec3 comv = 
				matter.calcSystemMassCenterVelocityInGround(ss[i]);
			SimTK::Vec3 headv = 
				matter.getMobilizedBody(pelvis.getIndex()).findStationVelocityInGround(ss[i], 0.4*UnitY);
			//std::cout << headv << std::endl;
			headv = headv - comv;
			Kheadv += HEAD_VELOCITY_WEIGHT*Q(headv[0], 0.2); 
			// std::cout << Q(headv[0], 0.2) << " " << headv[0] << std::endl; 
			/*
			   SimTK::Vec3 headCenter; 
			   head.getMassCenter(headCenter);
			   SimTK::Vec3 headRelVel = matter.getMobilizedBody(head.getIndex()).findStationVelocityInGround(ss[j], headCenter) - comv;
			 */
			//// end ////






			//// KGRF ////
			/* get grf impulse (threshold at 20,000 N/s, i.e 200N change in 0.01 sec)
			/ states are sampled at 0.01 sec.
			/ cout << ss[i].getTime() << "   N=" << N << endl; */
			double ts = STATE_UPD_STEPSIZE * OBJ_SKIP_FRAMES;   // should be 0.01
			const HuntCrossleyForce& GRF_R = (HuntCrossleyForce&) (model.getForceSet().get("grf_r"));
			const HuntCrossleyForce& GRF_L = (HuntCrossleyForce&) (model.getForceSet().get("grf_l"));
			double currGRF[2];  // in Y direction
			//model.getMultibodySystem().realize(ss[i], SimTK::Stage::Dynamics);

			// Use this force to change stance states - how much weight is each leg carrying
			currGRF[0] = GRF_R.getRecordValues(ss[i])[1] * -1;     // y direction is index 1
			currGRF[1] = GRF_L.getRecordValues(ss[i])[1] * -1;     // multiply by -1 to get force of foot on ground

			// Calculating KGRF (GRF_IMPULSE_PENALTY)
			if (isNaN(lastGRF[0])) {   // first case
				lastGRF[0] = currGRF[0];
				lastGRF[1] = currGRF[1];
			}
			else {
				double GRFImpulse[2];
				GRFImpulse[0] = (currGRF[0] - lastGRF[0]) / (ts*_systemMass);
				GRFImpulse[1] = (currGRF[1] - lastGRF[1]) / (ts*_systemMass);
				lastGRF[0] = currGRF[0];
				lastGRF[1] = currGRF[1];
				//cout << "t=" << ss[i].getTime() << "   Impulse(R) = " 
				//    << GRFImpulse[0] << "   Impulse(L) =  " << GRFImpulse[1] << endl;
				//cout << GRFImpulse[0] << endl;

				double tmp = KGRF;
				double slopeThreshold = 400;       // slope / bodyweight
				KGRF += GRF_IMPULSE_PENALTY_WEIGHT*Q(GRFImpulse[0], slopeThreshold);  // right leg
				if (KGRF > tmp) {
					numGRFFramesExeedingImpulseThreshold[0]++;
					//cout << "RightLeg impulse violation at t = " << ss[i].getTime() << " penalty = " << GRF_IMPULSE_PENALTY_WEIGHT*Q(GRFImpulse[0], slopeThreshold) << endl;
				}

				tmp = KGRF;
				KGRF += GRF_IMPULSE_PENALTY_WEIGHT*Q(GRFImpulse[1], slopeThreshold);  // left leg
				if (KGRF > tmp) {
					numGRFFramesExeedingImpulseThreshold[1]++;
					//cout << "LeftLeg impulse violation at t = " << ss[i].getTime() << " penalty = " << GRF_IMPULSE_PENALTY_WEIGHT*Q(GRFImpulse[1], slopeThreshold) << endl;
				}

			}
			//// end ////


		}
		//cout << "numGRFFramesExeedingImpulseThreshold[0] = " << numGRFFramesExeedingImpulseThreshold[0] << endl;
		//cout << "numGRFFramesExeedingImpulseThreshold[1] = " << numGRFFramesExeedingImpulseThreshold[1] << endl;

		// Loop through contact states (cN = size of contactStateHistory )
		for (int i = 1; i < cN; i++) {
			SimTK::Vec3 com_T = matter.calcSystemMassCenterLocationInGround(contact_s[i]); 
			SimTK::Vec3 com_0 = matter.calcSystemMassCenterLocationInGround(contact_s[i-1]);

			// times of alternate foot contacts
			double t_T = contact_s[i].getTime(); 
			double t_0 = contact_s[i-1].getTime(); 

			stepPeriod += (t_T - t_0);
			stepLength += (com_T[0] - com_0[0]);
			//cout << "stepPeriod(" << i << "): " << t_T << " - " << t_0 << " = " << (t_T - t_0)
			//     << ",  stepLength(" << i << "): " << com_T[0] << " - " << com_0[0] << " = " << (com_T[0] - com_0[0]) << endl;

			double vx = (com_T[0] - com_0[0])/(t_T - t_0); 
			//Kvel += TARGET_VELOCITY_PENALTY_WEIGHT*Q(vx - _targetVel, 0.05);
			Kvel += TARGET_VELOCITY_PENALTY_WEIGHT*Q(vx - _targetVel, 0.0);

			//std::cout << "vx: " << vx << endl;

		}


		/*
		// Loop through contact states (cN = size of contactStateHistory )
		// ACCELERATION
		for (int i = 4; i < cN-1; i++) {
			SimTK::Vec3 com = matter.calcSystemMassCenterLocationInGround(contact_s[i]);
			SimTK::Vec3 comm = matter.calcSystemMassCenterLocationInGround(contact_s[i - 1]);
			SimTK::Vec3 comp = matter.calcSystemMassCenterLocationInGround(contact_s[i + 1]);

			double tsteps = STATE_UPD_STEPSIZE * OBJ_SKIP_FRAMES;
			double dt = contact_s[i].getTime() - contact_s[i - 1].getTime();
			
			// accel = ( f(x+h) - 2*f(x) + f(x-h) )/ h^2
			double accel = ((comp[0] - (2 * com[0]) + comm[0]) * tsteps * tsteps) / (dt*dt);

			Kacc += accel; // TARGET_VELOCITY_PENALTY_WEIGHT*Q(accel - _targetVel, 0.05);
			std::cout << "accel: " << accel << endl; 
			std::cout << "dt: " << dt << endl;
		}
		std::cout << "Kacc: " << Kacc << endl;
		*/

	}
	catch(...) {
		std::cout << "Caught an exception...?" << endl;
	}

    // Note: some of these values may be incorrect if we get weird bouncing contacts...
    double avrStepPeriod = stepPeriod/(cN-1);
	double avrStridePeriod = 2 * avrStepPeriod;
    double avrStepLength = stepLength/(cN-1);
    double avrStrideLength = 2 * avrStepLength;
    double avrStrideFrequency = 1/avrStridePeriod;

	double effortPenalty = 0; 

	effortPenalty = ghc->getTotalMetabolics() + 
		COORDINATE_LIMIT_PENALTY_WEIGHT*ghc->getTotalLigamentTorque2(); 
		
	
	double Kfail = 100.0*(_finalTime / (OBJ_SKIP_FRAMES*STATE_UPD_STEPSIZE) - N);

	effortPenalty /= N; 
	Kfail /= N;
	if (cN > 1)  
		Kvel /= (cN-1);
	else 
		Kvel = 0;
	
	if (postureFrames > 1) {
		Kheadv /= postureFrames; 
        KGRF /= (postureFrames-1); 
    }
	else {
		Kheadv = 0; 
		KGRF = 0;
    }
    //cout << Kvel/N << " " << Kvel/(cN-1) << " " << N << " " << (cN-1) << " " << double(cN-1)/N << endl; 

	SimTK::Vec3 com_T = 
	matter.calcSystemMassCenterLocationInGround(ss[N-1]); 
	SimTK::Vec3 com_0 = 
	matter.calcSystemMassCenterLocationInGround(ss[0]);

	double avg_vx = 
	(com_T[0] - com_0[0])/(ss[N-1].getTime() - ss[0].getTime());

//	double w_task = 2.5*(10*(1.0/(OBJ_SKIP_FRAMES*STATE_UPD_STEPSIZE))); 
	double w_task = 5*(10*(1.0/(OBJ_SKIP_FRAMES*STATE_UPD_STEPSIZE))); 
	if (_targetVel < 0) {
		f = w_task*(Kfail + Kheadv);

		double w_effort = 1.0/_systemMass; 

		f += w_effort*effortPenalty/(fabs(avg_vx)); 
	} 
	else {
		f = w_task*(Kfail + Kvel + Kheadv + KGRF);
		double w_effort = 1.0/_systemMass; 
		f += w_effort*effortPenalty;

        if (useViz) {
		    cout << "Fail=" << w_task*Kfail << "  " 
            << "Tasks=" << w_task*Kvel << "  " 
            << "HeadVel=" << w_task*Kheadv << "  " 
            << "GRFimpulse=" << w_task*KGRF << "  " 
            << "EFFORT=" << w_effort*effortPenalty << endl;
        }
	}
	
	if (std::isnan(f)) {
		f = HUGE_VAL; 
		return 0; 
	}
    if (_printDetailsToFile) {
        ofstream outfile("_details.txt");
        outfile << "Velocity: " << avg_vx << " m/s" << "  (target = " << _targetVel << " m/s)" << endl;
		outfile << "Metabolic Rate: " << ghc->getTotalMetabolics()/N << " J/s" << endl; 
        outfile << "Stride Frequency: " << avrStrideFrequency << " Hz" << endl; 
        outfile << "Stride length: " << avrStrideLength << " m" << endl;
		outfile << "Effort Penalty: " << effortPenalty << endl; 
        outfile << "Objective: " << f << endl; 
        outfile.close();
    }
	if (useViz) {
		if (ghc) {
            //printMuscleMasses(model);
			cout << "Velocity: " << avg_vx << " m/s" << "  (target = " << _targetVel << " m/s)" << endl;
			cout << "Metabolic Rate: " << ghc->getTotalMetabolics()/N << " J/s" << endl; 
            cout << "Stride Frequency: " << avrStrideFrequency << " Hz" << endl; 
            cout << "Stride Length: " << avrStrideLength << " m" << endl;
			cout << "Effort Penalty: " << effortPenalty << endl; 
		}
	}
//	std::cout << Kfoot << " " << Kvel << " " << Khead << " " << Kheadv <<  " " << Kfail << " " << Ktorso << " "  << effortPenalty/mass << " "  << f << std::endl;
	
    // Print results to file
    if (_printResults){
		std::ostringstream description;
		//description << endl;
		description << "finaltime=" << _finalTime << endl;
		description << "targetVel=" << _targetVel << endl;
		description << "avgVel=" << avg_vx << endl;
		description << "modelFile=" << _modelFile << endl;
		//description << "ctrlFile=" << _ctrlFile << endl;
		description << "fitness=" << f << endl;

		_results->setDescription(description.str());

        stringstream file;
        file << _results->getName() << ".sto"; 
        _results->print(file.str());
    }


#if 0 // Printing out each simulation as a separate file. Has been replaced by printing to one csv file instead
	double fitnessOverflow = 500000;
	if (_doOpt && _save_all_sim && f < fitnessOverflow) {// so it doesn't save files when visualizing
		//////////////////////////////////////////////////////
		// NOT IMPLEMENTED YET
		// Printing result_itr for all simulations on each core (That's a lot of files!)

		// Adding metadata to result file
		std::ostringstream description;
		description << "avg_vx=" << avg_vx << endl;
		description << "targetVel=" << _targetVel << endl;
		description << "fitness=" << f << endl;
		allCtrls.setDescription(description.str());

		// Generating file ID. Probably/most certainly not the ideal approach but it works for now 
		// All cores write to the same file on the first run. Not good
		// Due to all cores running the same sim with same parameters at the same time. How to differentiate?
		int r = rand();
		int timeNow = time(0);
		long long idVal = (long long)(avrStrideFrequency * avrStrideLength * effortPenalty * avg_vx * ghc->getTotalMetabolics() * 1000 + timeNow + r);
		std::stringstream sid;
		sid << fixed << std::setfill('0') << std::setw(14) << idVal;

		//cout << "opt filename " << "result_coreSim_" + sid.str() + ".sto" << endl;
		//cout << "iSecret: " << timeNow << endl;
		allCtrls.print("result_coreSim_" + convertInt(optRuns) + "_" + sid.str() + ".sto");

		//Array<double>& ctrlData = allCtrls.getStateVector(0)->getData();
	}
#endif

    return 0; 
}



// Setting initial muscle values and initiating drawings
// pertAmp: Perturbation Amplitude
void PredictiveOptimizationSystem::initAllMuscles(Model& m, double pertAmp)
{
    double lfac = 0.84/1.0;     // ratio of leg length compared to GeyerHerr (1.0 meter)
	lfac = 1.0; 

    // Get list of body references (for muscle drawing purposes).
	SimTK::MobilizedBodyIndex pelvis = m.updBodySet().get("pelvis").getIndex();
	SimTK::MobilizedBodyIndex thigh_r = m.updBodySet().get("thigh_r").getIndex();
    SimTK::MobilizedBodyIndex shank_r = m.updBodySet().get("shank_r").getIndex();
    SimTK::MobilizedBodyIndex foot_r = m.updBodySet().get("foot_r").getIndex();
    SimTK::MobilizedBodyIndex thigh_l = m.updBodySet().get("thigh_l").getIndex();
    SimTK::MobilizedBodyIndex shank_l = m.updBodySet().get("shank_l").getIndex();
    SimTK::MobilizedBodyIndex foot_l = m.updBodySet().get("foot_l").getIndex();
	
	ForceSet& fs = m.updForceSet(); 


	// Setting up potential perturbation forces
	if (fs.contains("perturbForce") && pertAmp) {
		//srand((int)time(0)); // set random seed
		
		double val1 = 50 * pertAmp; // ((rand() % 100) - 50)*pertAmp;
		//double val2 = 30 * pertAmp; // ((rand() % 100) - 50)*pertAmp;
		//double val3 = 40 * pertAmp; // ((rand() % 100) - 50)*pertAmp;
		//double val4 = 50 * pertAmp; // ((rand() % 100) - 50)*pertAmp;
		double t[3] = {  0,    2,   3 };// , 3, 5, 6, 7, 8, 9, 10};	// time nodes for linear function
		double f[3] = {  0, val1,   0 };// , 0, val2, 0, val3, 0, val4, 0};	// force values at t1 and t2
		double p[3] = {-10,  -10, -10 };// , -10, -10, -10, -10, -10, -10, -10};	// point in x values at t1 and t2

		PiecewiseConstantFunction *forceX = new PiecewiseConstantFunction(3, t, f);
		PiecewiseConstantFunction *pointX = new PiecewiseConstantFunction(3, t, p);

		PrescribedForce* perturbForce = static_cast<PrescribedForce*>(&fs.get("perturbForce"));

		perturbForce->setForceFunctions(forceX, new Constant(0.0), new Constant(0.0));
		perturbForce->setPointFunctions(pointX, new Constant(0.0), new Constant(0.0));

		std::cout << endl << "Perturbation Active: " << val1 << endl;
		//cout << endl << "Perturbation Amplitude: " << pertAmp << ", Force = at 2: " << val1 << ", at 5: " << val2 << endl << endl;


		//cout << "perts" << perts << endl << "tstep: " << tstep << endl << "t: " << t << endl << "f: " << f << endl << "p: " << p << endl;
		//cout << "T: ";
		//for (int i = 10 - 1; i >= 0; i--)
		//	cout << t[i] << " ";
		//cout << endl << "F: ";
		//for (int i = 10 - 1; i >= 0; i--)
		//	cout << f[i] << " ";
		//cout << endl << "P: ";
		//for (int i = 10 - 1; i >= 0; i--)
		//	cout << p[i] << " ";
		//cout << endl;

		//m.print("../testmodel.osim");
	}
		
	

	double offset = -0.3; 
	if (fs.contains("ILPSO_r")) {
		SimpleMuscle* ilpso_r = static_cast<SimpleMuscle*>(&fs.get("ILPSO_r")); 

		#ifdef POLYMOMARMS
			ilpso_r->setAttachmentParamsPoly1(0.255, 0.028320, 0.001560, 0.001650); // polynomial
		#else
			#ifdef USE_GEYER_HIP_MOMARM
				ilpso_r->setAttachmentParams1(lfac*0.1, -10000, SimTK_PI, 0.5);     // (G&H) hip
			#else
				ilpso_r->setAttachmentParams1(lfac*0.05, -10000, SimTK_PI, 0.5);     // (G&H) hip
			#endif
		#endif

		ilpso_r->setMuscleParams(0.008, TENDON_COMP);
		ilpso_r->setCoordinates(1, "hip_r_flexion", "NULL");
		
		ilpso_r->addDrawPoint(pelvis, SimTK::Vec3(0.061,0.033+offset,0));
		ilpso_r->addDrawPoint(pelvis, SimTK::Vec3(0.105,-0.057+offset,0));
		ilpso_r->addDrawPoint(pelvis, SimTK::Vec3(0.103,-0.118+offset,0));
		ilpso_r->addDrawPoint(thigh_r, SimTK::Vec3(0.076,0.013,0));
		ilpso_r->addDrawPoint(thigh_r, SimTK::Vec3(0.048,-0.054,0));
	}
	
	if (fs.contains("ILPSO_l")) {
		SimpleMuscle* ilpso_l = static_cast<SimpleMuscle*>(&fs.get("ILPSO_l")); 

		#ifdef POLYMOMARMS
			ilpso_l->setAttachmentParamsPoly1(0.255, 0.028320, 0.001560, 0.001650); // polynomial
		#else
			#ifdef USE_GEYER_HIP_MOMARM
				ilpso_l->setAttachmentParams1(lfac*0.1, -10000, SimTK_PI, 0.5);     // (G&H) hip
			#else
				ilpso_l->setAttachmentParams1(lfac*0.05, -10000, SimTK_PI, 0.5);     // (G&H) hip
			#endif
		#endif

		ilpso_l->setMuscleParams(0.008, TENDON_COMP);
		ilpso_l->setCoordinates(1, "hip_l_flexion", "NULL");
		ilpso_l->addDrawPoint(pelvis, SimTK::Vec3(0.061,0.033+offset,0));
		ilpso_l->addDrawPoint(pelvis, SimTK::Vec3(0.105,-0.057+offset,0));
		ilpso_l->addDrawPoint(pelvis, SimTK::Vec3(0.103,-0.118+offset,0));
		ilpso_l->addDrawPoint(thigh_l, SimTK::Vec3(0.076,0.013,0));
		ilpso_l->addDrawPoint(thigh_l, SimTK::Vec3(0.048,-0.054,0));
	}
    
	if (fs.contains("GMAX_r")) {
		SimpleMuscle* gmax_r = 
			static_cast<SimpleMuscle*>(&fs.get("GMAX_r"));  
		#ifdef POLYMOMARMS
			gmax_r->setAttachmentParamsPoly1(0.233, -0.089250, -0.021894, 0.003013); // polynomial
		#else
			#ifdef USE_GEYER_HIP_MOMARM
				gmax_r->setAttachmentParams1(lfac*0.1, -10000, 2.62, -0.5);     // (G&H) hip
			#else
				gmax_r->setAttachmentParams1(lfac*0.08, -10000, 2.62, -0.5);     // (G&H) hip
			#endif
		#endif
		gmax_r->setMuscleParams(0.006, TENDON_COMP);
		gmax_r->setCoordinates(1, "hip_r_flexion", "NULL");
		gmax_r->addDrawPoint(pelvis, SimTK::Vec3(-0.059,-0.132+offset,0));
		gmax_r->addDrawPoint(pelvis, SimTK::Vec3(-0.056,-0.182+offset,0));
		gmax_r->addDrawPoint(thigh_r, SimTK::Vec3(-0.082,-0.007,0));
		gmax_r->addDrawPoint(thigh_r, SimTK::Vec3(-0.077,-0.055,0));
		gmax_r->addDrawPoint(thigh_r, SimTK::Vec3(-0.044,-0.111,0));
	}

	if (fs.contains("GMAX_l")) {
		SimpleMuscle* gmax_l = 
			static_cast<SimpleMuscle*>(&fs.get("GMAX_l")); 

		#ifdef POLYMOMARMS
			gmax_l->setAttachmentParamsPoly1(0.233, -0.089250, -0.021894, 0.003013); // polynomial
		#else
			#ifdef USE_GEYER_HIP_MOMARM
				gmax_l->setAttachmentParams1(lfac*0.1, -10000, 2.62, -0.5);     // (G&H) hip
			#else
				gmax_l->setAttachmentParams1(lfac*0.08, -10000, 2.62, -0.5);     // (G&H) hip
			#endif
		#endif

		gmax_l->setMuscleParams(0.006, TENDON_COMP);
		gmax_l->setCoordinates(1, "hip_l_flexion", "NULL");
		gmax_l->addDrawPoint(pelvis, SimTK::Vec3(-0.059,-0.132+offset,0));
		gmax_l->addDrawPoint(pelvis, SimTK::Vec3(-0.056,-0.182+offset,0));
		gmax_l->addDrawPoint(thigh_l, SimTK::Vec3(-0.082,-0.007,0));
		gmax_l->addDrawPoint(thigh_l, SimTK::Vec3(-0.077,-0.055,0));
		gmax_l->addDrawPoint(thigh_l, SimTK::Vec3(-0.044,-0.111,0));
	}


	if (fs.contains("HAMS_l")) {
		SimpleMuscle* hams_l = 
			static_cast<SimpleMuscle*>(&fs.get("HAMS_l"));  
		#ifdef POLYMOMARMS
			hams_l->setAttachmentParamsPoly1(0.425, -0.072210, 0.008746, 0.005905); // polynomial
			hams_l->setAttachmentParamsPoly2(-0.020575, -0.009049, 0.0034985); // polynomial
		#else
			hams_l->setAttachmentParams1(lfac*0.08, -10000, 2.71, -0.7);            // (G&H) hip
			hams_l->setAttachmentParams2(lfac*0.05, SimTK_PI, SimTK_PI, 0.7);     // (G&H) knee
		#endif

		hams_l->setMuscleParams(0.012, TENDON_COMP, 0.44);
		hams_l->setCoordinates(2, "hip_l_flexion", "knee_l_extension");
		hams_l->addDrawPoint(pelvis, SimTK::Vec3(-0.068,-0.162+offset,0));
		hams_l->addDrawPoint(shank_l, SimTK::Vec3(-0.052,0.001,0));
		hams_l->addDrawPoint(shank_l, SimTK::Vec3(-0.03,-0.05,0));
	}

	if (fs.contains("HAMS_r")) {
		SimpleMuscle* hams_r = 
			static_cast<SimpleMuscle*>(&fs.get("HAMS_r"));  
		#ifdef POLYMOMARMS
			hams_r->setAttachmentParamsPoly1(0.425, -0.072210, 0.008746, 0.005905); // polynomial
			hams_r->setAttachmentParamsPoly2(-0.020575, -0.009049, 0.0034985); // polynomial
		#else
			hams_r->setAttachmentParams1(lfac*0.08, -10000, 2.71, -0.7);            // (G&H) hip
			hams_r->setAttachmentParams2(lfac*0.05, SimTK_PI, SimTK_PI, 0.7);     // (G&H) knee
		#endif

		hams_r->setMuscleParams(0.012, TENDON_COMP, 0.44);
		hams_r->setCoordinates(2, "hip_r_flexion", "knee_r_extension");
		hams_r->addDrawPoint(pelvis, SimTK::Vec3(-0.068,-0.162+offset,0));
		hams_r->addDrawPoint(shank_r, SimTK::Vec3(-0.052,0.001,0));
		hams_r->addDrawPoint(shank_r, SimTK::Vec3(-0.03,-0.05,0));
	}

	if (fs.contains("RF_r")) {
		SimpleMuscle* rf_r = 
			static_cast<SimpleMuscle*>(&fs.get("RF_r"));  
		#ifdef POLYMOMARMS
			rf_r->setAttachmentParamsPoly1(0.391, 0.035330, 0.005920, 0.0); // polynomial
			rf_r->setAttachmentParamsPoly2(0.056900, -0.007940, 0.0); // polynomial
		#else
			#ifdef USE_GEYER_HIP_MOMARM
				rf_r->setAttachmentParams1(lfac*0.1, -10000, SimTK_PI, 0.7);     // (G&H) hip
			#else
				rf_r->setAttachmentParams1(lfac*0.05, -10000, SimTK_PI, 0.7);     // (G&H) hip
			#endif
			rf_r->setAttachmentParams2(lfac*0.06, 2.88, 2.18, -0.7);        // (G&H) knee
		#endif

		rf_r->setMuscleParams(0.004, TENDON_COMP, 0.423);
		rf_r->setCoordinates(2, "hip_r_flexion", "knee_r_extension");
		rf_r->addDrawPoint(pelvis, SimTK::Vec3(0.09,-0.167+offset,0));
		rf_r->addDrawPoint(thigh_r, SimTK::Vec3(0.028,-0.407,0));
		rf_r->addDrawPoint(thigh_r, SimTK::Vec3(0.024,-0.427,0));
		rf_r->addDrawPoint(thigh_r, SimTK::Vec3(0.013,-0.439,0));
		rf_r->addDrawPoint(shank_r, SimTK::Vec3(0.027,0.02,0));
	}

	if (fs.contains("RF_l")) {
		SimpleMuscle* rf_l = 
			static_cast<SimpleMuscle*>(&fs.get("RF_l"));  
		#ifdef POLYMOMARMS
	        rf_l->setAttachmentParamsPoly1(0.391, 0.035330, 0.005920, 0.0); // polynomial
			rf_l->setAttachmentParamsPoly2(0.056900, -0.007940, 0.0); // polynomial
		#else
			#ifdef USE_GEYER_HIP_MOMARM
				rf_l->setAttachmentParams1(lfac*0.1, -10000, SimTK_PI, 0.7);     // (G&H) hip
			#else
				rf_l->setAttachmentParams1(lfac*0.05, -10000, SimTK_PI, 0.7);     // (G&H) hip
			#endif
			rf_l->setAttachmentParams2(lfac*0.06, 2.88, 2.18, -0.7);        // (G&H) knee
		#endif

		rf_l->setMuscleParams(0.004, TENDON_COMP, 0.423);
		rf_l->setCoordinates(2, "hip_l_flexion", "knee_l_extension");
		rf_l->addDrawPoint(pelvis, SimTK::Vec3(0.09,-0.167+offset,0));
		rf_l->addDrawPoint(thigh_l, SimTK::Vec3(0.028,-0.407,0));
		rf_l->addDrawPoint(thigh_l, SimTK::Vec3(0.024,-0.427,0));
		rf_l->addDrawPoint(thigh_l, SimTK::Vec3(0.013,-0.439,0));
		rf_l->addDrawPoint(shank_l, SimTK::Vec3(0.027,0.02,0));
	}

	if (fs.contains("VAS_r")) {
		SimpleMuscle* vas_r = 
			static_cast<SimpleMuscle*>(&fs.get("VAS_r"));  
		#ifdef POLYMOMARMS
	        vas_r->setAttachmentParamsPoly1(0.188, 0.045780, -0.007270, 0.0); // polynomial
		#else
			vas_r->setAttachmentParams1(lfac*0.06, 2.88, 2.18, -0.7);     // (G&H) knee
		#endif

		vas_r->setMuscleParams(0.024, TENDON_COMP, 0.50);
		vas_r->setCoordinates(1, "knee_r_extension", "NULL");
		vas_r->addDrawPoint(thigh_r, SimTK::Vec3(0.04,-0.2,0));
		vas_r->addDrawPoint(thigh_r, SimTK::Vec3(0.05,-0.27,0));
		vas_r->addDrawPoint(shank_r, SimTK::Vec3(0.043,0.017,0));
	}

	if (fs.contains("VAS_l")) {
		SimpleMuscle* vas_l = 
			static_cast<SimpleMuscle*>(&fs.get("VAS_l"));  
		#ifdef POLYMOMARMS
			vas_l->setAttachmentParamsPoly1(0.188, 0.045780, -0.007270, 0.0); // polynomial
		#else
			vas_l->setAttachmentParams1(lfac*0.06, 2.88, 2.18, -0.7);     // (G&H) knee
		#endif

		vas_l->setMuscleParams(0.024, TENDON_COMP, 0.50);
		vas_l->setCoordinates(1, "knee_l_extension", "NULL");
		vas_l->addDrawPoint(thigh_l, SimTK::Vec3(0.04,-0.2,0));
		vas_l->addDrawPoint(thigh_l, SimTK::Vec3(0.05,-0.27,0));
		vas_l->addDrawPoint(shank_l, SimTK::Vec3(0.043,0.017,0));
	}

	double offset2 = 0.05; 
	if (fs.contains("GAS_r")) {
		SimpleMuscle* gas_r = static_cast<SimpleMuscle*>(&fs.get("GAS_r"));  
		#ifdef POLYMOMARMS
			gas_r->setAttachmentParamsPoly1(0.413, -0.034070, 0.012890, -0.003550); // polynomial
			gas_r->setAttachmentParamsPoly2(0.093690, 0.098000, 0.0213067); // polynomial
		#else
			// first param is magnitude, second param is phase shift for the moment arm
			gas_r->setAttachmentParams1(lfac*0.05, 2.44, 2.88, 0.7);     // (G&H) knee
			//gas_r->setAttachmentParams1(lfac*0.05*0.7, 2.44-0.52, 2.88, 0.7);     // (G&H) knee with reduced momarm (10/4/2013)
        
			gas_r->setAttachmentParams2(lfac*0.05, 1.92, 1.4, -0.7);     // (G&H) ankle
		#endif

		gas_r->setMuscleParams(0.006, TENDON_COMP, 0.54);
		gas_r->setCoordinates(2, "knee_r_extension", "ankle_r_dorsiflexion");
		gas_r->addDrawPoint(thigh_r, SimTK::Vec3(-0.04,-0.36,0));
		gas_r->addDrawPoint(thigh_r, SimTK::Vec3(-0.06,-0.4,0));
		gas_r->addDrawPoint(foot_r, SimTK::Vec3(-0.05,-0.05+offset2,0));
	}

	if (fs.contains("GAS_l")) {
		SimpleMuscle* gas_l = static_cast<SimpleMuscle*>(&fs.get("GAS_l")); 
		#ifdef POLYMOMARMS
	        gas_l->setAttachmentParamsPoly1(0.413, -0.034070, 0.012890, -0.003550); // polynomial
		    gas_l->setAttachmentParamsPoly2(0.093690, 0.098000, 0.0213067); // polynomial
		#else
	        gas_l->setAttachmentParams1(lfac*0.05, 2.44, 2.88, 0.7);     // (G&H) knee
		    // gas_l->setAttachmentParams1(lfac*0.05*0.7, 2.44-0.52, 2.88, 0.7);     // (G&H) knee with reduced momarm (10/4/2013)
			gas_l->setAttachmentParams2(lfac*0.05, 1.92, 1.4, -0.7);     // (G&H) ankle
		#endif

		gas_l->setMuscleParams(0.006, TENDON_COMP, 0.54);
		gas_l->setCoordinates(2, "knee_l_extension", "ankle_l_dorsiflexion");
		gas_l->addDrawPoint(thigh_l, SimTK::Vec3(-0.04,-0.36,0));
		gas_l->addDrawPoint(thigh_l, SimTK::Vec3(-0.06,-0.4,0));
		gas_l->addDrawPoint(foot_l, SimTK::Vec3(-0.05,-0.05+offset2,0));
	}

	if (fs.contains("SOL_r")) {
		SimpleMuscle* sol_r = 
			static_cast<SimpleMuscle*>(&fs.get("SOL_r"));  
		#ifdef POLYMOMARMS
			sol_r->setAttachmentParamsPoly1(0.280, 0.093690, 0.098000, 0.021307); // polynomial
		#else
			sol_r->setAttachmentParams1(lfac*0.05, 1.92, 1.4, -0.5);     // (G&H) ankle
		#endif

		sol_r->setMuscleParams(0.016, TENDON_COMP, 0.81);
		sol_r->setCoordinates(1, "ankle_r_dorsiflexion", "NULL");
		sol_r->addDrawPoint(shank_r, SimTK::Vec3(-0.04,-0.14,0));
		sol_r->addDrawPoint(foot_r, SimTK::Vec3(-0.05,-0.05+offset2,0));
	}

	if (fs.contains("SOL_l")) {
		SimpleMuscle* sol_l = 
			static_cast<SimpleMuscle*>(&fs.get("SOL_l"));  
		#ifdef POLYMOMARMS
			sol_l->setAttachmentParamsPoly1(0.280, 0.093690, 0.098000, 0.021307); // polynomial
		#else
			sol_l->setAttachmentParams1(lfac*0.05, 1.92, 1.4, -0.5);     // (G&H) ankle
		#endif

		sol_l->setMuscleParams(0.016, TENDON_COMP, 0.81);
		sol_l->setCoordinates(1, "ankle_l_dorsiflexion", "NULL");
		sol_l->addDrawPoint(shank_l, SimTK::Vec3(-0.04,-0.14,0));
		sol_l->addDrawPoint(foot_l, SimTK::Vec3(-0.05,-0.05+offset2,0));
	}

	if (fs.contains("TA_r")) {
		SimpleMuscle* tibant_r = 
			static_cast<SimpleMuscle*>(&fs.get("TA_r")); 
		#ifdef POLYMOMARMS
			tibant_r->setAttachmentParamsPoly1(0.346, -0.012760, -0.042670, -0.011433); // polynomial
		#else
			tibant_r->setAttachmentParams1(lfac*0.04, 1.40, 1.92, 0.7);     // (G&H) ankle
		#endif

		tibant_r->setMuscleParams(0.0032, TENDON_COMP, 0.7);
		tibant_r->setCoordinates(1, "ankle_r_dorsiflexion", "NULL");
		tibant_r->addDrawPoint(shank_r, SimTK::Vec3(0.04,-0.15,0));
		tibant_r->addDrawPoint(shank_r, SimTK::Vec3(0.05,-0.4,0));
		tibant_r->addDrawPoint(foot_r, SimTK::Vec3(0.09,-0.01+offset2,0));
	}

	if (fs.contains("TA_l")) {
		SimpleMuscle* tibant_l = 
			static_cast<SimpleMuscle*>(&fs.get("TA_l"));  
		#ifdef POLYMOMARMS
			tibant_l->setAttachmentParamsPoly1(0.346, -0.012760, -0.042670, -0.011433); // polynomial
		#else
			tibant_l->setAttachmentParams1(lfac*0.04, 1.40, 1.92, 0.7);     // (G&H) ankle
		#endif

		tibant_l->setMuscleParams(0.0032, TENDON_COMP, 0.7);
		tibant_l->setCoordinates(1, "ankle_l_dorsiflexion", "NULL");
		tibant_l->addDrawPoint(shank_l, SimTK::Vec3(0.04,-0.15,0));
		tibant_l->addDrawPoint(shank_l, SimTK::Vec3(0.05,-0.4,0));
		tibant_l->addDrawPoint(foot_l, SimTK::Vec3(0.09,-0.01+offset2,0));
	}
}


void PredictiveOptimizationSystem::
    printMuscleCurves(Model& m, SimTK::State& s, std::string muscle, std::string coordinate) const
{
    const double rotationIntervalRad = 1 * SimTK_DEGREE_TO_RADIAN;
    SimpleMuscle& musc = (SimpleMuscle&) (m.updForceSet().get(muscle));
    Coordinate& q = (Coordinate&) (m.updCoordinateSet().get(coordinate));
    const double minRad = q.getRangeMin();
    const double maxRad = q.getRangeMax();
    cout << muscle << " && " << coordinate << endl;
    int coordIndex = -1;

    FILE *fp;
    stringstream filename;
    filename << MUSCLE_CURVE_PATH << coordinate << "__" << muscle << ".xls";
    fp=fopen(filename.str().c_str(), "w");

    for (unsigned i=0; i<musc.getCoords().size(); ++i) {
        //cout << musc.getCoords()[i]->getName() << " " << coordinate << endl;
        if (musc.getCoords()[i]->getName() == coordinate)
            coordIndex = i;
    }
    if (coordIndex == -1) {
        cout << "NO MUSCLE ATTACHMENT FOR THIS COMBINATION" << endl;
    }

	// Print labels
	fprintf(fp, "%s\t%s\t%s\n", "Angle [deg]", "Moment Arm", "MTU length");
    // Loop j: DOF Range.
    for (double j=minRad; j<=maxRad; j+=rotationIntervalRad) {
        Array<double> V;
        //cout << "angle = " << j << endl;
        placeCurrentStateIntoZeroPose(s);
        q.setValue(s, j);       // j must be in the coordinate range if it is 'clamped'.
        m.getMultibodySystem().realize(s, SimTK::Stage::Position);
        musc.updateMuscleVars(s);
        //char buffer[100];
        fprintf(fp, "%.1f\t%7.3f\t%7.5f\n", j*SimTK_RADIAN_TO_DEGREE, musc.getMomArmSign(coordIndex)*musc.getMuscVars().momArm[coordIndex], musc.getMuscVars().l_mtu);
        //cout << buffer << endl;
        
    }
    fclose(fp);
}



//_____________________________________________________________________________
/**
 * Place the current state into a zero pose with zero velocity.
 * Does not perform realization - this must be done after this method is called.
 */
void PredictiveOptimizationSystem::
    placeCurrentStateIntoZeroPose(SimTK::State& s) const
{
    SimTK::Vector q0(s.getNQ(), 0.0);
    SimTK::Vector u0(s.getNU(), 0.0);
    s.setQ(q0);                    // set q's to zero
    s.setU(u0);                    // set u's to zero
}


//_____________________________________________________________________________
/**
 * Get the state values from the OpenSim model.
 */
SimTK::Vector PredictiveOptimizationSystem::
    getOpenSimStateValues(const Model& model, const SimTK::State& s) const
{
    SimTK::Vector v(model.getNumStateVariables());
    for(int i=0; i<model.getStateVariableSystemIndices().getSize(); i++) 
        v[i] = s.getY()[model.getStateVariableSystemIndices()[i]];
    return v;
}


//_____________________________________________________________________________
/**
 * Setup result storage.
 */
void PredictiveOptimizationSystem::setupResultStorage(const Model& model) const
{
    // Set up labels for outputs.
    Array<string> colNames;
    colNames.append("time");

    // STATES
    colNames.append(model.getStateVariableNames());

    // CONTROLS
    int nU = model.getControllerSet().getSize();
    for (int i=0; i<nU; ++i)
        for (int j=0; j<model.getControllerSet()[i].getActuatorSet().getSize(); ++j)
            colNames.append(model.getControllerSet()[i].getName()+"__"+model.getControllerSet()[i].getActuatorSet()[j].getName());

    // ACTUATOR FORCES
    int nA = model.getActuators().getSize();
    for (int i=0; i<nA; ++i)
        colNames.append(model.getActuators()[i].getName());

    // OTHER MUSCLE PROPERTIES
    for (int i=0; i<nA; ++i) {
		if (typeid(model.getActuators()[i]) != typeid(SimpleMuscle)) { 
			colNames.append(model.getActuators()[i].getName() + "_IGNORE_THIS_LABEL");
			colNames.append(model.getActuators()[i].getName() + "_IGNORE_THIS_LABEL");
            colNames.append(model.getActuators()[i].getName() + "_IGNORE_THIS_LABEL");
            colNames.append(model.getActuators()[i].getName() + "_IGNORE_THIS_LABEL");
            colNames.append(model.getActuators()[i].getName() + "_IGNORE_THIS_LABEL");
            colNames.append(model.getActuators()[i].getName() + "_IGNORE_THIS_LABEL");
            colNames.append(model.getActuators()[i].getName() + "_IGNORE_THIS_LABEL");
			continue; 
		}
        SimpleMuscle& musc = (SimpleMuscle&) model.getActuators()[i];
        colNames.append(musc.getName()+".PASSIVEFORCE");
        colNames.append(musc.getName()+".LT");
        colNames.append(musc.getName()+".LM");
        colNames.append(musc.getName()+".LMTU");
        colNames.append(musc.getName()+".FLmult");
        colNames.append(musc.getName()+".FVmult");
        colNames.append(musc.getName()+".METABOLICRATE");
    }

    // JOINT TORQUES
    int nC = model.getCoordinateSet().getSize();
    for (int i=0; i<nC; ++i)
        colNames.append(model.getCoordinateSet()[i].getName()+"_TORQUE");

    // MUSCLE CONTRIBUTION TO JOINT TORQUE
    for (int i=0; i<nA; ++i) {
		if (typeid(model.getActuators()[i]) != typeid(SimpleMuscle)) 
			continue; 
        SimpleMuscle& musc = (SimpleMuscle&) model.getActuators()[i];
        //cout << musc.getName() << musc.getNumDOF() << endl;
        for (int j=0; j<musc.getNumDOF(); ++j) {
            colNames.append(musc.getName()+"__"+musc.getCoords()[j]->getName());
            //cout << musc.getName() << musc.getCoords()[j]->getName() << endl;
        }
    }

    // COORDINATE LIMIT FORCES
    int nF = model.getForceSet().getSize();
    for (int i=0; i<nF; ++i) {
        SoftCoordinateLimitForce* clf = dynamic_cast<SoftCoordinateLimitForce*> (&model.getForceSet()[i]);
        if (clf) {
            colNames.append(clf->getName());
        }
    }

    // GRF (right foot then left foot)
    colNames.append("GRF_rX");
    colNames.append("GRF_rY");
    colNames.append("GRF_rZ");
    colNames.append("GRF_lX");
    colNames.append("GRF_lY");
    colNames.append("GRF_lZ");

    _results = NULL;
	//cout << endl << "### PredictiveOptSys.cpp: _results = new Storage(1000, '_results');" << endl;
    _results = new Storage(1000, "_results");
    _results->setColumnLabels(colNames);
    _results->setDescription("Simulation results.");
    //cout << colNames << endl;
}

//_____________________________________________________________________________
/**
 * Add results to storage.
 */
void PredictiveOptimizationSystem::addToResultStorage(const Model& model, const SimTK::State& s) const
{
    model.getMultibodySystem().realize(s, SimTK::Stage::Acceleration);

    // STATES
    int nS = model.getNumStateVariables();
    SimTK::Vector VS = getOpenSimStateValues(model, s);

    // CONTROLS
    int nU = model.getNumControls();
    SimTK::Vector VU = model.getControls(s);

    // ACTUATOR FORCES
    int nA = model.getActuators().getSize();
    SimTK::Vector VA(nA);
    for (int i=0; i<nA; ++i)
        VA[i] = model.getActuators()[i].getForce(s);

    // OTHER MUSCLE PROPERTIES
    int nMP = 7;        // 7 things to output
    SimTK::Vector VMUSC(nMP*nA);
    for (int i=0; i<nA; ++i) {
		if (typeid(model.getActuators()[i]) != typeid(SimpleMuscle)) { 
        	VMUSC[nMP*i+0] = 0; 
        	VMUSC[nMP*i+1] = 0; 
        	VMUSC[nMP*i+2] = 0; 
        	VMUSC[nMP*i+3] = 0; 
            VMUSC[nMP*i+4] = 0; 
            VMUSC[nMP*i+5] = 0; 
            VMUSC[nMP*i+6] = 0; 
			continue; 
		}
        SimpleMuscle& musc = (SimpleMuscle&) model.getActuators()[i];
        VMUSC[nMP*i+0] = musc.getMuscVars().F_pe;             // passive force
        VMUSC[nMP*i+1] = musc.getMuscVars().l_se;             // tendon length
        VMUSC[nMP*i+2] = musc.getMuscVars().l_ce;             // fiber length
        VMUSC[nMP*i+3] = musc.getMuscVars().l_mtu;            // mtu length
        VMUSC[nMP*i+4] = musc.getMuscVars().fl;               // FL multiplier
        VMUSC[nMP*i+5] = musc.getMuscVars().fv;               // FV multiplier
        VMUSC[nMP*i+6] = musc.getMuscVars().metabolicrate;    // metabolic rate
    }

    // JOINT TORQUES
    int nC = model.getCoordinateSet().getSize();
    SimTK::Vector VT(nA);
    VT = getInverseDynamicsLoad(model, s);

    // MUSCLE CONTRIBUTION TO JOINT TORQUE
    SimTK::Array_<double> tmp;
    for (int i=0; i<nA; ++i) {
		if (typeid(model.getActuators()[i]) != typeid(SimpleMuscle)) { 
			continue; 
		}
        SimpleMuscle& musc = (SimpleMuscle&) model.getActuators()[i];
        for (int j=0; j<musc.getNumDOF(); ++j) {
            tmp.push_back(musc.getMuscVars().mom[j]);
            //tmp.push_back(musc.getMomArmSign(j) * musc.getMuscVars().momArm[j] * VA[i]);
            //cout << musc.getName() << "  " << musc.getCoords()[j]->getName() << endl;
            //cout << musc.getMomArmSign(j) << endl;
            //cout << musc.getMuscVars().momArm[j] << endl;
            //cout << VA[i] << endl;
            //system("pause");
        }
    }
    int nMC = tmp.size();
    SimTK::Vector VMC(nMC);
    for (int i=0; i<nMC; ++i)
        VMC[i] = tmp[i];


    // COORDINATE LIMIT FORCES
    int nF = model.getForceSet().getSize();
    SimTK::Array_<double> tmp1;
    for (int i=0; i<nF; ++i) {
        SoftCoordinateLimitForce* clf = dynamic_cast<SoftCoordinateLimitForce*> (&model.getForceSet()[i]);
        if (clf) {
            tmp1.push_back(clf->calcLimitForce(s));
        }
    }
    int nCLF = tmp1.size();
    SimTK::Vector VCLF(nCLF);
    for (int i=0; i<nCLF; ++i)
        VCLF[i] = tmp1[i];


    // GRF
    int nGRF = 6;
    const HuntCrossleyForce& GRF_R = (HuntCrossleyForce&) (model.getForceSet().get("grf_r"));
    const HuntCrossleyForce& GRF_L = (HuntCrossleyForce&) (model.getForceSet().get("grf_l"));
    Array<double> tmpR = GRF_R.getRecordValues(s);
    Array<double> tmpL = GRF_L.getRecordValues(s);
    SimTK::Vector VGRF(nGRF);
    VGRF[0] = -tmpR[0]; VGRF[1] = -tmpR[1]; VGRF[2] = -tmpR[2];
    VGRF[3] = -tmpL[0]; VGRF[4] = -tmpL[1]; VGRF[5] = -tmpL[2];

    // Stitch together values into a large vector V
    SimTK::Vector V(nS+nU+nA+nMP*nA+nC+nMC+nCLF+nGRF);
    V(0, nS) = VS;
    V(nS, nU) = VU;
    V(nS+nU, nA) = VA;
    V(nS+nU+nA, nMP*nA) = VMUSC;
    V(nS+nU+nA+nMP*nA, nC) = VT;
    V(nS+nU+nA+nMP*nA+nC, nMC) = VMC;
    V(nS+nU+nA+nMP*nA+nC+nMC, nCLF) = VCLF;
    V(nS+nU+nA+nMP*nA+nC+nMC+nCLF, nGRF) = VGRF;
/*	if (VGRF[1] > 2000 || VGRF[5] > 2000) 
std::cout << s.getTime() << " " << VGRF << std::endl;*/
    _results->append(s.getTime(), V);
}



//_____________________________________________________________________________
/**
 * Perform an inverse dynamics analysis and append the model generalized
 * forces (due to muscles and torque motors) into the history storage.
 */
SimTK::Vector PredictiveOptimizationSystem::
    getInverseDynamicsLoad(const Model& model, const SimTK::State& s) const
{
    SimTK::Vector residualMobilityForces;
    
    // Disable the model actuators in the model and state.
    // We assume that these actuators are muscle & torque actuators
    // that are to be solved for in the generalized joint space.
    // I.e. this is an inverse dynamics analysis.
    SimTK::State sTmp = s;         // copy the state
    const int nA = model.getActuators().getSize();

    for (int i=0; i<nA; ++i) {
        Actuator& act = model.getActuators()[i];
        act.setDisabled(sTmp, true);
    }
    model.getMultibodySystem().realize(sTmp, SimTK::Stage::Acceleration);

    // Get all (enabled) applied mobility (generalized) forces applied to  modelthe. 
    // i.e, coordinate limit forces
    const SimTK::Vector &appliedMobilityForces = model.getMultibodySystem().getMobilityForces(s, SimTK::Stage::Dynamics);
		
	// Get all (enabled) applied body forces applied to the model. 
    // i.e, gravity, contact, force perturbations.
	const SimTK::Vector_<SimTK::SpatialVec>& appliedBodyForces = model.getMultibodySystem().getRigidBodyForces(s, SimTK::Stage::Dynamics);

    // Solve the inverse dynamics problem, i.e. solve for the generalized
    // forces due to the model actuators that we disabled previously. This 
    // analysis ignores coriolis and constraint forces.
    model.getMatterSubsystem().calcResidualForce(sTmp, appliedMobilityForces, 
        appliedBodyForces, sTmp.getUDot(), sTmp.getMultipliers(), residualMobilityForces);

    // Re-enable the actuators in the model and state.
    for (int i=0; i<nA; ++i) {
        Actuator& act = model.getActuators()[i];
        act.setDisabled(sTmp, false);
    }

    // Rerealize the original state (with all actuators enabled).
    model.getMultibodySystem().realize(s, SimTK::Stage::Acceleration);

    return -residualMobilityForces;
}



//_____________________________________________________________________________
/**
 * Print all muscle masses.
 */
void PredictiveOptimizationSystem::printMuscleMasses(const Model& m) const
{
    double totalMass = 0.0;
    const ForceSet& fs = m.getForceSet(); 
    for (int i=0; i<fs.getSize(); ++i) {
        SimpleMuscle* musc = dynamic_cast<SimpleMuscle*>(&fs[i]);
        if (musc) {
            totalMass += musc->getMass();
            cout << musc->getName() << " mass = " << musc->getMass() << " kg" << endl;
        }
    }
    cout << "TOTAL muscle mass = " << totalMass << " kg" << endl;
}
