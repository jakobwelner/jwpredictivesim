////   STATES
// (0): lStance
// (1): lSwingPrep
// (2): rStance
// (3): rSwingPrep

#include <iostream>
#include "GeyerHerrController.h"
#include "SimpleMuscle.h"
#include "Utils.h"
#include <cmath>

#define COM_Delay 0.0 // No delay for COM reading?

using namespace std;
using namespace SimTK;
using namespace OpenSim; 

static const char* const MTUS[] = { 
	"GMAX", "ILPSO", "HAMS", "RF", "VAS", "GAS", "SOL", "TA"
}; 

GeyerHerrController::GeyerHerrController() : Controller() {
	_state = -1;
	_stateStartTime = -1.0;
	_stateHistory.clear();
	_contactStateHistory.clear(); 
	
	for (int i = 0; i < NUM_MTUS; i++) {
		_prevF[i].clear(); 
		_prevl[i].clear();
		_act[i] = NULL;
	}
	_prevGRFLeft.clear(); 
	_prevGRFRight.clear(); 
	_prevKneeAngleRight.clear();  
	_prevKneeAngleLeft.clear();  
	_prevHipAngleRight.clear();  
	_prevHipAngleLeft.clear();  
	_prevTrunkAngle.clear(); 
	_prevCOMX.clear();   
	_prevCOMVX.clear();   
	_prevAnkleLocRightX.clear();  
	_prevAnkleLocLeftX.clear();  

    _stance[0] = false;
    _stance[1] = false;
    _stance_prep[0] = false;
    _stance_prep[1] = false;
    _swing_init[0] = false;
    _swing_init[1] = false;
    _d[0] = NaN;
    _d[1] = NaN;
		
	_trailingLegIdx = 0;
	_PTO = 0.105; 
	_totalMetabolics = 0; 	
	_totalLigamentTorque2 = 0; 
	 
	for (int i = 0; i < NUM_MTUS/2; i++) {
		_p[i] = 0;	// previous muscle activation, stance phase
		_q[i] = 0;	// previous muscle activation, swing phase
	}
	_G_sol = 0;
	_G_ta = 0;
	_l_off_ta = 0;
	_G_solta = 0;
	_G_gas = 0;
	_G_vas = 0;
	_k_phi = 0;
	_phi_k_off = 0;
	_theta_ref = 0;
	_G_ham = 0;
	_G_glu = 0;
	_G_hfl = 0;
	_G_hamhfl = 0;
	_l_off_hfl = 0;
	_l_off_ham = 0;
	_k_lean = 0;
	_k_p_glu = 0;
	_k_d_glu = 0;
	_k_p_hfl = 0;
	_k_d_hfl = 0;
	_k_p_ham = 0;
	_k_d_ham = 0;
	_Delta_S_glu = 0;
	_Delta_S_hfl = 0;
	_Delta_S_vas = 0;
	_Delta_S_rf = 0;
	_k_p_glu_sp = 0; 
	_k_d_glu_sp = 0; 
	_k_p_hfl_sp = 0;
	_k_d_hfl_sp = 0;
	_k_p_vas_sp = 0;
	_k_d_vas_sp = 0;
	_htheta_ref_sp = 0; 
	_phi_ref_sp = 0;
	_sp_threshold = -100;	// When to transition to stance-prep. Compared with (COMX - anklePosX), optimized
	_simbicon_cd = 0;  
	_simbicon_cv = 0;  
}
	

// updating muscle states
void GeyerHerrController::computeControls(const SimTK::State& s, SimTK::Vector& controls) const
{
	// Using delay of 0.01 ms for 
	//	- stance phase update
	//	- phi and phiv (what is this?)
	// Using delay of 0.005 ms for
	//	- htheta and hthetav (what is this?)
	//	- theta and thetav (what is this?)
	// Using 0 delay for (This could be assumed as correct predictive proprioception)
	//	- ankle position and velocity
	//	- COM position and velocity

	// 3 phases: stance, stance_prep, swing_init, (double support)

#if 1
	_stance[0] = (getPreviousDouble(&_prevGRFRight, 0.01) > 0); 
	_stance[1] = (getPreviousDouble(&_prevGRFLeft, 0.01) > 0);

	double phi[2], phiv[2];  
	getPreviousDouble(&_prevKneeAngleRight, 0.01, phi[0], phiv[0]);
	getPreviousDouble(&_prevKneeAngleLeft, 0.01, phi[1], phiv[1]); 
	
	double htheta[2], hthetav[2];  
	getPreviousDouble(&_prevHipAngleRight, 0.005, htheta[0], hthetav[0]);
	getPreviousDouble(&_prevHipAngleLeft, 0.005, htheta[1], hthetav[1]); 
	
	double theta, thetav; 
	getPreviousDouble(&_prevTrunkAngle, 0.005, theta, thetav);

	double comx, comvx; // No delay - assuming predictive proprioception?
	getPreviousDouble(&_prevCOMX, COM_Delay, comx, comvx);

	double ankle[2], anklev[2]; // No delay - assuming predictive proprioception?
	getPreviousDouble(&_prevAnkleLocRightX, COM_Delay, ankle[0], anklev[0]);
	getPreviousDouble(&_prevAnkleLocLeftX, COM_Delay, ankle[1], anklev[1]);
	
	// Difference in COM and ankle position on x-axis. Maybe 1.079 is leg-length?
	_d[0] = (comx - ankle[0])/1.079; 
	_d[1] = (comx - ankle[1])/1.079; 

	// Double support phase
	bool dSupport = (_stance[0] && _stance[1]); 

	// Update MTU's
	for (int j = 0; j < 2; j++) {
		// If current leg is in Stance Phase
		if (_stance[j]) {
			_swing_init[j] = false; 

			// If in double support
			if ( dSupport ) {
				_swing_init[j] = true; 
			}
			// Updating activation of all MTU's on current side
			for (int i = 0; i < NUM_MTUS/2; i++) {
				int idx = j*NUM_MTUS/2 + i;
				double u = _p[i]; 
				if (!_act[idx]) 
					continue;
				// GMAX
				if (i == 0) {
					double L = _k_p_glu*(theta - _theta_ref) + _k_d_glu*thetav;
					if (L > 0) {
						u += L; 
					}
					if (_trailingLegIdx == j) {
						if (dSupport) {
							u = _p[i] - _Delta_S_glu; 
						}
						else if (_swing_init[j]) {
							u -= _Delta_S_glu; 
						}
					}
				}
				// ILPSO
				if (i == 1) {
					double L = _k_p_hfl*(theta - _theta_ref) + _k_d_hfl*thetav;
					if (L < 0) {
						u -= L; 
					}
					if (_trailingLegIdx == j) {
						if (dSupport) {
							u = _p[i] + _Delta_S_hfl; 
						}
						else if (_swing_init[j]) {
							u += _Delta_S_hfl; 
						}
					}
				}
				// HAMS
				if (i == 2) {
					double L = _k_p_ham*(theta - _theta_ref) + _k_d_ham*thetav;
					if (L > 0) {
						u += L; 
					}
					if (dSupport && _trailingLegIdx == j) {
						u = _p[i]; 
					}
				}
				// RF
				if (i == 3) {
					if (_swing_init[j] && _trailingLegIdx == j) {
						u += _Delta_S_rf; 
					}
				}
				// VAS
				if (i == 4) {
					u += _G_vas*getF(idx, 0.01);
					double L = phi[j] - _phi_k_off; 
					if (L > 0 && phiv[j] > 0) {
						u -= _k_phi*L; 
					}
					if (_swing_init[j] && _trailingLegIdx == j) {
						u -= _Delta_S_vas; 
					}
				}
				// GAS
				if (i == 5) {
					u += _G_gas*getF(idx, 0.02);
				}
				// SOL
				if (i == 6) {
					u += _G_sol*getF(idx, 0.02);
				}
				// TA
				if (i == 7) {
					u += _G_ta*getL(idx, 0.02, _l_off_ta)
						- _G_solta*getF(idx-1, 0.02);  // Negative forcefeedback from SOL
				}
				setControl(_act[idx], u, controls);
			}
		}
		// If current leg is in Swing Phase
		else {
			_stance_prep[j] = false; 

			// If swing leg ankle-position is more than _sp_threshold away from COM on x-axis
			if (_d[j] < _sp_threshold) {
				_stance_prep[j] = true; 
			}
			
			//if (j == 0) {  // right leg
			//	cout << s.getTime() << ": " << _d[j] << "   (" << _stance_prep[j] << ")" << endl;
			//}

            for (int i = 0; i < NUM_MTUS/2; i++) {
				int idx = j*NUM_MTUS/2 + i; 
				if (!_act[idx]) 
					continue;
				double u = _q[i];  

				// GMAX
				if (i == 0) {
					if (_stance_prep[j]) {
						double hip_target = _htheta_ref_sp - 
							(_simbicon_cd*(comx - ankle[(j+1)%2]) + _simbicon_cv*comvx);
                        //cout << "t=" << s.getTime() << " hiptarget = " << hip_target << endl;
						double L = _k_p_glu_sp*(htheta[j] - hip_target) + 
							_k_d_glu_sp*hthetav[j];
						if (L < 0) {
							u -= L; 
						}
					}
					else { 
						u += _G_glu*getF(idx, 0.005);
					}
				}
				// ILPSO
				if (i == 1) {
					if (_stance_prep[j]) {
						double hip_target = _htheta_ref_sp - 
							(_simbicon_cd*(comx - ankle[(j+1)%2]) + _simbicon_cv*comvx);
						double L = _k_p_hfl_sp*(htheta[j] - hip_target) + 
							_k_d_hfl_sp*hthetav[j];
						if (L > 0) {
							u += L; 
						}
					}
					else { 
						u += (_G_hfl*getL(idx, 0.005, _l_off_hfl) 
								- _G_hamhfl*getL(idx+1, 0.005, _l_off_ham) + 
								_k_lean*(_PTO - _theta_ref)); 
					}
					
				}
				// HAMS
				if (i == 2) {
					u += _G_ham*getF(idx, 0.005);
//					std::cout << _G_ham << " " << getF(idx, 0.005) << " " << _q[i] << std::endl;
				}
				// RF
				if (i == 3) {
				}
				// VAS
				if (i == 4) {
					if (_stance_prep[j]) {
						double L = _k_p_vas_sp*(phi[j] - _phi_ref_sp) + 
							_k_d_vas_sp*phiv[j];
						if (L < 0) {
							u -= L; 
                            //cout << "t=" << s.getTime() << endl;
                            //cout << "_phi_ref_sp = " << _phi_ref_sp << endl;
                            //cout << "_k_p_vas_sp = " << _k_p_vas_sp << endl;
                            //cout << "_k_d_vas_sp = " << _k_d_vas_sp << endl;
                            //cout << "phi[j] = " << phi[j] << endl;
                            //cout << "phiv[j] = " << phiv[j] << "\n" << endl;
                            //cout << "u = " << u << "\n" << endl;
                            //cout << "L = " << L << "\n" << endl;
						}
					}
				}
				// GAS
				if (i == 5) {
				}
				// SOL
				if (i == 6) {
				}
				// TA
				if (i == 7) {
					u += _G_ta*getL(idx, 0.02, _l_off_ta); 
				}
				setControl(_act[idx], u, controls);
			}
		}
	}
	
	// Don't know what this is (the osim model doesn't seem to include this so it may be a leftover)
	// CoordinateActuator - maybe used for perturbations previously
	const Set<Actuator>& actSet = getActuatorSet();
	if (actSet.contains("mtp_r_actuator")) {
		const CoordinateActuator* cact = static_cast<const CoordinateActuator*>(&actSet.get("mtp_r_actuator")); 
		Coordinate* coord = cact->getCoordinate();
		setControl(cact, 30*(0.0 - coord->getValue(s)) - 3*coord->getSpeedValue(s), controls);  		
	}
	if (actSet.contains("mtp_l_actuator")) {
		const CoordinateActuator* cact = static_cast<const CoordinateActuator*>(&actSet.get("mtp_l_actuator")); 
		Coordinate* coord = cact->getCoordinate();
		setControl(cact, 30*(0.0 - coord->getValue(s)) - 3*coord->getSpeedValue(s), controls);  	
	}
#endif
}
	
// Saving values in stack for later retrieval
void GeyerHerrController::recordDelayedValues(const SimTK::State& s) {	
	for (int i = 0; i < NUM_MTUS; i++) {
		if (!_act[i]) 
			continue; 
		
		_prevF[i].push_back(_act[i]->getForce(s));
		_prevl[i].push_back(_act[i]->getStateVariable(s, "fiber_length"));
	}

	
	if (typeid(_model->getForceSet()[0]) == 
			typeid(OpenSim::HuntCrossleyForce)) {
		OpenSim::HuntCrossleyForce* hc_r = static_cast<OpenSim::HuntCrossleyForce*>(&_model->getForceSet()[0]);
		OpenSim::HuntCrossleyForce* hc_l = static_cast<OpenSim::HuntCrossleyForce*>(&_model->getForceSet()[1]);
		_prevGRFLeft.push_back(-hc_l->getRecordValues(s).get(1)); 
		_prevGRFRight.push_back(-hc_r->getRecordValues(s).get(1));
	}
	else {
		_prevGRFLeft.push_back(0); 
		_prevGRFRight.push_back(0);
	}

	 
	_prevKneeAngleRight.push_back(_model->getCoordinateSet().get("knee_r_extension").getValue(s) + SimTK::Pi); 
	_prevKneeAngleLeft.push_back(_model->getCoordinateSet().get("knee_l_extension").getValue(s) + SimTK::Pi); 
	
	_prevHipAngleRight.push_back(-_model->getCoordinateSet().get("hip_r_flexion").getValue(s) + SimTK::Pi); 
	_prevHipAngleLeft.push_back(-_model->getCoordinateSet().get("hip_l_flexion").getValue(s) + SimTK::Pi); 


	SimTK::Vec3 result; 
	const BodySet& bodySet = _model->getBodySet();
	const Body& pelvis = bodySet.get("pelvis");
	getUpVectorInGround( *_model, s, pelvis, result ); 
	
	double trunk_angle = -(acos(SimTK::dot(result.normalize(), SimTK::Vec3(1.0, 0.0, 0.0))) - SimTK::Pi/2); 
	_prevTrunkAngle.push_back(trunk_angle);
	
	const SimTK::SimbodyMatterSubsystem& matter = _model->getMatterSubsystem();
	
	const Body& foot_r = bodySet.get("foot_r");
	SimTK::Vec3 ankleLocR = matter.getMobilizedBody(foot_r.getIndex()).findStationLocationInGround(s, SimTK::Vec3(0, 0, 0)); 
	
	const Body& foot_l = bodySet.get("foot_l");
	SimTK::Vec3 ankleLocL = matter.getMobilizedBody(foot_l.getIndex()).findStationLocationInGround(s, SimTK::Vec3(0, 0, 0)); 

	_prevAnkleLocRightX.push_back(ankleLocR[0]); 
	_prevAnkleLocLeftX.push_back(ankleLocL[0]); 
	
	SimTK::Vec3 com = matter.calcSystemMassCenterLocationInGround(s);
	_prevCOMX.push_back(com[0]); 
	
	SimTK::Vec3 comv = matter.calcSystemMassCenterVelocityInGround(s);
	_prevCOMVX.push_back(comv[0]); 
	
	_trailingLegIdx = 0; 
	if (ankleLocR[0] > ankleLocL[0]) 
		_trailingLegIdx = 1;

	if (_trailingLegIdx == 0) {
		if (_prevGRFRight.size() > 1) {
			if (_prevGRFRight[_prevGRFRight.size()-1] <= 0 && 
				_prevGRFRight[_prevGRFRight.size()-2] > 0)
				_PTO = trunk_angle;  
		}
	}
	
	if (_trailingLegIdx == 1) {
		if (_prevGRFLeft.size() > 1) {
			if (_prevGRFLeft[_prevGRFLeft.size()-1] <= 0 && 
				_prevGRFLeft[_prevGRFLeft.size()-2] > 0)
				_PTO = trunk_angle;  
		}
	}
	 
}
	
// Get previous actuator force
double GeyerHerrController::getF(int idx, double delay) const  {
	return getPreviousDouble(&_prevF[idx], delay)/
		_act[idx]->get_Fmax(); 
}

// Get previous actuator length normalized by optimal length - offset
double GeyerHerrController::getL(int idx, double delay, double offset) const  {
	double L = getPreviousDouble(&_prevl[idx], delay)/_act[idx]->get_Lopt() - offset;

	if (L < 0) 
		L = 0; 
	return L; 
}

// Reading all MTU control parameters from storage object into local variables
void GeyerHerrController::setControlParams( Storage& simParamStorage ) {
	Set<Actuator>& actSet = updActuators();

	// Verifying that the actuators exist in the current actuator set
	// Adding existing actuators to local variable
	for (int i = 0; i < NUM_MTUS/2; i++) {
		std::string actname_r = MTUS[i]; 
		actname_r += "_r"; 
		if (actSet.contains(actname_r)) 
			_act[i] = static_cast<SimpleMuscle*>(&actSet.get(actname_r));  
		
		std::string actname_l = MTUS[i]; 
		actname_l += "_l"; 
		if (actSet.contains(actname_l)) 
			_act[i+NUM_MTUS/2] = static_cast<SimpleMuscle*>(&actSet.get(actname_l));  
	}
	
	Array<double>& allSimParams = 
		simParamStorage.getLastStateVector()->getData();
	
	// Not sure what _p value are
	for (int i = 0; i < NUM_MTUS/2; i++) {
		std::string actname = std::string(MTUS[i]) + std::string("_p");
		int idx = simParamStorage.getStateIndex(actname);
		if (idx >= 0) { 
			_p[i] = allSimParams.get(idx);
		}
	}
	
	// Not sure what _q values are
	for (int i = 0; i < NUM_MTUS/2; i++) {
		std::string actname = std::string(MTUS[i]) + std::string("_q");
		int idx = simParamStorage.getStateIndex(actname);
		if (idx >= 0) { 
			_q[i] = allSimParams.get(idx);
		}
	}
	// Read MTU parameters from storage file
	int idx = simParamStorage.getStateIndex("G_sol");
	if (idx >= 0) 
		_G_sol = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("G_ta");
	if (idx >= 0) 
		_G_ta = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("l_off_ta");
	if (idx >= 0) 
		_l_off_ta = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("G_solta");
	if (idx >= 0) 
		_G_solta = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("G_gas");
	if (idx >= 0) 
		_G_gas = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("G_vas");
	if (idx >= 0) 
		_G_vas = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("k_phi");
	if (idx >= 0) 
		_k_phi = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("phi_k_off");
	if (idx >= 0) 
		_phi_k_off = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("theta_ref");
	if (idx >= 0) {
		_theta_ref = allSimParams.get(idx);
		_PTO = _theta_ref; 
	}
	idx = simParamStorage.getStateIndex("G_ham");
	if (idx >= 0) 
		_G_ham = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("G_glu");
	if (idx >= 0) 
		_G_glu = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("G_hfl");
	if (idx >= 0) 
		_G_hfl = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("G_hamhfl");
	if (idx >= 0) 
		_G_hamhfl = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("l_off_hfl");
	if (idx >= 0) 
		_l_off_hfl = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("l_off_ham");
	if (idx >= 0) 
		_l_off_ham = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("k_lean");
	if (idx >= 0) 
		_k_lean = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("k_p_glu");
	if (idx >= 0) 
		_k_p_glu = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("k_d_glu");
	if (idx >= 0) 
		_k_d_glu = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("k_p_hfl");
	if (idx >= 0) 
		_k_p_hfl = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("k_d_hfl");
	if (idx >= 0) 
		_k_d_hfl = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("k_p_ham");
	if (idx >= 0) 
		_k_p_ham = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("k_d_ham");
	if (idx >= 0) 
		_k_d_ham = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("Delta_S_glu");
	if (idx >= 0) 
		_Delta_S_glu = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("Delta_S_hfl");
	if (idx >= 0) 
		_Delta_S_hfl = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("Delta_S_rf");
	if (idx >= 0) 
		_Delta_S_rf = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("Delta_S_vas");
	if (idx >= 0) 
		_Delta_S_vas = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("k_p_glu_sp");
	if (idx >= 0) 
		_k_p_glu_sp = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("k_d_glu_sp");
	if (idx >= 0) 
		_k_d_glu_sp = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("k_p_hfl_sp");
	if (idx >= 0) 
		_k_p_hfl_sp = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("k_d_hfl_sp");
	if (idx >= 0) 
		_k_d_hfl_sp = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("k_p_vas_sp");
	if (idx >= 0) 
		_k_p_vas_sp = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("k_d_vas_sp");
	if (idx >= 0) 
		_k_d_vas_sp = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("htheta_ref_sp");
	if (idx >= 0) 
		_htheta_ref_sp = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("phi_ref_sp");
	if (idx >= 0) 
		_phi_ref_sp = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("sp_threshold");
	if (idx >= 0) 
		_sp_threshold = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("simbicon_cd");
	if (idx >= 0) 
		_simbicon_cd = allSimParams.get(idx);

	idx = simParamStorage.getStateIndex("simbicon_cv");
	if (idx >= 0) 
		_simbicon_cv = allSimParams.get(idx);
}


//=============================================================================
// VISUALIZER GEOMETRY
//=============================================================================
void GeyerHerrController::generateDecorations(bool fixed, const ModelDisplayHints& hints, 
    const State& s, Array_<SimTK::DecorativeGeometry>& geometry) const
{
    Super::generateDecorations(fixed, hints, s, geometry); 

    // There is no fixed geometry to generate here.
    if (fixed) { return; }

    string mode[2];
    for (int i=0; i<2; ++i) {
        if (_stance_prep[i] && !_stance[i])
            mode[i] = "STANCE-PREP";
        else if (_swing_init[i] && _stance[i] && _trailingLegIdx == i)
            mode[i] = "SWING-INIT";
        else if (_stance[i]) 
            mode[i] = "STANCE-REG";
        else
            mode[i] = "SWING-REG";
    }


    // Gait mode text.
    char gaitModeTxt[200];
    sprintf(gaitModeTxt, "RightLeg: %s (d=%.4f),   LeftLeg: %s (d=%.4f)", 
        mode[0].c_str(), _d[0], mode[1].c_str(), _d[1]);
    DecorativeText gaitMode(gaitModeTxt);
    gaitMode.setIsScreenText(true);
    geometry.push_back(gaitMode);

    char gaitModeTxt2[200];
    sprintf(gaitModeTxt2, "Threshold: SP=%.4f)", _sp_threshold);
    DecorativeText gaitMode2(gaitModeTxt2);
    gaitMode2.setIsScreenText(true);
    geometry.push_back(gaitMode2);
}


void GeyerHerrController::printInnerState() {
	cout << "_state: " << _state << endl;
}

void GeyerHerrController::saveControllerState(const SimTK::State& s) {
	cout << "saveControllerState is Disabled" << endl;
	/*
	cout << endl << "Saving state variables from GHC" << endl << endl;

	//// Printing variables to file
	writeVectorToFile("_prevF", _prevF, 16); // Muscle Force Vector
	writeVectorToFile("_prevl", _prevl, 16); // Muscle FiberLength Vector
	writeVectorToFile("_prevGRFLeft", _prevGRFLeft);
	writeVectorToFile("_prevGRFRight", _prevGRFRight);
	writeVectorToFile("_prevKneeAngleRight", _prevKneeAngleRight);
	writeVectorToFile("_prevKneeAngleLeft", _prevKneeAngleLeft);
	writeVectorToFile("_prevTrunkAngle", _prevTrunkAngle);
	writeVectorToFile("_prevHipAngleRight", _prevHipAngleRight);
	writeVectorToFile("_prevHipAngleLeft", _prevHipAngleLeft);
	writeVectorToFile("_prevCOMX", _prevCOMX);
	writeVectorToFile("_prevCOMVX", _prevCOMVX);
	writeVectorToFile("_prevAnkleLocRightX", _prevAnkleLocRightX);
	writeVectorToFile("_prevAnkleLocLeftX", _prevAnkleLocLeftX);
	writeVectorToFile("_p", _p, 16);
	writeVectorToFile("_q", _q, 16);
	writeDoubleToFile("_state", _state);
	
	// Saving out negative duration to work with reset time when loaded at t=0
	double negDuration = -1*(s.getTime() - _stateStartTime); 
	writeDoubleToFile("_stateStartTime", negDuration);
	writeDoubleToFile("_stance[0]", _stance[0]);
	writeDoubleToFile("_stance[1]", _stance[1]);
	writeDoubleToFile("_stance_prep[0]", _stance_prep[0]);
	writeDoubleToFile("_stance_prep[1]", _stance_prep[1]);
	writeDoubleToFile("_swing_init[0]", _swing_init[0]);
	writeDoubleToFile("_swing_init[1]", _swing_init[1]);
	writeDoubleToFile("_d[0]", _d[0]);
	writeDoubleToFile("_d[1]", _d[1]);
	writeDoubleToFile("_trailingLegIdx", _trailingLegIdx);
	writeDoubleToFile("_PTO", _PTO);
	*/

#if 0
	// Double Variables
	writeDoubleToFile("_G_sol", _G_sol);
	writeDoubleToFile("_G_ta", _G_ta);
	writeDoubleToFile("_l_off_ta", _l_off_ta);
	writeDoubleToFile("_G_solta", _G_solta);
	writeDoubleToFile("_G_gas", _G_gas);
	writeDoubleToFile("_G_vas", _G_vas);
	writeDoubleToFile("_k_phi", _k_phi);
	writeDoubleToFile("_k_p_glu", _k_p_glu);
	writeDoubleToFile("_k_d_glu", _k_d_glu);
	writeDoubleToFile("_phi_k_off", _phi_k_off);
	writeDoubleToFile("_theta_ref", _theta_ref);
	writeDoubleToFile("_G_ham", _G_ham);
	writeDoubleToFile("_G_glu", _G_glu);
	writeDoubleToFile("_G_hfl", _G_hfl);
	writeDoubleToFile("_G_hamhfl", _G_hamhfl);
	writeDoubleToFile("_l_off_hfl", _l_off_hfl);
	writeDoubleToFile("_l_off_ham", _l_off_ham);
	writeDoubleToFile("_k_lean", _k_lean);
	writeDoubleToFile("_k_p_hfl", _k_p_hfl);
	writeDoubleToFile("_k_d_hfl", _k_d_hfl);
	writeDoubleToFile("_k_p_ham", _k_p_ham);
	writeDoubleToFile("_k_d_ham", _k_d_ham);
	writeDoubleToFile("_Delta_S_glu", _Delta_S_glu);
	writeDoubleToFile("_Delta_S_hfl", _Delta_S_hfl);
	writeDoubleToFile("_Delta_S_rf", _Delta_S_rf);
	writeDoubleToFile("_Delta_S_vas", _Delta_S_vas);
	writeDoubleToFile("_k_p_glu_sp", _k_p_glu_sp);
	writeDoubleToFile("_k_d_glu_sp", _k_d_glu_sp);
	writeDoubleToFile("_k_p_hfl_sp", _k_p_hfl_sp);
	writeDoubleToFile("_k_d_hfl_sp", _k_d_hfl_sp);
	writeDoubleToFile("_k_p_vas_sp", _k_p_vas_sp);
	writeDoubleToFile("_k_d_vas_sp", _k_d_vas_sp);
	writeDoubleToFile("_htheta_ref_sp", _htheta_ref_sp);
	writeDoubleToFile("_phi_ref_sp", _phi_ref_sp);
	writeDoubleToFile("_sp_threshold", _sp_threshold);
	writeDoubleToFile("_simbicon_cd", _simbicon_cd);
	writeDoubleToFile("_simbicon_cv", _simbicon_cv);
#endif
}



void GeyerHerrController::loadControllerState() {
	cout << "loadControllerState is Disabled" << endl;
	/*
	cout << endl << "Loading state variables into GHC" << endl;
	
	readVarFromFile("_prevF", _prevF, 16);
	readVarFromFile("_prevl", _prevl, 16);
	readVarFromFile("_prevGRFLeft", _prevGRFLeft);
	readVarFromFile("_prevGRFRight", _prevGRFRight);
	readVarFromFile("_prevKneeAngleRight", _prevKneeAngleRight);
	readVarFromFile("_prevKneeAngleLeft", _prevKneeAngleLeft);
	readVarFromFile("_prevTrunkAngle", _prevTrunkAngle);
	readVarFromFile("_prevHipAngleRight", _prevHipAngleRight);
	readVarFromFile("_prevHipAngleLeft", _prevHipAngleLeft);
	readVarFromFile("_prevCOMX", _prevCOMX);
	readVarFromFile("_prevCOMVX", _prevCOMVX);
	readVarFromFile("_prevAnkleLocRightX", _prevAnkleLocRightX);
	readVarFromFile("_prevAnkleLocLeftX", _prevAnkleLocLeftX);
	readVarFromFile("_p", _p, 16);
	readVarFromFile("_q", _q, 16);

	readDoubleFromFile("_state", _state);
	readDoubleFromFile("_stateStartTime", _stateStartTime);
	readDoubleFromFile("_stance[0]", _stance[0]);
	readDoubleFromFile("_stance[1]", _stance[1]);
	readDoubleFromFile("_stance_prep[0]", _stance_prep[0]);
	readDoubleFromFile("_stance_prep[1]", _stance_prep[1]);
	readDoubleFromFile("_swing_init[0]", _swing_init[0]);
	readDoubleFromFile("_swing_init[1]", _swing_init[1]);
	readDoubleFromFile("_d[0]", _d[0]);
	readDoubleFromFile("_d[1]", _d[1]);
	readDoubleFromFile("_trailingLegIdx", _trailingLegIdx);
	readDoubleFromFile("_PTO", _PTO);
	*/
	
	
#if 0
	readDoubleFromFile("_G_sol", _G_sol);
	readDoubleFromFile("_G_ta", _G_ta);
	readDoubleFromFile("_l_off_ta", _l_off_ta);
	readDoubleFromFile("_G_solta", _G_solta);
	readDoubleFromFile("_G_gas", _G_gas);
	readDoubleFromFile("_G_vas", _G_vas);
	readDoubleFromFile("_k_phi", _k_phi);
	readDoubleFromFile("_k_p_glu", _k_p_glu);
	readDoubleFromFile("_k_d_glu", _k_d_glu);
	readDoubleFromFile("_phi_k_off", _phi_k_off);
	readDoubleFromFile("_theta_ref", _theta_ref);
	readDoubleFromFile("_G_ham", _G_ham);
	readDoubleFromFile("_G_glu", _G_glu);
	readDoubleFromFile("_G_hfl", _G_hfl);
	readDoubleFromFile("_G_hamhfl", _G_hamhfl);
	readDoubleFromFile("_l_off_hfl", _l_off_hfl);
	readDoubleFromFile("_l_off_ham", _l_off_ham);
	readDoubleFromFile("_k_lean", _k_lean);
	readDoubleFromFile("_k_p_hfl", _k_p_hfl);
	readDoubleFromFile("_k_d_hfl", _k_d_hfl);
	readDoubleFromFile("_k_p_ham", _k_p_ham);
	readDoubleFromFile("_k_d_ham", _k_d_ham);
	readDoubleFromFile("_Delta_S_glu", _Delta_S_glu);
	readDoubleFromFile("_Delta_S_hfl", _Delta_S_hfl);
	readDoubleFromFile("_Delta_S_rf", _Delta_S_rf);
	readDoubleFromFile("_Delta_S_vas", _Delta_S_vas);
	readDoubleFromFile("_k_p_glu_sp", _k_p_glu_sp);
	readDoubleFromFile("_k_d_glu_sp", _k_d_glu_sp);
	readDoubleFromFile("_k_p_hfl_sp", _k_p_hfl_sp);
	readDoubleFromFile("_k_d_hfl_sp", _k_d_hfl_sp);
	readDoubleFromFile("_k_p_vas_sp", _k_p_vas_sp);
	readDoubleFromFile("_k_d_vas_sp", _k_d_vas_sp);
	readDoubleFromFile("_htheta_ref_sp", _htheta_ref_sp);
	readDoubleFromFile("_phi_ref_sp", _phi_ref_sp);
	readDoubleFromFile("_sp_threshold", _sp_threshold);
	readDoubleFromFile("_simbicon_cd", _simbicon_cd);
	readDoubleFromFile("_simbicon_cv", _simbicon_cv);
#endif
}


void GeyerHerrController::saveMuscleStates(Model& m, SimTK::State& s){

	cout << "Saving Muscle States to File" << endl;
	std::string muscList[NUM_MTUS];

	for (int i = 0; i < NUM_MTUS / 2; i++) {
		std::string actname = MTUS[i];
		muscList[i] = actname + "_r";
		muscList[i+(NUM_MTUS/2)] = actname + "_l";
	}

	for (int i = 0; i < NUM_MTUS; i++) {
		SimpleMuscle& musc = (SimpleMuscle&)(m.updForceSet().get(muscList[i]));
		printMuscleToFile(s, musc, muscList[i]);
		//mv.u = 1;
		//musc.setMuscleVars(mv);
	}
}


void GeyerHerrController::loadMuscleStates(Model& m) {
	cout << "loadMuscleState is Disabled" << endl;
	/*
	std::string muscList[NUM_MTUS];

	for (int i = 0; i < NUM_MTUS / 2; i++) {
		std::string actname = MTUS[i];
		muscList[i] = actname + "_r";
		muscList[i + (NUM_MTUS / 2)] = actname + "_l";
	}

	for (int i = 0; i < NUM_MTUS; i++) {
		SimpleMuscle& musc = (SimpleMuscle&)(m.updForceSet().get(muscList[i]));

		double values[20];
		readVarFromFile("muscleState_" + muscList[i], values, 20);
		
		OpenSim::MuscVars newMuscVars;

		newMuscVars.l_ce = values[0];			// STATE: fiber length
		newMuscVars.a = values[1];				// STATE: activation
		newMuscVars.u = values[2];				// INPUT: excitation
		newMuscVars.theta[0] = values[3];		// gen coords
		newMuscVars.theta[1] = values[4];       // gen coords
		newMuscVars.thetadot[0] = values[5];    // gen speeds
		newMuscVars.thetadot[1] = values[6];    // gen speeds
		newMuscVars.momArm[0] = values[7];		// Moment Arm
		newMuscVars.momArm[0] = values[8];		// Moment Arm
		newMuscVars.mom[0] = values[9];			// Moment
		newMuscVars.mom[1] = values[10];		// Moment
		newMuscVars.l_mtu = values[11];			// MTU length
		newMuscVars.l_se = values[12];			// tendon length
		newMuscVars.F_se = values[13];			// tendon force
		newMuscVars.F_be = values[14];			// buffer elasticity
		newMuscVars.F_pe = values[15];			// passive force
		newMuscVars.fl = values[16];			// force length
		newMuscVars.fv = values[17];			// force velocity
		newMuscVars.fvInv = values[18];			// inverse force velocity
		newMuscVars.metabolicrate = values[19]; // metabolic rate

		cout << "Loading muscle state for " << muscList[i] << endl;
		musc.setMuscleVars(newMuscVars);

		
		//// Verify data:
		//OpenSim::MuscVars test;
		//test = musc.getMuscVars();
		//cout << muscList[i] << endl;
		//cout << "Activation: values[1]=" << values[1] << ", test.a=" << test.a << endl;
		//cout << "Exitation: values[2]=" << values[2] << ", test.u=" << test.u << endl;
		//cout << "MTU length: values[11]=" << values[11] << ", test.l_mtu=" << test.l_mtu << endl;
		
	}
*/
}


void GeyerHerrController::printMuscleToFile(SimTK::State& s, SimpleMuscle& musc, std::string muscle) {
	cout << "printMuscleToFile is Disabled" << endl;
	/*
	OpenSim::MuscVars mv = musc.getMuscVars();

	double values[20];

	values[0] = mv.l_ce;			// STATE: fiber length
	values[1] = mv.a;				// STATE: activation
	values[2] = mv.u;				// INPUT: excitation
	values[3] = mv.theta[0];		// gen coords
	values[4] = mv.theta[1];        // gen coords
	values[5] = mv.thetadot[0];     // gen speeds
	values[6] = mv.thetadot[1];     // gen speeds
	values[7] = mv.momArm[0];		// Moment Arm
	values[8] = mv.momArm[0];		// Moment Arm
	values[9] = mv.mom[0];			// Moment
	values[10] = mv.mom[1];			// Moment
	values[11] = mv.l_mtu;			// MTU length
	values[12] = mv.l_se;			// tendon length
	values[13] = mv.F_se;			// tendon force
	values[14] = mv.F_be;			// buffer elasticity
	values[15] = mv.F_pe;			// passive force
	values[16] = mv.fl;				// force length
	values[17] = mv.fv;				// force velocity
	values[18] = mv.fvInv;			// inverse force velocity
	values[19] = mv.metabolicrate;  // metabolic rate

	writeVectorToFile("muscleState_" + muscle, values, 20);
	*/
	/*
	FILE *fp;
	stringstream filename;
	filename << "testing/" << "muscleState" << "__" << muscle << ".csv";
	fp = fopen(filename.str().c_str(), "w");
	fprintf(fp, "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s\n",
		"time",
		"Fiber Length",
		"Activation",
		"Excitation",
		"Gen coords[0]",
		"Gen coords[1]",
		"Gen speeds[0]",
		"Gen speeds[1]",
		"Muscle Moment Arm[0]",
		"Muscle Moment Arm[1]",
		"Muscle Moment[0]",
		"Muscle Moment[1]",
		"MTU length",
		"Tendon Length",
		"Tendon Force",
		"buffer elasticity",
		"Passive Force",
		"Force Length",
		"Force Velocity",
		"Inverse Force Velocity",
		"Metabolic Rate"
		);
	fprintf(fp, "%0.4f, %0.4f, %0.4f, %0.4f, %0.4f, %0.4f, %0.4f, %0.4f, %0.4f, %0.4f, %0.4f, %0.4f, %0.4f, %0.4f, %0.4f, %0.4f, %0.4f, %0.4f, %0.4f, %0.4f, %0.4f\n",
		s.getTime(),
		l_ce,
		a,
		u,
		theta[0],
		theta[1],
		thetadot[0],
		thetadot[1],
		momArm[0],
		momArm[0],
		mom[0],
		mom[1],
		l_mtu,
		l_se,
		F_se,
		F_be,
		F_pe,
		fl,
		fv,
		fvInv,
		metabolicrate);
	fclose(fp);
	*/
}