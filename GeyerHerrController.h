#ifndef GEYER_HERR_H_
#define GEYER_HERR_H_

#include <OpenSim/OpenSim.h>

#define NUM_MTUS 16
#define OBJ_SKIP_FRAMES 10

namespace OpenSim {

class SimpleMuscle; 
class GeyerHerrController : public Controller
{
OpenSim_DECLARE_CONCRETE_OBJECT(GeyerHerrController, Controller);
friend class ControlStateHandler;  
public:
	GeyerHerrController();

	// Added by JW
	void saveControllerState(const SimTK::State& s);
	void loadControllerState();
	void printInnerState();
	void saveMuscleStates(Model& m, SimTK::State& s);
	void printMuscleToFile(SimTK::State& s, SimpleMuscle& musc, std::string muscle);
	void loadMuscleStates(Model& m);


	// Original
	void setControlParams( Storage& simParamStorage ); 
    void computeControls(const SimTK::State& s, SimTK::Vector& controls) const OVERRIDE_11;
	
	void recordDelayedValues(const SimTK::State& s);
	
	double getF(int idx, double delay) const; 
	double getL(int idx, double delay, double offset) const;
	
	double getTotalMetabolics() {
		return _totalMetabolics; 
	}
	
	double getTotalLigamentTorque2() {
		return _totalLigamentTorque2; 
	}
	
	const std::vector<SimTK::State>& getStateHistory() {
		return _stateHistory; 
	}
	const std::vector<SimTK::State>& getContactStateHistory() {
		return _contactStateHistory; 
	}
private:
    void generateDecorations(bool fixed, const ModelDisplayHints& hints, 
        const SimTK::State& s, 
        SimTK::Array_<SimTK::DecorativeGeometry>& geometry) const OVERRIDE_11;
	
	int getState() {
		return _state; 
	}
	void setState( int state, double time ) {
		_state = state; 
		_stateStartTime = time;
	}

	double getStateStartTime() {
		return _stateStartTime; 
	}
	
	int _state;
	double _stateStartTime; 
	std::vector<SimTK::State> _stateHistory; 
	std::vector<SimTK::State> _contactStateHistory; 

	std::vector<double> _prevF[NUM_MTUS]; 
	std::vector<double> _prevl[NUM_MTUS];
	std::vector<double> _prevGRFLeft; 
	std::vector<double> _prevGRFRight;
	std::vector<double> _prevKneeAngleRight;  
	std::vector<double> _prevKneeAngleLeft;  
	std::vector<double> _prevTrunkAngle;
	std::vector<double> _prevHipAngleRight;  
	std::vector<double> _prevHipAngleLeft;  
	std::vector<double> _prevCOMX;  
	std::vector<double> _prevCOMVX;  
	std::vector<double> _prevAnkleLocRightX;  
	std::vector<double> _prevAnkleLocLeftX;  
	
	double _totalMetabolics;  
	double _totalLigamentTorque2; 
	
	SimpleMuscle* _act[NUM_MTUS];
	double _p[NUM_MTUS];  
	double _q[NUM_MTUS];  
	
    mutable bool _stance[2];
    mutable bool _stance_prep[2];
    mutable bool _swing_init[2];
    mutable double _d[2];
	int _trailingLegIdx;
	double _PTO;  
	double _G_sol;
	double _G_ta;
	double _l_off_ta;
	double _G_solta;
	double _G_gas;
	double _G_vas;
	double _k_phi;
	double _k_p_glu;
	double _k_d_glu;
	double _phi_k_off;
	double _theta_ref;
	double _G_ham;
	double _G_glu;
	double _G_hfl;
	double _G_hamhfl;
	double _l_off_hfl;
	double _l_off_ham;
	double _k_lean;
	double _k_p_hfl;
	double _k_d_hfl;
	double _k_p_ham;
	double _k_d_ham;
	double _Delta_S_glu; 
	double _Delta_S_hfl; 
	double _Delta_S_rf; 
	double _Delta_S_vas; 
	double _k_p_glu_sp; 
	double _k_d_glu_sp; 
	double _k_p_hfl_sp;
	double _k_d_hfl_sp;
	double _k_p_vas_sp;
	double _k_d_vas_sp;
	double _htheta_ref_sp; 
	double _phi_ref_sp;
	double _sp_threshold;
	double _simbicon_cd; 
	double _simbicon_cv; 
};	

class BodyWatcher : public SimTK::Visualizer::FrameController {
public:
    explicit BodyWatcher(const SimTK::MobilizedBody& body, double zoom, double vTrans, double hTrans) : 
    m_body(body), m_zoomZDistance(zoom), m_vertTrans(vTrans), m_horizTrans(hTrans) {}

    void generateControls(const SimTK::Visualizer&             viz, 
                          const SimTK::State&                  state, 
                          SimTK::Array_< SimTK::DecorativeGeometry >& geometry) OVERRIDE_11
    {
        const SimTK::Vec3 Bo = m_body.getBodyOriginLocation(state);
        const SimTK::Vec3 p_GC = Bo + SimTK::Vec3(m_horizTrans, m_vertTrans, m_zoomZDistance); // above and back
        const SimTK::Rotation R_GC(SimTK::UnitVec3(0,1,0), SimTK::YAxis,
                            p_GC-Bo, SimTK::ZAxis);
        viz.setCameraTransform(Transform(R_GC,p_GC));
        //viz.pointCameraAt(Bo, Vec3(0,1,0));
    }

    // ZOOM DISTANCE
    double getZoomDistance() const { return m_zoomZDistance; }
    void incrementZoomDistance(const double& increment) { m_zoomZDistance += increment; }

    // CAMERA TRANSLATION
    void incrementVerticalCameraTranslation(const double& increment) { m_vertTrans += increment; }
    void incrementHorizontalCameraTranslation(const double& increment) { m_horizTrans += increment; }


private:
    const SimTK::MobilizedBody m_body;
    double m_zoomZDistance;
    double m_vertTrans;
    double m_horizTrans;
};
}
#endif // SIMBICON_H_


