#include "EventHandlers.h"


using namespace std;
using namespace SimTK;
using namespace OpenSim;

// These are hardcoded and changing them changes the fitness value, keeping everything else equal
#define MIN_SWING_PREP 0.1
#define STANCE_TIME 0.3

extern Visualizer* myVisualizer;  

UserInputHandler::UserInputHandler(
    Visualizer::InputSilo& silo,
    OpenSim::BodyWatcher& bw,
    Real interval) 
        : PeriodicEventHandler(interval), _silo(silo), _bodyWatcher(bw) {
	}

void UserInputHandler::handleEvent(State& s, Real accuracy, bool& shouldTerminate) const
{
    shouldTerminate = false;
    while (_silo.isAnyUserInput()) {
        unsigned key, modifiers;
            
        while (_silo.takeKeyHit(key, modifiers)) {
            // End simulation: ESC
            if (key == Visualizer::InputListener::KeyEsc) {
                shouldTerminate = true;
                _silo.clear();
                return;
            }
            // Pause/unpause simulation: SPACE
            else if (key == ' ') {
				myVisualizer->report(s); 
                _silo.waitForAnyUserInput();
                return;
            }
            // Zoom in.
            else if (key == Visualizer::InputListener::KeyPageUp) {
                _bodyWatcher.incrementZoomDistance(-0.04);
            }

            // Zoom out.
            else if (key == Visualizer::InputListener::KeyPageDown) {
                _bodyWatcher.incrementZoomDistance(0.04);
            }

            // Translate camera up.
            else if (key == '8') {
                _bodyWatcher.incrementVerticalCameraTranslation(0.02);
            }

            // Translate camera down.
            else if (key == '5') {
                _bodyWatcher.incrementVerticalCameraTranslation(-0.02);
            }

            // Rotate camera left.
            else if (key == '6') {
                _bodyWatcher.incrementHorizontalCameraTranslation(-0.02);
            }

            // Rotate camera right.
            else if (key == '4') {
                _bodyWatcher.incrementHorizontalCameraTranslation(0.02);
            }
        }
    }
}


#include "GeyerHerrController.h"
#include "SimpleMuscle.h"
#include "SoftCoordinateLimitForce.h"
#include "EventHandlers.h"
#include "Utils.h"
#include <cmath>
#include <typeinfo>


ControlStateHandler::ControlStateHandler(
    Model& model,
    SimTK::Real interval) 
        : PeriodicEventHandler(interval), _model(model) {
	}

void ControlStateHandler::handleEvent(SimTK::State& s, SimTK::Real accuracy, bool& shouldTerminate) const
{
    shouldTerminate = false;
	GeyerHerrController* ghc = static_cast<GeyerHerrController*>(&_model.getControllerSet()[0]);

	int frame = int(floor(s.getTime()/STATE_UPD_STEPSIZE + 0.5));
	if (frame % OBJ_SKIP_FRAMES == 0) { 
		ghc->_stateHistory.push_back(s); 
	}
			
	bool rContact = false; 
	bool lContact = false; 
	if (typeid(_model.getForceSet()[0]) == typeid(OpenSim::HuntCrossleyForce)) {
		OpenSim::HuntCrossleyForce* hc_r = static_cast<OpenSim::HuntCrossleyForce*>(&_model.getForceSet()[0]);
		OpenSim::HuntCrossleyForce* hc_l = static_cast<OpenSim::HuntCrossleyForce*>(&_model.getForceSet()[1]);
		_model.getMultibodySystem().realize(s, SimTK::Stage::Dynamics); 
		rContact = (hc_r->getRecordValues(s).get(1) < 0); 
		lContact = (hc_l->getRecordValues(s).get(1) < 0); 
	}
	else {
		_model.getMultibodySystem().realize(s, SimTK::Stage::Dynamics); 
	}
	//cout << "foot contact: l/r: " << lContact << ", " << rContact << endl;
	
	const ForceSet& fs = _model.getForceSet();
	const Set<Actuator>& as = fs.getActuators();
	
	ghc->recordDelayedValues(s);
	if (frame % OBJ_SKIP_FRAMES == 0) { 
		double frameMetabolics = 0.0; 
		for (int i = 0; i < as.getSize(); i++) {
			if (typeid(as.get(i)) == typeid(OpenSim::SimpleMuscle)) {
				SimpleMuscle* m = static_cast<SimpleMuscle*>(&as.get(i));
				m->updateMuscleVars(s);
				frameMetabolics += m->getMuscVars().metabolicrate;
			}
		}
		frameMetabolics += 1.51*80; // basal term

		ghc->_totalMetabolics += frameMetabolics; 

		double frameLigamentTorque2 = 0.0; 
		for (int i = 0; i < fs.getSize(); i++) {
			if (typeid(fs.get(i)) == 
					typeid(OpenSim::SoftCoordinateLimitForce)) {
				SoftCoordinateLimitForce* f = 
					static_cast<SoftCoordinateLimitForce*>(&fs.get(i));
				double tau = f->getRecordValues(s).get(0); 
				frameLigamentTorque2 += tau*tau; 
			}
		}

		ghc->_totalLigamentTorque2 += frameLigamentTorque2; 
	}
	int curState = ghc->getState();
	double duration = s.getTime() - ghc->getStateStartTime();  

	//cout << "CurState: " << curState << endl;
	// Initiating States
	if (curState < 0) {
		if (rContact) { 
			// Right foot contact
			ghc->setState(2, s.getTime()); 
			ghc->_contactStateHistory.push_back(s); 
		}
		else if (lContact) {
			// Left foot contact
			ghc->setState(0, s.getTime()); 
			ghc->_contactStateHistory.push_back(s); 
		}
	}

	//// Handling state-switching 
	// (0): lStance waiting 0.3 -> (1)
	// (1): lSwingPrep waiting for rContact and 0.1 -> (2)
	// (2): rStance waiting 0.3 -> (3)
	// (3): rSwingPrep waiting for lContact and 0.1 -> (0)

	else if (curState == 0) {
		if (duration > STANCE_TIME) {								// HARDCODED VALUE: delay from lContact till transition phase
			ghc->setState(1, s.getTime()); 
//        	std::cout << "Trans 0 -> 1" << std::endl;;
		}
		else if (rContact && duration > MIN_SWING_PREP) {			// HARDCODED VALUE: delay for transition from 0 to 2
			ghc->setState(2, s.getTime()); 
			ghc->_contactStateHistory.push_back(s); 
  //      	std::cout << "Trans 0 -> 2" << std::endl;;
		}
	}
	else if (curState == 1) {
		if (rContact && duration > MIN_SWING_PREP) {				// HARDCODED VALUE: 
			ghc->setState(2, s.getTime()); 
			ghc->_contactStateHistory.push_back(s); 
    //    	std::cout << "Trans 1 -> 2" << std::endl;;
		}
	}
	else if (curState == 2) {
		if (duration > STANCE_TIME) {								// HARDCODED VALUE: delay for transition between rContact and swing
			ghc->setState(3, s.getTime()); 
      //  	std::cout << "Trans 2 -> 3" << std::endl;;
		}
		else if (lContact && duration > MIN_SWING_PREP) {			// HARDCODED VALUE: 
			ghc->setState(0, s.getTime());
			ghc->_contactStateHistory.push_back(s); 
       // 	std::cout << "Trans 2 -> 0" << std::endl;;
		}
	}
	else if (curState == 3) {
		if (lContact && duration > MIN_SWING_PREP) {				// HARDCODED VALUE: Transition from lSwing to lStance
			ghc->setState(0, s.getTime());
			ghc->_contactStateHistory.push_back(s); 
       // 	std::cout << "Trans 3 -> 0" << std::endl;;
		}
	}

	// This appears to check for something on whether it should terminate all
	SimTK::Vec3 com_p = 
		_model.getMatterSubsystem().calcSystemMassCenterLocationInGround(s);
		 
	if (std::isnan(com_p[0]) || std::isnan(com_p[1]) || std::isnan(com_p[2])) {
		shouldTerminate = true; 
	}

	const ContactGeometrySet& contactSet = _model.getContactGeometrySet();
	const SimTK::Vec3& ori = static_cast<ContactHalfSpace*>(&contactSet[0])->getOrientation(); 
	double theta = ori[2] - (-1.57079633);
	if (com_p[1] < 0.3 + com_p[0]*tan(theta) || com_p[1] > 5 + com_p[0]*tan(theta)) { 
		shouldTerminate = true; 
	}
}

