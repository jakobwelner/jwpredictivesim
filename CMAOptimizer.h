#ifndef _SimTK_CMA_OPTIMIZER_H_
#define _SimTK_CMA_OPTIMIZER_H_

#include "SimTKcommon.h"
#include <OpenSim/OpenSim.h>
#include "simmath/internal/common.h"
#include "simmath/internal/OptimizerRep.h" 
#include "cmaes_interface.h"

#define DIETTAG 2

namespace SimTK {


class CMAOptimizer: public Optimizer::OptimizerRep {
public:
    ~CMAOptimizer(); 

    CMAOptimizer(const OptimizerSystem& sys); 

    Real optimize(  SimTK::Vector &results );
    OptimizerRep* clone() const;

private:
	cmaes_t evo;
	int lambda; // number of samples per iteration
	double sigma; // initial stepsize
	bool resume;  
	bool enableMPI; 
	bool blindRandomSearch;
	String dump_filename;
	String ctrlPrefix;

	void slave(); 
	Real master( SimTK::Vector &results );
	Real masterBRS(Vector &results, const OptimizerSystem& sys);
	Real masterCMAES(Vector &results, const OptimizerSystem& sys);
	
};

double getUniformDouble(double min, double max);
} // namespace SimTK

#endif //_SimTK_CMA_OPTIMIZER_H_

