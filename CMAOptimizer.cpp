#include "CMAOptimizer.h"
#include <iostream>
#include <fstream>
#include <random>
#include <iomanip>   
#include <time.h>
#ifdef USE_MPI
#include <mpi.h>
#endif

using std::cout;
using std::endl;

// Time containers for timestamping receiving nodes
time_t currTime;
time_t startTime;

namespace SimTK {

Optimizer::OptimizerRep* CMAOptimizer::clone() const {
    return( new CMAOptimizer(*this) );
}


CMAOptimizer::CMAOptimizer( const OptimizerSystem& sys )
    : OptimizerRep( sys ), lambda(0), sigma(0.3), resume(false), enableMPI(false), blindRandomSearch(false), dump_filename("default_dump.csv"), ctrlPrefix("notDefined")
{
     /* internal flags for CMA */

     if( sys.getNumParameters() < 1 ) {
        const char* where = "Optimizer Initialization";
        const char* szName = "dimension";
        SimTK_THROW5(SimTK::Exception::ValueOutOfRange, szName, 1,  sys.getNumParameters(), INT_MAX, where); 
     }

}
CMAOptimizer::~CMAOptimizer() {
/*	int numtasks = 0;
	int myrank = 0; 
	MPI_Comm_size(MPI_COMM_WORLD,&numtasks);
	MPI_Comm_rank(MPI_COMM_WORLD,&myrank);
	if (myrank == 0) {
		for (int rank = 1; rank < numtasks; ++rank) {
			MPI_Send(0, 0, MPI_INT, rank, DIETTAG, MPI_COMM_WORLD);
		}
	}*/
}

void CMAOptimizer::slave() {
#ifdef USE_MPI
	MPI_Status status;
    const OptimizerSystem& sys = getOptimizerSystem();
	int nvars = sys.getNumParameters();
	Vector controls; 
	controls.resize(nvars); 
	double* msg = new double[nvars+1];

	while (1) {
		MPI_Recv(msg,
				nvars+1 ,
				MPI_DOUBLE, 0,
				MPI_ANY_TAG,
				MPI_COMM_WORLD, &status);

		if (status.MPI_TAG == DIETTAG) {
			delete msg;
			MPI_Finalize(); 
			exit(0); 
		}

		for (int i = 0; i < nvars; i++) {
			controls[i] = msg[i];
		}
		double res; 
		// Performing simulation
		sys.objectiveFunc(controls, true, res);

		double result[2];
		result[0] = res;
		result[1] = msg[nvars];

		MPI_Send(result, 2, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
	}
#endif
}


Real CMAOptimizer::master(Vector &results) {
	Real f = HUGE_VAL;
	
	const OptimizerSystem& sys = getOptimizerSystem();

	bool blindRandomSearch = false;
	getAdvancedBoolOption("blindRandomSearch", blindRandomSearch);

	if (blindRandomSearch) {
		f = masterBRS(results, sys);
	}
	else {
		f = masterCMAES(results, sys);
	}
	return f;
}




Real CMAOptimizer::masterCMAES( Vector &results, const OptimizerSystem& sys) {
	cout << "Optimizing using CMA-ES" << endl;

    Real f = HUGE_VAL; 
    int n = sys.getNumParameters();

	int numsamples = 0;
    getAdvancedIntOption("lambda", numsamples );
	if (numsamples == 0) {
		// This is using some funky algorithm to decide on number of samples? - only if they are not set
		numsamples = 4+int(3*std::log((double)n)); 
	}
	
	double stepsize = 0;
    getAdvancedRealOption("sigma", stepsize ); 
	if (stepsize == 0.0) {
		stepsize = 0.1; 
	}
	double* stepsizeArray = new double[n];
	for (int i = 0; i < n; i++) {
		stepsizeArray[i] = stepsize;  
	}
	
	bool isresume = false; 
    getAdvancedBoolOption("resume", isresume );
	
	bool usempi = false; 
    getAdvancedBoolOption("enableMPI", usempi );
	 
	// funvals - Fitness Function Value Array
	double* funvals = cmaes_init(&evo, n, &results[0], stepsizeArray, 0, numsamples, "non"); 
	if (isresume) {
		cmaes_resume_distribution(&evo, (char*)"resumecmaes.dat"); 
	}

	evo.sp.stopMaxIter = maxIterations;
	std::vector<double*> msgs; 
	msgs.resize(numsamples);
	for (unsigned int i = 0; i < msgs.size(); i++) {
		msgs[i] = new double[n+1];
	} 
	
	while (!cmaes_TestForTermination(&evo)) {
		//// Running CMA-ES ////
		// Get a gauss-varying mutation of value-sets for whole population
		double*const* pop = cmaes_SamplePopulation(&evo);
		std::cout << "CMA Generation: " << cmaes_Get(&evo, "generation") << std::endl;
		std::cout.flush(); 
    	if( sys.getHasLimits() ) {
    		Real *lowerLimits, *upperLimits;
        	sys.getParameterLimits( &lowerLimits, &upperLimits );
			
			/* Iterating over population size. 
			Checking if new mutations are within upper and lower limits
			Resample if they are not.
			*/
			for (int i = 0; i < cmaes_Get(&evo, "popsize"); i++) {
				bool feasible = false; 
				while (!feasible) {
					feasible = true; 
					// Resample i'th element until it is within bounds.
					for (int j = 0; j < n; j++) {
						if (pop[i][j] < lowerLimits[j] || pop[i][j] > upperLimits[j]) {
							feasible = false; 
							pop = cmaes_ReSampleSingle(&evo, i); 
							break; 
						}
					}
				}
			}
		}
		// Done generating and checking new mutated population

		#ifdef USE_MPI
		if (usempi) {
			int nprocs; 	
			MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

			double buffer[2];
			MPI_Status status;
			int numToReceive = msgs.size();
			unsigned int nextToSend = 0; 
			for (unsigned int i = 0; i < msgs.size(); i++) {
				for (int j = 0; j < n; j++) {
					msgs[i][j] = pop[i][j]; 
				}
				msgs[i][n] = i; 
			}


			for (int i = 1; i < nprocs; i++) {
				MPI_Send(msgs[nextToSend++],
						n+1,
						MPI_DOUBLE,
						i,
						1,
						MPI_COMM_WORLD);
				if (nextToSend >= msgs.size())
					break;
			}

			cout << "Sending command to slaves..." << endl;
			while (nextToSend < msgs.size()) {
				MPI_Recv(buffer,
						2,
						MPI_DOUBLE,
						MPI_ANY_SOURCE,
						MPI_ANY_TAG,
						MPI_COMM_WORLD,
						&status);

				MPI_Send(msgs[nextToSend++],
						n+1,
						MPI_DOUBLE,
						status.MPI_SOURCE,
						1,
						MPI_COMM_WORLD);

				funvals[int(buffer[1])] = buffer[0];
				numToReceive--;
				std::cout << "(" << numToReceive << "," << buffer[0] << ")"; 				
				std::cout.flush();  
			}

			cout << "Receiving results..." << endl;
			
			// Timestamp on receiving prints
			int secondsPassed = 0;
			time(&startTime);

			while (numToReceive > 0) {
				time(&currTime);  /* get current time; same as: timer = time(NULL)  */
				secondsPassed = (int)difftime(currTime, startTime);

				MPI_Recv(buffer,
						2,
						MPI_DOUBLE,
						MPI_ANY_SOURCE,
						MPI_ANY_TAG,
						MPI_COMM_WORLD,
						&status);
				funvals[int(buffer[1])] = buffer[0];
				numToReceive--;
				std::cout << "(" << numToReceive << "," << buffer[0] << "," << secondsPassed << ")";
				std::cout.flush();  
			}
			std::cout << "...done receiving." << std::endl;


			/////////////////////////////////////////////////////////////////////////////////////
			// Saving all sim if dump_filename exist
			String dump_filename;
			String ctrlPrefix;
			getAdvancedStrOption("dump_filename", dump_filename);
			getAdvancedStrOption("ctrlPrefix", ctrlPrefix);

			// Convert from Opt Input
			OpenSim::Storage activeIdx(ctrlPrefix + "_activeparams.sto");
			OpenSim::Storage ctrlRange(ctrlPrefix + "_ranges.sto");
			OpenSim::Array<double>& allActiveIdx = activeIdx.getLastStateVector()->getData();
			OpenSim::Array<double>& allCtrlRangeL = ctrlRange.getStateVector(0)->getData();
			OpenSim::Array<double>& allCtrlRangeU = ctrlRange.getLastStateVector()->getData();

			int count = 0;
			OpenSim::Array<double> rangeU;
			OpenSim::Array<double> rangeL;
			// UnNormalizing control parameters:            
			// inputMax - inputMin*input + inputMin
			for (int i = 0; i < allActiveIdx.getSize(); i++) {
				if (allActiveIdx.get(i) == 1.0) {
					rangeU.append(allCtrlRangeU.get(i));
					rangeL.append(allCtrlRangeL.get(i));
				}
			}

			if (FILE *file = fopen(dump_filename, "r")) {
				fclose(file);

				// This could be the spot to print out all simulation parameters
				cout << endl << "Dumping parameters to " << dump_filename << endl;
				// For writing out all simulations to file
				std::ofstream all_data_dumpfile;
				std::ostringstream print_string;
				//cout << "population: " << cmaes_Get(&evo, "popsize") << ", parameters: " << n << endl << endl;

				for (int i = 0; i < cmaes_Get(&evo, "popsize"); i++) {
					for (int j = 0; j < n; j++) {

						print_string << std::fixed << std::setprecision(15) << (rangeU.get(j) - rangeL.get(j))*pop[i][j] + rangeL.get(j) << "\t";
						//print_string << std::fixed << std::setprecision(15) << pop[i][j] << "\t";
					}
					print_string << std::fixed << std::setprecision(15) << funvals[i] << "\n";
				}
				//cout << "Printing all data to string" << endl;
				//cout << print_string << endl;
				all_data_dumpfile.open(dump_filename, std::ios_base::app);
				all_data_dumpfile << print_string.str();
			}
			//////////////////////////////////////////////////////////////////////////////////////







		} else {
			for (int i = 0; i < cmaes_Get(&evo, "lambda"); i++) {
				objectiveFuncWrapper(n, pop[i], true, &funvals[i], this);
			}
		}
		#else
			for (int i = 0; i < cmaes_Get(&evo, "lambda"); i++) {
				objectiveFuncWrapper(n, pop[i], true, &funvals[i], this);
			}
		#endif
		
		// Update xmeans, fitness history, xbestever, fbestever and other variables for deciding how to generate new population
		cmaes_UpdateDistribution(&evo, funvals); // funvals - Fitness Function Value Array Input
		
		const double* optx = cmaes_GetPtr(&evo, "xbestever");
		for (int i = 0; i < n; i++) {
			results[i] = optx[i]; 
		}
		f = cmaes_Get(&evo, "fbestever");
//		std::cout << "best val: " << f << std::endl;
	}
	cmaes_WriteToFile(&evo, "resume", "resumecmaes.dat");

	for (unsigned int i = 0; i < msgs.size(); i++) {
		delete msgs[i];
	}
	delete stepsizeArray;
	
	return f;  
}





// Alternative Blind Random Search
Real CMAOptimizer::masterBRS(Vector &results, const OptimizerSystem& sys) {
	cout << "Optimizing using Blind Random Search" << endl;
	Real f = HUGE_VAL;
	//const OptimizerSystem& sys = getOptimizerSystem();
	int n = sys.getNumParameters();

	int numsamples = 0;
	getAdvancedIntOption("lambda", numsamples);
	if (numsamples == 0) {
		numsamples = 4 + int(3 * std::log((double)n));
	}
	double stepsize = 0;
	getAdvancedRealOption("sigma", stepsize);
	if (stepsize == 0.0) {
		stepsize = 0.1;
	}
	double* stepsizeArray = new double[n];
	for (int i = 0; i < n; i++) {
		stepsizeArray[i] = stepsize;
	}


	bool usempi = false;
	getAdvancedBoolOption("enableMPI", usempi);

	// funvals - Fitness Function Value Array
	// Write replacement for this
	double* funvals = cmaes_init(&evo, n, &results[0], stepsizeArray, 0, numsamples, "non");


	evo.sp.stopMaxIter = maxIterations;
	std::vector<double*> msgs;
	msgs.resize(numsamples);
	for (unsigned int i = 0; i < msgs.size(); i++) {
		msgs[i] = new double[n + 1];
	}


	// Initiating population by cmaes. Overwriting with uniform distribution
	//while (!cmaes_TestForTermination(&evo)) {
	for (int p = 0; p < 10; p++) {
		double*const* pop = cmaes_SamplePopulation(&evo);
		//std::cout << "Uniform Distribution - Generation: " << cmaes_Get(&evo, "generation") << std::endl;
		std::cout << "Uniform Distribution - Generation: " << p << std::endl;
		std::cout.flush();
		if (sys.getHasLimits()) {
			Real *lowerLimits, *upperLimits;
			sys.getParameterLimits(&lowerLimits, &upperLimits);

			for (int i = 0; i < cmaes_Get(&evo, "popsize"); i++) {
				for (int j = 0; j < n; j++) {
					pop[i][j] = getUniformDouble(lowerLimits[j], upperLimits[j]);
					//cout << "SampleI: " << j << ", LowLim: " << lowerLimits[j] << ", UpLim: " << upperLimits[j] << ", Sample: " << pop[i][j] << endl;
				}
			}
		}
		else
			cout << "Some parameter didn't have limits" << endl;
		// Done generating and checking new mutated population

#ifdef USE_MPI
		if (usempi) {
			int nprocs;
			MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

			double buffer[2];
			MPI_Status status;
			int numToReceive = msgs.size();
			unsigned int nextToSend = 0;
			for (unsigned int i = 0; i < msgs.size(); i++) {
				for (int j = 0; j < n; j++) {
					msgs[i][j] = pop[i][j];
				}
				msgs[i][n] = i;
			}


			for (int i = 1; i < nprocs; i++) {
				MPI_Send(msgs[nextToSend++],
					n + 1,
					MPI_DOUBLE,
					i,
					1,
					MPI_COMM_WORLD);
				if (nextToSend >= msgs.size())
					break;
			}

			cout << "Sending command to slaves..." << endl;
			while (nextToSend < msgs.size()) {
				MPI_Recv(buffer,
					2,
					MPI_DOUBLE,
					MPI_ANY_SOURCE,
					MPI_ANY_TAG,
					MPI_COMM_WORLD,
					&status);

				MPI_Send(msgs[nextToSend++],
					n + 1,
					MPI_DOUBLE,
					status.MPI_SOURCE,
					1,
					MPI_COMM_WORLD);

				funvals[int(buffer[1])] = buffer[0];
				numToReceive--;
				std::cout << "(" << numToReceive << "," << buffer[0] << ")";
				std::cout.flush();
			}

			cout << "Receiving results..." << endl;

			// Timestamp on receiving prints
			int secondsPassed = 0;
			time(&startTime);

			while (numToReceive > 0) {
				time(&currTime);  /* get current time; same as: timer = time(NULL)  */
				secondsPassed = (int)difftime(currTime, startTime);

				MPI_Recv(buffer,
					2,
					MPI_DOUBLE,
					MPI_ANY_SOURCE,
					MPI_ANY_TAG,
					MPI_COMM_WORLD,
					&status);
				funvals[int(buffer[1])] = buffer[0];
				numToReceive--;
				std::cout << "(" << numToReceive << "," << buffer[0] << "," << secondsPassed << ")";
				std::cout.flush();
			}
			std::cout << "...done receiving." << std::endl;



			/////////////////////////////////////////////////////////////////////////////////////
			// Saving all sim if dump_filename exist
			String dump_filename;
			String ctrlPrefix;
			getAdvancedStrOption("dump_filename", dump_filename);
			getAdvancedStrOption("ctrlPrefix", ctrlPrefix);

			// Convert from Opt Input
			OpenSim::Storage activeIdx(ctrlPrefix + "_activeparams.sto");
			OpenSim::Storage ctrlRange(ctrlPrefix + "_ranges.sto");
			OpenSim::Array<double>& allActiveIdx = activeIdx.getLastStateVector()->getData();
			OpenSim::Array<double>& allCtrlRangeL = ctrlRange.getStateVector(0)->getData();
			OpenSim::Array<double>& allCtrlRangeU = ctrlRange.getLastStateVector()->getData();
			
			int count = 0;
			OpenSim::Array<double> rangeU;
			OpenSim::Array<double> rangeL;
			// UnNormalizing control parameters:            
			// inputMax - inputMin*input + inputMin
			for (int i = 0; i < allActiveIdx.getSize(); i++) {
				if (allActiveIdx.get(i) == 1.0) {
					rangeU.append(allCtrlRangeU.get(i));
					rangeL.append(allCtrlRangeL.get(i));
				}
			}
			
			if (FILE *file = fopen(dump_filename, "r")) {
				fclose(file);

				// This could be the spot to print out all simulation parameters
				cout << endl << "Dumping parameters to " << dump_filename << endl;
				// For writing out all simulations to file
				std::ofstream all_data_dumpfile;
				std::ostringstream print_string;
				//cout << "population: " << cmaes_Get(&evo, "popsize") << ", parameters: " << n << endl << endl;

				for (int i = 0; i < cmaes_Get(&evo, "popsize"); i++) {
					for (int j = 0; j < n; j++) {
						
						print_string << std::fixed << std::setprecision(15) << (rangeU.get(j) - rangeL.get(j))*pop[i][j] + rangeL.get(j) << "\t";
						//print_string << std::fixed << std::setprecision(15) << pop[i][j] << "\t";
					}
					print_string << std::fixed << std::setprecision(15) << funvals[i] << "\n";
				}
				//cout << "Printing all data to string" << endl;
				//cout << print_string << endl;
				all_data_dumpfile.open(dump_filename, std::ios_base::app);
				all_data_dumpfile << print_string.str();
			}
			//////////////////////////////////////////////////////////////////////////////////////



		}
		else {
			for (int i = 0; i < cmaes_Get(&evo, "lambda"); i++) {
				objectiveFuncWrapper(n, pop[i], true, &funvals[i], this);
			}
		}
#else
		for (int i = 0; i < cmaes_Get(&evo, "lambda"); i++) {
			objectiveFuncWrapper(n, pop[i], true, &funvals[i], this);
		}
#endif

		// Update xmeans, fitness history, xbestever, fbestever and other variables for deciding how to generate new population
		cmaes_UpdateDistribution(&evo, funvals); // funvals - Fitness Function Value Array Input

		const double* optx = cmaes_GetPtr(&evo, "xbestever");
		for (int i = 0; i < n; i++) {
			results[i] = optx[i];
		}
		f = cmaes_Get(&evo, "fbestever");
		//		std::cout << "best val: " << f << std::endl;
	}
	//cmaes_WriteToFile(&evo, "resume", "resumecmaes.dat");

	for (unsigned int i = 0; i < msgs.size(); i++) {
		delete msgs[i];
	}
	delete stepsizeArray;

	return f;
}



Real CMAOptimizer::optimize( Vector &results ) {
#ifdef USE_MPI
	//MPI_Comm_size(MPI_COMM_WORLD,&numtasks);
	int myrank = 0; 
	MPI_Comm_rank(MPI_COMM_WORLD,&myrank);
	if (myrank > 0) {
		slave(); 
		return 0; 
	}
	else {
		return master(results); 
	}
#else
	return master(results); 
#endif
}








// Added by JW
double getUniformDouble(double min, double max)
{
	std::random_device rd;
	std::mt19937 e2(rd());
	std::uniform_real_distribution<> dist(min, max);

	//std::cout << std::fixed << std::setprecision(15) << dist(e2) << std::endl;

	return dist(e2);
}



} // namespace SimTK
