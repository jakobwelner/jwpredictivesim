#include <OpenSim/OpenSim.h>

namespace OpenSim {
class PredictiveOptimizationSystem : public SimTK::OptimizerSystem {
public: 
	PredictiveOptimizationSystem(int numParams, std::string modelFile, 
		std::string ctrlPrefix,  
		double finalTime, bool visualize, double targetVel, double backpackLoad, bool printDetailsToFile, bool optAccel, double pertAmp, bool supraCtrl, bool load_ctrl_values, bool save_ctrl_values, double save_ctrl_at_time, bool doOpt, bool save_all_sim) :
		SimTK::OptimizerSystem(numParams), 
		_modelFile(modelFile), 
		_ctrlPrefix(ctrlPrefix), 
		_finalTime(finalTime), 
		_visualize(visualize), 
        _targetVel(targetVel),
        _backpackLoad(backpackLoad),
        _printDetailsToFile(printDetailsToFile),
		_optAccel(optAccel),
		_pertAmp(pertAmp),
		_supraCtrl(supraCtrl),
		_load_ctrl_values(load_ctrl_values),
		_save_ctrl_values(save_ctrl_values),
		_save_ctrl_at_time(save_ctrl_at_time),
		_doOpt(doOpt),
		_save_all_sim(save_all_sim)
		 {_printResults = false;
          _results = NULL;} 
	
	~PredictiveOptimizationSystem() 
    {delete _results;}
	
	int objectiveFunc(const SimTK::Vector& params,
			bool reporting, SimTK::Real& f) const OVERRIDE_11;  

    void setPrintResults(bool r) {_printResults = r; }
    static void initAllMuscles(Model& m, double pertAmp); 


private:
	std::string _modelFile;
	std::string _ctrlPrefix;
	double _finalTime;  
	bool _visualize;
    bool _printResults;
	bool _optAccel;
	bool _doOpt;
	bool _save_all_sim;
    double _targetVel;
    double _backpackLoad;
	double _pertAmp;
	bool _supraCtrl;
	bool _load_ctrl_values;
	bool _save_ctrl_values;
	double _save_ctrl_at_time;

    bool _printDetailsToFile;
    mutable double _systemMass;
    mutable Storage* _results;          // so we can edit in a const method

    void placeCurrentStateIntoZeroPose(SimTK::State& s) const;
    void printMuscleCurves(Model& m, SimTK::State& s, std::string muscle, std::string coordinate) const;
    void printMuscleMasses(const Model& m) const;
    SimTK::Vector getOpenSimStateValues(const Model& model, const SimTK::State& s) const;
    void setupResultStorage(const Model& model) const;
    void addToResultStorage(const Model& model, const SimTK::State& s) const;
    SimTK::Vector getInverseDynamicsLoad(const Model& model, const SimTK::State& s) const;
}; 
}
