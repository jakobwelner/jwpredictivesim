#ifndef OPENSIM_SOFT_COORDINATE_LIMIT_FORCE_H_
#define OPENSIM_SOFT_COORDINATE_LIMIT_FORCE_H_

//=============================================================================
// INCLUDES
//=============================================================================
#include <OpenSim/OpenSim.h>

namespace OpenSim { 

class SoftCoordinateLimitForce : public Force {
OpenSim_DECLARE_CONCRETE_OBJECT(SoftCoordinateLimitForce, Force);
public:
//==============================================================================
// PROPERTIES
//==============================================================================
    /** @name Property declarations
    These are the serializable properties associated with this class. **/
    /**@{**/
    OpenSim_DECLARE_PROPERTY(coordinate, std::string,
        "Coordinate (name) to be limited.");
    OpenSim_DECLARE_PROPERTY(upper_stiffness, double,
        "Stiffness of the passive limit force when coordinate exceeds upper "
        "limit. Note, rotational stiffness expected in N*m/degree.");
    OpenSim_DECLARE_PROPERTY(upper_limit, double,
        "The upper limit of the coordinate range of motion (rotations in "
        "degrees).");
    OpenSim_DECLARE_PROPERTY(lower_stiffness, double,
        "Stiffness of the passive limit force when coordinate exceeds lower "
        "limit. Note, rotational stiffness expected in N*m/degree.");
    OpenSim_DECLARE_PROPERTY(lower_limit, double,
        "The lower limit of the coordinate range of motion (rotations in "
        "degrees).");
    OpenSim_DECLARE_PROPERTY(v_max, double,
        "Damping factor on the coordinate's speed applied only when limit "
        "is exceeded. For translational has units N/(m/s) and rotational has "
        "Nm/(degree/s)");
    /**@}**/

//=============================================================================
// PUBLIC METHODS
//=============================================================================
    /** Default constructor */
    SoftCoordinateLimitForce();

    SoftCoordinateLimitForce(const std::string& coordName, double q_upper, 
        double K_upper, double q_lower, double K_lower, double v_max); 
    
    //use compiler default copy constructor and assignment operator

	/** Destructor */
    ~SoftCoordinateLimitForce();


    //--------------------------------------------------------------------------
    // GET AND SET
    //--------------------------------------------------------------------------
    // Properties
    /** Stiffness of the passive limit force when coordinate exceeds upper 
    limit. Note, rotational stiffness expected in N*m/degree. */
    void setUpperStiffness(double aUpperStiffness);
    double getUpperStiffness() const;
    
    /** Upper limit of the coordinate range of motion (rotations in degrees).*/
    void setUpperLimit(double aUpperLimit);
    double getUpperLimit() const;
    
    /** Stiffness of the passive limit force when coordinate exceeds lower 
    limit. Note, rotational stiffness expected in N*m/degree. */
    void setLowerStiffness(double aLowerStiffness);
    double getLowerStiffness() const;
    
    /** Lower limit of the coordinate range of motion (rotations in degrees).*/
    void setLowerLimit(double aLowerLimit);
    double getLowerLimit() const;
    
    void setVmax(double aVmax);
    double getVmax() const;

    //--------------------------------------------------------------------------
    // COMPUTATIONS
    //--------------------------------------------------------------------------
    /** Force calculation operator. **/
    double calcLimitForce( const SimTK::State& s) const;


    //--------------------------------------------------------------------------
    // REPORTING
    //--------------------------------------------------------------------------
    /** 
     * Methods to query a Force for the value actually applied during simulation
     * The names of quantities (column labels) are  returned by getRecordLabels()
     */
    Array<std::string> getRecordLabels() const ;
    /**
     * Given SimTK::State object extract all the values necessary to report forces, application location
     * frame, etc. used in conjunction with getRecordLabels and should return same size Array
     */
    Array<double> getRecordValues(const SimTK::State& state) const ;

protected:
    //--------------------------------------------------------------------------
    // Model Component Interface
    //--------------------------------------------------------------------------
    /** Setup this SoftCoordinateLimitForce as part of the model.
        This were the existence of the coordinate to limit is checked. */ 
    void connectToModel(Model& aModel) OVERRIDE_11;
    /** Create the underlying Force that is part of the multibodysystem. */
    void addToSystem(SimTK::MultibodySystem& system) const OVERRIDE_11;

    //--------------------------------------------------------------------------
    // Force Interface
    //--------------------------------------------------------------------------
    void computeForce(const SimTK::State& s, 
                      SimTK::Vector_<SimTK::SpatialVec>& bodyForces, 
                      SimTK::Vector& mobilityForces) const OVERRIDE_11;

private:
    // Object helpers
    void setNull();
    void constructProperties();

    // Scaling for coordinate values in m or degrees (rotational) 
    double _w;

    // Coordinate limits in internal (SI) units (m or rad)
    double _qup;
    double _qlow;
    // Constant stiffnesses in internal (SI) N/m or Nm/rad
    double _Kup;
    double _Klow;

    double _vmax;

    // Corresponding generalized coordinate to which the coordinate actuator
    // is applied.
    SimTK::ReferencePtr<Coordinate> _coord;

//=============================================================================
};	// END of class SoftCoordinateLimitForce

}; //namespace
//=============================================================================
//=============================================================================


#endif // #ifndef OPENSIM_COORDINATE_LIMIT_FORCE_H_
