/* -------------------------------------------------------------------------- *
 *                     OpenSim:  SoftCoordinateLimitForce.cpp                     *
 * -------------------------------------------------------------------------- *
 * The OpenSim API is a toolkit for musculoskeletal modeling and simulation.  *
 * See http://opensim.stanford.edu and the NOTICE file for more information.  *
 * OpenSim is developed at Stanford University and supported by the US        *
 * National Institutes of Health (U54 GM072970, R24 HD065690) and by DARPA    *
 * through the Warrior Web program.                                           *
 *                                                                            *
 * Copyright (c) 2005-2012 Stanford University and the Authors                *
 * Author(s): Ajay Seth                                                       *
 *                                                                            *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may    *
 * not use this file except in compliance with the License. You may obtain a  *
 * copy of the License at http://www.apache.org/licenses/LICENSE-2.0.         *
 *                                                                            *
 * Unless required by applicable law or agreed to in writing, software        *
 * distributed under the License is distributed on an "AS IS" BASIS,          *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *
 * See the License for the specific language governing permissions and        *
 * limitations under the License.                                             *
 * -------------------------------------------------------------------------- */


//=============================================================================
// INCLUDES
//=============================================================================
#include "SoftCoordinateLimitForce.h"
#include <OpenSim/Simulation/Model/Model.h>
#include <OpenSim/Simulation/SimbodyEngine/Coordinate.h>

using namespace OpenSim;
using namespace std;

//=============================================================================
// CONSTRUCTOR(S) AND DESTRUCTOR
//=============================================================================
//_____________________________________________________________________________
// Destructor.
SoftCoordinateLimitForce::~SoftCoordinateLimitForce()
{
}
//_____________________________________________________________________________
// Default constructor.
SoftCoordinateLimitForce::SoftCoordinateLimitForce()
{
	setNull();
    constructProperties();
}

//_____________________________________________________________________________
// Convenience constructor.
SoftCoordinateLimitForce::SoftCoordinateLimitForce
   (const string& coordName, double q_upper, 
	double K_upper,	double q_lower, double K_lower, double v_max) : Force()
{
	setNull();
    constructProperties();

	set_coordinate(coordName);
	set_upper_stiffness(K_upper);
	set_upper_limit(q_upper);
	set_lower_stiffness(K_lower);
	set_lower_limit(q_lower);
	set_v_max(v_max);

	setName(coordName + "_SoftLimitForce");
}

//_____________________________________________________________________________
// Set the data members of this actuator to their null values. Note that we
// also use this after copy construction or copy assignment; these should be
// calculated at connectToModel().
void SoftCoordinateLimitForce::setNull()
{
	// Scaling for coordinate values in m or degrees (rotational) 
	_w = SimTK::NaN;

	// Coordinate limits in internal (SI) units (m or rad)
	_qup = SimTK::NaN;
	_qlow = SimTK::NaN;
	// Constant stiffnesses in internal (SI) N/m or Nm/rad
	_Kup = SimTK::NaN;;
	_Klow = SimTK::NaN;

	_vmax = SimTK::NaN;

	_coord = NULL;
}
//_____________________________________________________________________________
// Allocate and initialize properties.
void SoftCoordinateLimitForce::constructProperties()
{
	constructProperty_coordinate("UNASSIGNED");
	constructProperty_upper_stiffness(1.0);
	constructProperty_upper_limit(0.0);
	constructProperty_lower_stiffness(1.0);
	constructProperty_lower_limit(0.0);
	constructProperty_v_max(0.001);
}


//=============================================================================
// GET AND SET SoftCoordinateLimitForce Stiffness and Damping parameters
//=============================================================================
void SoftCoordinateLimitForce::setUpperStiffness(double aUpperStiffness)
{
	set_upper_stiffness(aUpperStiffness);
}

void SoftCoordinateLimitForce::setUpperLimit(double aUpperLimit)
{
	set_upper_limit(aUpperLimit);
}

void SoftCoordinateLimitForce::setLowerStiffness(double aLowerStiffness)
{
	set_lower_stiffness(aLowerStiffness);
}

void SoftCoordinateLimitForce::setLowerLimit(double aLowerLimit)
{
	set_lower_limit(aLowerLimit);
}

void SoftCoordinateLimitForce::setVmax(double aVmax)
{
	set_v_max(aVmax);
}

//_____________________________________________________________________________
/**
 * Get the parameters.
 */
double SoftCoordinateLimitForce::getUpperStiffness() const
{
	return get_upper_stiffness();
}

double SoftCoordinateLimitForce::getUpperLimit() const
{
	return get_upper_limit();
}

double SoftCoordinateLimitForce::getLowerStiffness() const
{
	return get_lower_stiffness();
}
double SoftCoordinateLimitForce::getLowerLimit() const
{
	return get_lower_limit();
}

double SoftCoordinateLimitForce::getVmax() const
{
	return get_v_max();
}

//_____________________________________________________________________________
/**
 * Perform some setup functions that happen after the
 * object has been deserialized or copied.
 *
 * @param aModel OpenSim model containing this SoftCoordinateLimitForce.
 */
void SoftCoordinateLimitForce::connectToModel(Model& aModel)
{
	Super::connectToModel(aModel);

    string errorMessage;

	const string& coordName = get_coordinate();
	const double& upperStiffness = get_upper_stiffness();
	const double& upperLimit = get_upper_limit();
	const double& lowerStiffness = get_lower_stiffness();
	const double& lowerLimit = get_lower_limit();
	const double& vmax = get_v_max();

	// Look up the coordinate
	if (!_model->updCoordinateSet().contains(coordName)) {
		errorMessage = "SoftCoordinateLimitForce: Invalid coordinate (" + coordName + ") specified in Actuator " + getName();
		throw (Exception(errorMessage.c_str()));
	}
	_coord = &_model->updCoordinateSet().get(coordName);

	// scaling for units
	_w = (_coord->getMotionType() == Coordinate::Rotational) ? SimTK_DEGREE_TO_RADIAN : 1.0;

	_qup = _w*upperLimit;
	_qlow = _w*lowerLimit;
	_Kup = upperStiffness/_w;
	_Klow = lowerStiffness/_w;
	_vmax = _w*vmax;
}


/** Create the underlying Force that is part of the multibodysystem. */
void SoftCoordinateLimitForce::addToSystem(SimTK::MultibodySystem& system) const
{
	Super::addToSystem(system);
}

//=============================================================================
// COMPUTATIONS
//=============================================================================
//_____________________________________________________________________________
/**
 * Compute and apply the mobility force corrsponding to the passive limit force
 *
 */
void SoftCoordinateLimitForce::computeForce( const SimTK::State& s, 
							   SimTK::Vector_<SimTK::SpatialVec>& bodyForces, 
							   SimTK::Vector& mobilityForces) const
{
	applyGeneralizedForce(s, *_coord, calcLimitForce(s), mobilityForces);
}

double SoftCoordinateLimitForce::calcLimitForce( const SimTK::State& s) const
{
	double q = _coord->getValue(s);
	double qdot = _coord->getSpeedValue(s);

	double f_up = 0.0;
	double f_low = 0.0;  
	if ((q - _qup > 0) && (qdot/_vmax > -1)) {
		f_up = -_Kup*(q - _qup)*(1 + qdot/_vmax); 
	}
	
	if ((_qlow - q > 0) && (qdot/_vmax < 1)) {
		f_low = _Klow*(_qlow - q)*(1 - qdot/_vmax); 
	}

	double f_limit = f_up + f_low;

	return f_limit;
}


/** 
 * Methods to query a Force for the value actually applied during simulation
 * The names of the quantities (column labels) is returned by this first function
 * getRecordLabels()
 */
Array<std::string> SoftCoordinateLimitForce::getRecordLabels() const {
	OpenSim::Array<std::string> labels("LimitForce");
	labels.append(getName());
	return labels;
}
/**
 * Given SimTK::State object extract all the values necessary to report forces, application location
 * frame, etc. used in conjunction with getRecordLabels and should return same size Array
 */
Array<double> SoftCoordinateLimitForce::getRecordValues(const SimTK::State& state) const {
	OpenSim::Array<double> values(0, 0, 1);
	values.append(calcLimitForce(state));
	return values;
}
