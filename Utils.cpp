#include "Utils.h"
#include <fstream>
#include <string>
#include <iostream>
#include <iterator>
#include <vector>
#include <algorithm> // for std::copy

using namespace std;
using namespace OpenSim; 

#define SAVE_STATE_DIR "controllerState/"

// Added by JW

std::string convertInt(int number) {
	std::stringstream ss;
	ss << std::setfill('0') << std::setw(4) << number;
	return ss.str();}

/*
void writeVarToFile(std::string const filename, std::vector<double> data) {
	ofstream ofs(filename);
	if (!ofs)
		cout << "Error opening file for output: " << filename << endl;
	for (auto& j : data)
		ofs << j << " ";
	ofs.close();}

void writeVarToFile(std::string const filename, double const data) {
	ofstream ofs(filename);
	if (!ofs)
		cout << "Error opening file for output" << endl;
	ofs << data << " ";
	ofs.close();}

void writeDoubleToFile(std::string const name, double& data) {
	//cout << name << ": " << data << endl;
	std::string filename = SAVE_STATE_DIR + name + "_0000.txt";
	writeVarToFile(filename, data);}

void writeDoubleToFile(std::string const name, int& data) {
	//cout << name << ": " << data << endl;
	std::string filename = SAVE_STATE_DIR + name + "_0000.txt";
	writeVarToFile(filename, data);}

void writeDoubleToFile(std::string const name, bool& data) {
	//cout << name << ": " << data << endl;
	std::string filename = SAVE_STATE_DIR + name + "_0000.txt";
	writeVarToFile(filename, data);}

void writeVectorToFile(std::string const name, std::vector<double>* const data_object, int const length) {
	for (int i = 0; i < length; i++) {
		std::string filename = SAVE_STATE_DIR + name + "_" + convertInt(i) + ".txt";
		writeVarToFile(filename, data_object[i]);}}

void writeVectorToFile(std::string const name, std::vector<double> const data_object) {
	std::string filename = SAVE_STATE_DIR + name + "_0000.txt";
	writeVarToFile(filename, data_object);}

void writeVectorToFile(std::string const name, double* const data_object, int const length) {
	for (int i = 0; i < length; i++) {
		std::string filename = SAVE_STATE_DIR + name + "_" + convertInt(i) + ".txt";
		writeVarToFile(filename, data_object[i]);}}




////////////////////// READING BACK VARIABLES ///////////////////////////////
void readVectorFile(std::string const filename, std::vector<double> &output) {
	std::ifstream is(filename);
	std::istream_iterator<double> start(is), end;
	std::vector<double> numbers(start, end);
	output = numbers;}

void readVarFromFile(std::string const name, std::vector<double> output[], int const length) {
	for (int j = 0; j < length; j++) {
		std::string filename = SAVE_STATE_DIR + name + "_" + convertInt(j) + ".txt";
		readVectorFile(filename, output[j]);}}

void readVarFromFile(std::string const name, std::vector<double> &output) {
	std::string filename = SAVE_STATE_DIR + name + "_0000.txt";
	readVectorFile(filename, output);}

void readVarFromFile(std::string const name, double output[], int const length) {
	for (int i = 0; i < length; i++) {
		std::string filename = SAVE_STATE_DIR + name + "_" + convertInt(i) + ".txt";
		std::ifstream is(filename);
		is >> output[i];}}

void readDoubleFromFile(std::string const name, double &output) {
	std::string filename = SAVE_STATE_DIR + name + "_0000.txt";
	std::ifstream is(filename);
	is >> output;}

void readDoubleFromFile(std::string const name, int &output) {
	std::string filename = SAVE_STATE_DIR + name + "_0000.txt";
	std::ifstream is(filename);
	is >> output;}

void readDoubleFromFile(std::string const name, bool &output) {
	std::string filename = SAVE_STATE_DIR + name + "_0000.txt";
	std::ifstream is(filename);
	is >> output;}
*/


// Updating input parameters in the output list
void updateParams(const Array<double>& input, std::string ctrlPrefix, Array<double>& output) {
	//Storage allCtrls(_ctrlPrefix + "_values.sto");
	//Array<double>& allSimParams = allCtrls.getLastStateVector()->getData();
	//convertFromOptInput(params, _ctrlPrefix, allSimParams);

	Storage activeIdx(ctrlPrefix + "_activeparams.sto");
	Array<double>& allActiveIdx = activeIdx.getLastStateVector()->getData();

	for (int i = 0; i < input.getSize(); i++) {
		if (allActiveIdx.get(i) == 1.0) {
			std::cout << "output: " << output.get(i) << " , input: " << input.get(i) << endl;
			output.set(i, input.get(i));
		}
	}

}


// UnNormalizing control parameters: 
// Overwriting only active parameters, leaving the rest intact
// Matching the shorter output to activeparams by assuming that they are 
//   in the same order as when they got converted  into OptInput
void convertFromOptInput( const SimTK::Vector& input, std::string ctrlPrefix, 
	Array<double>& output ) {

	//cout << endl << "### Utils.cpp: both _activeparams.sto and _ranges.sto" << endl;
	Storage activeIdx(ctrlPrefix + "_activeparams.sto");
	Storage ctrlRange(ctrlPrefix + "_ranges.sto");
	Array<double>& allActiveIdx = activeIdx.getLastStateVector()->getData(); 
	Array<double>& allCtrlRangeL = ctrlRange.getStateVector(0)->getData(); 
	Array<double>& allCtrlRangeU = ctrlRange.getLastStateVector()->getData(); 

	int count = 0; 
	// UnNormalizing control parameters:            
	// inputMax - inputMin*input + inputMin
	for (int i = 0; i < output.getSize(); i++) {
		if (allActiveIdx.get(i) == 1.0) {
			output.set(i, (allCtrlRangeU.get(i) - allCtrlRangeL.get(i))*input[count++] + allCtrlRangeL.get(i)); 
		}
	}
}

// Normalizing and outputting active parameters with bounds using
// [prefix]_range, [prefix]_activeparams and [prefix]_bounds.sto
// Resizing output to include only active parameters
void convertToOptInput( const Array<double>& input, 
	std::string ctrlPrefix, 
	SimTK::Vector& output, 
	SimTK::Vector& lower_bound, 
	SimTK::Vector& upper_bound
	 ) {
	//cout << endl << "### Utils.cpp: _activeparams.sto, _ranges.sto, _bounds" << endl;
	Storage activeIdx(ctrlPrefix + "_activeparams.sto");
	Storage ctrlRange(ctrlPrefix + "_ranges.sto");
	Storage ctrlBound(ctrlPrefix + "_bounds.sto");
	Array<double>& allActiveIdx = activeIdx.getLastStateVector()->getData(); 
	Array<double>& allCtrlRangeL = ctrlRange.getStateVector(0)->getData(); 
	Array<double>& allCtrlRangeU = ctrlRange.getLastStateVector()->getData();
	Array<double>& allCtrlBoundL = ctrlBound.getStateVector(0)->getData(); 
	Array<double>& allCtrlBoundU = ctrlBound.getLastStateVector()->getData();
	 
	int count = 0; 
	for (int i = 0; i < input.getSize(); i++) {
		if (allActiveIdx.get(i) == 1.0) {
			count++; 
		}
	}

	output.resize(count); 
	lower_bound.resize(count); 
	upper_bound.resize(count); 
	count = 0; 

	// Normalizing input values to range:		input - inputMin/(inputMax-inputMin)
	for (int i = 0; i < input.getSize(); i++) {
		if (allActiveIdx.get(i) == 1.0) {
			output[count] = (input.get(i) - allCtrlRangeL.get(i))/(allCtrlRangeU.get(i) - allCtrlRangeL.get(i)); 
			lower_bound[count] = ( allCtrlBoundL.get(i) - allCtrlRangeL.get(i) ) / ( allCtrlRangeU.get(i) - allCtrlRangeL.get(i) ); 
			upper_bound[count] = ( allCtrlBoundU.get(i) - allCtrlRangeL.get(i) ) / ( allCtrlRangeU.get(i) - allCtrlRangeL.get(i) ); 
			count++;
		}
	}
}

void getUpperBodyCOM( const Model& model, const SimTK::State& s, SimTK::Vec3& result ) {
	const Array<Object*> upperBodyObjs = model.getBodySet().getGroup("upper_bodies")->getMembers(); 
	const SimTK::SimbodyMatterSubsystem& matter = model.getMatterSubsystem();
	
	SimTK::Vec3 com(0.0, 0.0, 0.0);
	double totalMass = 0.0;  
	for (int i = 0; i < upperBodyObjs.getSize(); i++) {
		const Body* b = static_cast<Body*>(upperBodyObjs.get(i));
		SimTK::Vec3 bodyMassCenter; 
		b->getMassCenter(bodyMassCenter);
		com += b->getMass()*matter.getMobilizedBody(b->getIndex()).findStationLocationInGround(s, bodyMassCenter);
		totalMass += b->getMass(); 
	}
	result = com/totalMass; 
}

void getSagCorNormals( const Model& model, 
	const SimTK::State& s, SimTK::Vec3& sagN, SimTK::Vec3& corN ) {
	const BodySet& bodySet = model.getBodySet();
	const SimTK::SimbodyMatterSubsystem& matter = model.getMatterSubsystem();

	const Body& pelvis = bodySet.get("pelvis");
	
	sagN = matter.getMobilizedBody(pelvis.getIndex()).findStationLocationInGround(s, UnitZ) - matter.getMobilizedBody(pelvis.getIndex()).getBodyOriginLocation(s);
	corN = matter.getMobilizedBody(pelvis.getIndex()).findStationLocationInGround(s, UnitX) - matter.getMobilizedBody(pelvis.getIndex()).getBodyOriginLocation(s);
	sagN[1] = 0; 
	sagN = sagN.normalize();  
	corN[1] = 0; 
	corN = corN.normalize();  
}

void getUpVectorInGround( const Model& model, const SimTK::State& s, 
	const Body& b, SimTK::Vec3& result ) { 
	const SimTK::SimbodyMatterSubsystem& matter = model.getMatterSubsystem();
	result = matter.getMobilizedBody(b.getIndex()).findStationLocationInGround(s, UnitY) - matter.getMobilizedBody(b.getIndex()).getBodyOriginLocation(s);
}

void getFrontVectorInGround( const Model& model, 
	const SimTK::State& s, const Body& b, SimTK::Vec3& result ) { 
	const SimTK::SimbodyMatterSubsystem& matter = model.getMatterSubsystem();
	result = matter.getMobilizedBody(b.getIndex()).findStationLocationInGround(s, UnitX) - matter.getMobilizedBody(b.getIndex()).getBodyOriginLocation(s);
}

void setControl(const OpenSim::Actuator* act, double val, SimTK::Vector& controls) {
	SimTK::Vector signal(1, val);
	double max = act->getMaxControl(); 
	double min = act->getMinControl(); 
	if (val > max ) 
		signal[0] = max;
	else if (val < min) 
		signal[0] = min; 
	act->setControls(signal, controls);  
}
// Get previous states from queue as double pointers. See other description
double getPreviousDouble( const std::vector<double>* queuePtr, double delta_t ) {
	double theta, thetav; 
	getPreviousDouble( queuePtr, delta_t, theta, thetav ); 
	return theta;  
}


// Get previous states from queue as double pointers
// theta: previous value
// thetav: velocity since prev value
void getPreviousDouble( const std::vector<double>* queuePtr, double delta_t,
		double& theta, double& thetav ) {
	int idx = (int) floor(delta_t/STATE_UPD_STEPSIZE);

	if (idx >= int(queuePtr->size())) {
		theta = 0.0; 
		if (queuePtr->size() > 0) {
			theta = (*queuePtr)[queuePtr->size()-1];    
		}
	}
	else {
		theta = (*queuePtr)[queuePtr->size()-1-idx];    
	}


	if (idx >= int((queuePtr->size())-1)) {
		thetav = 0.0; 
		if (queuePtr->size() > 1) {
			thetav = 
				(theta - (*queuePtr)[queuePtr->size()-2])/STATE_UPD_STEPSIZE; 
		}
	}
	else {
		thetav = 
			(theta - (*queuePtr)[queuePtr->size()-2-idx])/STATE_UPD_STEPSIZE;  
	}
}   

void getTransformedCoordValue( const SimTK::State& s, 
	const Coordinate* coord, double& val, double& speedVal ) {
	if (coord->getName().find("ankle") != std::string::npos) {
		val = -coord->getValue(s) + SimTK::Pi/2; 
		speedVal = -coord->getSpeedValue(s);
		return;  
	}
	if (coord->getName().find("knee") != std::string::npos) {
		val = coord->getValue(s) + SimTK::Pi; 
		speedVal = coord->getSpeedValue(s);
		return;  
	}
	if (coord->getName().find("hip") != std::string::npos) {
		val = -coord->getValue(s) + SimTK::Pi; 
		speedVal = -coord->getSpeedValue(s);
		return;  
	}
}
