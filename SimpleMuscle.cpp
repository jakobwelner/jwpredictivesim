
//=============================================================================
// INCLUDES
//=============================================================================
#include "SimpleMuscle.h"
#include "Utils.h"
#include <OpenSim/Simulation/Model/CoordinateSet.h>
#include <OpenSim/Simulation/Model/Model.h>
#include <cmath>

using namespace std;
using namespace SimTK;
using namespace OpenSim;

// Added by JW
void SimpleMuscle::printInnerValues() {
	cout << endl << "Printing out innner Muscle variables" << endl;
	cout << "_r_0: " << _r_0 << endl;
	cout << "_roh: " << _roh << endl;
	cout << "_phi_ref: " << _phi_ref << endl;
	cout << "_phi_max: " << _phi_max << endl;
	cout << "_r_02: " << _r_02 << endl;
	cout << "_roh2: " << _roh2 << endl;
	cout << "_phi_ref2: " << _phi_ref2 << endl;
	cout << "_phi_max2: " << _phi_max2 << endl;
	cout << "_a0: " << _a0 << endl;
	cout << "_a1: " << _a1 << endl;
	cout << "_a2: " << _a2 << endl;
	cout << "_a3: " << _a3 << endl;
	cout << "_a4: " << _a4 << endl; 
	cout << "_a5: " << _a5 << endl;
	cout << "_a6: " << _a6 << endl;
	cout << "_epsilon_ref: " << _epsilon_ref << endl;
	cout << "_w: " << _w << endl;
	cout << "_K: " << _K << endl;
	cout << "_N: " << _N << endl;
	cout << "_Tact: " << _Tact << endl;
	cout << "_Tdeact: " << _Tdeact << endl;
	cout << "_csa: " << _csa << endl;
	cout << "_typeI_portion: " << _typeI_portion << endl;
	cout << "_numDOF: " << _numDOF << endl;
	cout << "_signToGF[0]: " << _signToGF[0] << endl;
	cout << "_signToGF[1]: " << _signToGF[1] << endl << endl;
}





// Original
//=============================================================================
// CONSTRUCTOR(S) AND SETUP
//=============================================================================
//_____________________________________________________________________________
/**
 * Default constructor.
 */
SimpleMuscle::SimpleMuscle() : Actuator()
{
    setNull();
    constructProperties();
	set_min_control(0.01); 
	set_max_control(1.0); 
}


//_____________________________________________________________________________
/** 
 * Convenience constructor.
 */
SimpleMuscle::SimpleMuscle(
    const string& name) : Actuator()
{
    setNull();
    constructProperties();

	setName(name);
	set_min_control(0.01); 
	set_max_control(1.0); 
}


//_____________________________________________________________________________
/**
 * Set the data members of this SimpleMuscle to their null values.
 */
void SimpleMuscle::setNull()
{
    setAuthors("Tim Dorn & Jack Wang");
    _r_0 = 0.05; 
	_roh = 0.5; 
	_phi_ref = 1.40; 
	_phi_max = 1.92; 
	
	_r_02 = -1; 
	_roh2 = 0.5; 
	_phi_ref2 = 1.40; 
	_phi_max2 = 1.92; 

    _a0 = 0; _a1 = 0; _a2= 0; _a3 = 0; 
    _a4 = 0; _a5 = 0; _a6 = 0; 
	
	//_l_slack = 0.26;  
	//_Fmax = 4000; 
	_epsilon_ref = 0.04;    
	_w = 0.56; 
	//_vmax= -6.0; 
	_K = 5; 
	_N = 1.5; 
	_Tact = 0.01; 
    _Tdeact = 0.02;
	_csa = 0.016;  

	_typeI_portion = 0.5; 

    _coords.clear();
    _numDOF = 0;

    _drawBody.clear();
    _drawPoint.clear();

	_signToGF[0] = 1.0; 
	_signToGF[1] = 1.0; 
}


//_____________________________________________________________________________
/**
 * Connect properties to local pointers.
 */
void SimpleMuscle::constructProperties()
{
	constructProperty_Lopt(0.04);   
    constructProperty_Fmax(4000); 
    constructProperty_Lslack(0.26); 
    constructProperty_Vmax(-10.0); 
}


//_____________________________________________________________________________
/**
 * Perform some set up functions that happen after the
 * object has been deserialized or copied.
 */
void SimpleMuscle::connectToModel(Model& aModel)
{
    // Base class
    Super::connectToModel(aModel);
}

//_____________________________________________________________________________
/** 
 * Set attachment params for the 1st coordinate (construction).
 */
void SimpleMuscle::setAttachmentParams1(double r_0, double phi_max, double phi_ref, 
	double roh) {
	_r_0 = r_0; 
	_phi_max = phi_max; 
	_phi_ref = phi_ref; 
	_roh = roh; 
	if (getName().find("ILPSO") != std::string::npos) {
		_signToGF[0] = 1.0; 
	}
	if (getName().find("GMAX") != std::string::npos) {
		_signToGF[0] = -1.0; 
	}
	if (getName().find("RF") != std::string::npos) {
		_signToGF[0] = 1.0; 
	}
	if (getName().find("HAMS") != std::string::npos) {
		_signToGF[0] = -1.0; 
	}
	if (getName().find("VAS") != std::string::npos) {
		_signToGF[0] = 1.0; 
	}
	if (getName().find("GAS") != std::string::npos) {
		_signToGF[0] = -1.0; 
	}
	if (getName().find("SOL") != std::string::npos) {
		_signToGF[0] = -1.0; 
	}
	if (getName().find("TA") != std::string::npos) {
		_signToGF[0] = 1.0; 
	}
}

#ifdef POLYMOMARMS
void SimpleMuscle::setAttachmentParamsPoly1(double a0, double a1, double a2, 
	double a3) {
	_a0 = a0; 
	_a1 = a1;  
	_a2 = a2; 
	_a3 = a3; 
    if (getName().find("ILPSO") != std::string::npos) {
		_signToGF[0] = 1.0; 
	}
	if (getName().find("GMAX") != std::string::npos) {
		_signToGF[0] = -1.0; 
	}
	if (getName().find("RF") != std::string::npos) {
		_signToGF[0] = 1.0; 
	}
	if (getName().find("HAMS") != std::string::npos) {
		_signToGF[0] = -1.0; 
	}
	if (getName().find("VAS") != std::string::npos) {
		_signToGF[0] = 1.0; 
	}
	if (getName().find("GAS") != std::string::npos) {
		_signToGF[0] = -1.0; 
	}
	if (getName().find("SOL") != std::string::npos) {
		_signToGF[0] = -1.0; 
	}
	if (getName().find("TA") != std::string::npos) {
		_signToGF[0] = 1.0; 
	}
}
#endif
	

//_____________________________________________________________________________
/** 
 * Set attachment params for the 2nd coordinate (construction).
 */
void SimpleMuscle::setAttachmentParams2(double r_0, double phi_max, double phi_ref, 
	double roh) {
	_r_02 = r_0; 
	_phi_max2 = phi_max; 
	_phi_ref2 = phi_ref; 
	_roh2 = roh; 
	if (getName().find("RF") != std::string::npos) {
		_signToGF[1] = 1.0; 
	}
	if (getName().find("HAMS") != std::string::npos) {
		_signToGF[1] = -1.0; 
	}
	if (getName().find("GAS") != std::string::npos) {
		_signToGF[1] = -1.0; 
	}
}

#ifdef POLYMOMARMS
void SimpleMuscle::setAttachmentParamsPoly2(double a4, double a5, double a6) {
	_a4 = a4; 
	_a5 = a5;  
	_a6 = a6; 
	if (getName().find("RF") != std::string::npos) {
		_signToGF[1] = 1.0; 
	}
	if (getName().find("HAMS") != std::string::npos) {
		_signToGF[1] = -1.0; 
	}
	if (getName().find("GAS") != std::string::npos) {
		_signToGF[1] = -1.0; 
	}
}
#endif

//_____________________________________________________________________________
/** 
 * Set muscle paramaters (construction).
 */
void SimpleMuscle::setMuscleParams(double csa, double eps_ref, double typeI) {

	//get_Fmax() = Fmax; 
	//get_Vmax() = v_max; 
	//_l_slack = l_slack; 
	
	_csa = csa;
	_epsilon_ref = eps_ref; 
	_typeI_portion = typeI;  

    _mass = get_Lopt()*_csa*MUSCLE_DENSITY;
}


//_____________________________________________________________________________
/** 
 * Set initial muscle state.
 */
void SimpleMuscle::initStateFromProperties(State& s) const {
	Super::initStateFromProperties(s);

    setStateVariable(s, "fiber_length", get_Lopt());    // set to optimal fiber length
    setStateVariable(s, "activation", 0.01);        // set to 0.01 activation
}


//_____________________________________________________________________________
/** 
 * Set coordinates spanned by this muscle (construction).
 */
void SimpleMuscle::setCoordinates(int numDOF, string coord1, string coord2)
{
    _numDOF = numDOF;
    _coords.clear();

    if (numDOF >= 1)
        _coords.push_back(&_model->updCoordinateSet().get(coord1));
    if (numDOF == 2)
        _coords.push_back(&_model->updCoordinateSet().get(coord2));
}


//_____________________________________________________________________________
/** 
 * Get passive buffer passive elasticity force.
 */
double SimpleMuscle::getFbe() const {
	double Fbe = 0; 
	if (muscVars.l_ce <= get_Lopt()*(1-_w)) {
//		Fbe = (muscVars.l_ce - get_Lopt()*(1-_w))/get_Lopt();
		Fbe = (muscVars.l_ce - get_Lopt()*(1-_w))/(get_Lopt()*0.5*_w);
		Fbe *= Fbe; 
	//	Fbe = get_Fmax()*Fbe/(0.5*_w);
		Fbe *= get_Fmax();
	}

	return Fbe; 
}


//_____________________________________________________________________________
/** 
 * Get passive fiber force.
 */
double SimpleMuscle::getFpe() const {
	double Fpe = 0; 
	if (muscVars.l_ce > get_Lopt()) {
			Fpe = (muscVars.l_ce - get_Lopt())/(get_Lopt()*_w);
		Fpe *= Fpe; 
		Fpe *= get_Fmax();
	}
	return Fpe; 
}
	

//_____________________________________________________________________________
/** 
 * Get tendon force.
 */
double SimpleMuscle::getFse() const {
	double Fse = 0;
	if (muscVars.l_se > get_Lslack()) {
		Fse = ((muscVars.l_se - get_Lslack())/get_Lslack())/_epsilon_ref;
		Fse *= Fse;
		Fse *= get_Fmax();  
	}
	return Fse; 	
}


//_____________________________________________________________________________
/** 
 * Get inverse FV multiplier.
 */
double SimpleMuscle::getInvfv() const {
	double v_ce = 0;

	if (muscVars.fv > _N)
		return -get_Vmax()*get_Lopt();
		
	if (muscVars.fv < 0) 
		return get_Vmax()*get_Lopt();  
	 
	if (muscVars.fv < 1.0) { 
		v_ce = get_Vmax()*(1.0 - muscVars.fv)/(1 + _K*muscVars.fv);
	}
	else {
		v_ce = ((muscVars.fv - 1)*get_Vmax())/((muscVars.fv - _N)*7.56*_K - (_N-1));
	}
	
	return v_ce*get_Lopt(); 
}


//_____________________________________________________________________________
/** 
 * Get FV multiplier.
 */
double SimpleMuscle::getfv( double v_ce ) const {
	v_ce = v_ce/get_Lopt();

	if (v_ce < 0) 
		return (get_Vmax() - v_ce)/(get_Vmax() + _K*v_ce); 
	else 
		return _N + (_N-1)*(get_Vmax() + v_ce)/(7.56*_K*v_ce - get_Vmax()); 
}


//_____________________________________________________________________________
/** 
 * Get FL multiplier.
 */
double SimpleMuscle::getfl(const State& s) const {
    double l_ce = getStateVariable(s, "fiber_length");
	double fl = std::pow(fabs((l_ce - get_Lopt())/(get_Lopt()*_w)), 3); 
	fl *= log(0.05); 
	fl = exp(fl);
	return fl; 
}


//_____________________________________________________________________________
/** 
 * Update MTU length in MuscVars.
 */
void SimpleMuscle::updateMTULength() const {

    muscVars.l_mtu = 0;
#ifdef POLYMOMARMS
    // UMBERGER DEFINITION OF MTU LENGTH
    if (_numDOF >= 1) {
        muscVars.l_mtu += _a0 
                          - _a1*muscVars.theta[0] 
                          + _a2*std::pow(muscVars.theta[0], 2)
                          - _a3*std::pow(muscVars.theta[0], 3);
    }
    if (_numDOF >= 2) {
        muscVars.l_mtu += -_a4*muscVars.theta[1] 
                          + _a5*std::pow(muscVars.theta[1], 2)
                          - _a6*std::pow(muscVars.theta[1], 3);
    }


#else
    // GEYER AND HERR DEFINITION OF MTU LENGTH
	double l_mtu = get_Lslack() + get_Lopt();
	double delta_mtu = 0;  

    if (_numDOF >= 1) {
	    if (_phi_max == -10000) { // hip joint
		    delta_mtu = _r_0*_roh*(muscVars.theta[0] - _phi_ref);
	    }
	    else {
		    delta_mtu = _r_0*_roh*(sin(muscVars.theta[0] - _phi_max) - 
			    sin(_phi_ref - _phi_max));
	    }
    }

	if (_numDOF == 2) { // biarticular muscle
		delta_mtu += _r_02*_roh2*(sin(muscVars.theta[1] - _phi_max2) - 
			sin(_phi_ref2 - _phi_max2));
	}

	l_mtu += delta_mtu; 
	muscVars.l_mtu = l_mtu; 
#endif
}


//_____________________________________________________________________________
/** 
 * Update moment arms in MuscVars (no sign -- all positive values here)
 */
void SimpleMuscle::updateMomentArms() const {
    muscVars.momArm[0] = 0;
    muscVars.momArm[1] = 0;

#ifdef POLYMOMARMS
    if (getName().find("ILPSO") != std::string::npos) {
		muscVars.momArm[0] = 0.08; 
	}
    else if (getName().find("GMAX") != std::string::npos) {
		muscVars.momArm[0] = 0.08; 
	}
    else if (getName().find("GAS") != std::string::npos) {
		muscVars.momArm[0] = 0.03; 
        muscVars.momArm[1] = -_a4 
                          + 2*_a5*muscVars.theta[1]       
                          - 3*_a6*pow(muscVars.theta[1], 2);
	}
    else {
        // UMBERGER DEFINITION OF MOMENT ARM
        if (_numDOF >= 1) {
            muscVars.momArm[0] = -_a1 
                          + 2*_a2*muscVars.theta[0]       
                          - 3*_a3*pow(muscVars.theta[0], 2);
        }
        if (_numDOF >= 2) {
            muscVars.momArm[1] = -_a4 
                          + 2*_a5*muscVars.theta[1]       
                          - 3*_a6*pow(muscVars.theta[1], 2);
        }
    }
    if (muscVars.momArm[0] < 0)
		muscVars.momArm[0] = 0; 
	if (muscVars.momArm[1] < 0)
		muscVars.momArm[1] = 0; 


#else
    // GEYER AND HERR DEFINITION OF MOMENT ARM
    if (_numDOF >= 1) {
	    if (_phi_max == -10000) {
		    muscVars.momArm[0] = _r_0; 
	    }
	    else {
		    muscVars.momArm[0] = cos(muscVars.theta[0] - _phi_max)*_r_0; 
	    }
    }
	
	if (_numDOF == 2) {     // biarticular muscle
		muscVars.momArm[1] = cos(muscVars.theta[1] - _phi_max2)*_r_02; 

	}

    if (muscVars.momArm[0] < 0)
		muscVars.momArm[0] = 0; 
	if (muscVars.momArm[1] < 0)
		muscVars.momArm[1] = 0; 
#endif
}

double SimpleMuscle::sgn(double num) const {
    if (num > 0)
        return 1.0;
    else if (num < 0)
        return -1.0;
    else 
        return 0.0;
}


//_____________________________________________________________________________
/** 
 * Get activation metabolic heat rate.
 */
double SimpleMuscle::getActivationHeatRate() const {
	//double mass = get_Lopt()*_csa*MUSCLE_DENSITY;
	return _mass*(40*(_typeI_portion)*sin(0.5*SimTK_PI*muscVars.u) + 
		133*(1-_typeI_portion)*(1-cos(0.5*SimTK_PI*muscVars.u))); 
}


//_____________________________________________________________________________
/** 
 * Get maintenance metabolic heat rate.
 */
double SimpleMuscle::getMaintenanceHeatRate() const {
	//double mass = get_Lopt()*_csa*MUSCLE_DENSITY;
	double l_hat = muscVars.l_ce/get_Lopt();
	double L = 1.0; 
	
	if (l_hat > 1.5) 
		L = 0.0; 
	else if (l_hat > 1.0)
		L = -2*l_hat + 3; 
	else if (l_hat > 0.5)
		L = l_hat; 
	else
		L = 0.5;
	return _mass*L*(74*(_typeI_portion)*sin(0.5*SimTK_PI*muscVars.a) + 
		111*(1-_typeI_portion)*(1-cos(0.5*SimTK_PI*muscVars.a))); 
}


//_____________________________________________________________________________
/** 
 * Get maintenance metabolic heat rate.
 */
void SimpleMuscle::addDrawPoint(SimTK::MobilizedBodyIndex b, SimTK::Vec3 p)
{
    _drawBody.push_back(b);
    _drawPoint.push_back(p);
}


//_____________________________________________________________________________
/** 
 * Apply generalized force to system.
 */
void SimpleMuscle::computeForce(const State& s, 
                              Vector_<SpatialVec>& bodyForces, 
                              Vector& generalizedForces) const
{
    double tendonForce = computeActuation(s);
    for (int i=0; i<_numDOF; ++i) {
        applyGeneralizedForce(s, *_coords[i], _signToGF[i]*muscVars.momArm[i]*tendonForce, generalizedForces);
//		std::cout << getName() << " " << i << " " << _signToGF[i]*muscVars.momArm[i]*tendonForce << std::endl;
    }


}


//_____________________________________________________________________________
/** 
 * Compute actuator force.
 */
double SimpleMuscle::computeActuation(const SimTK::State& s) const
{
    updateMuscleVars(s);
	setForce(s, muscVars.F_se); 
    return muscVars.F_se;
}


//_____________________________________________________________________________
/** 
 * Potential energy function.
 */
double SimpleMuscle::computePotentialEnergy(const State& s) const
{
    return 0;
}



//=============================================================================
// STATES AND THEIR DERIVATIVES
//=============================================================================
void SimpleMuscle::updateMuscleVars(const SimTK::State& s) const
{
    _model->getMultibodySystem().realize(s, Stage::Velocity);
    //if (s.getTime() != muscVars.timeLastUpdated) {
        
        muscVars.timeLastUpdated = s.getTime();
        muscVars.u = getControl(s);
        muscVars.l_ce = getStateVariable(s, "fiber_length");
        muscVars.a = getStateVariable(s, "activation");


        if (_numDOF >= 1) {
//            muscVars.theta[0] = _coords[0]->getValue(s);
  //          muscVars.thetadot[0] = _coords[0]->getSpeedValue(s);
			
			getTransformedCoordValue(s, _coords[0], muscVars.theta[0], muscVars.thetadot[0]); 

        }
        if (_numDOF == 2) {
            //muscVars.theta[1] = _coords[1]->getValue(s);
           // muscVars.thetadot[1] = _coords[1]->getSpeedValue(s);
			
			getTransformedCoordValue(s, _coords[1], muscVars.theta[1], muscVars.thetadot[1]); 
        }

        updateMomentArms();
        updateMTULength();
        muscVars.l_se = muscVars.l_mtu - muscVars.l_ce;
        muscVars.F_se = getFse();
        muscVars.F_be = getFbe();
        muscVars.F_pe = getFpe();
        muscVars.fl = getfl(s);
        muscVars.fv = (muscVars.F_se + muscVars.F_be)/(muscVars.a*get_Fmax()*muscVars.fl + muscVars.F_pe);
    //	muscVars.fv = (muscVars.F_se + muscVars.F_be - muscVars.F_pe)/(muscVars.a*get_Fmax()*muscVars.fl);
        muscVars.fvInv = getInvfv();
        if (_numDOF >=1)
            muscVars.mom[0] = _signToGF[0] * muscVars.momArm[0] * muscVars.F_se;
        if (_numDOF == 2)
        muscVars.mom[1] = _signToGF[1] * muscVars.momArm[1] * muscVars.F_se;

        updateMetabolicRate();

    //}
	/*
		if (getName() == "HAMS_r") { 
		std::cout << muscVars.l_mtu << " " << muscVars.l_ce << " " << muscVars.a << " " << muscVars.F_se << std::endl;
		std::cout << muscVars.momArm[0] << std::endl;
		}*/

}

void SimpleMuscle::addToSystem(SimTK::MultibodySystem& system) const
{
    Super::addToSystem(system);

    addStateVariable("activation");
    addStateVariable("fiber_length");
    //cout << getName() << ":  " << getStateVariableNames() << endl;
}

Array<std::string> SimpleMuscle::getStateVariableNames() const
{
    Array<std::string> stateVariableNames =
        ModelComponent::getStateVariableNames();

    for(int i=0; i<stateVariableNames.getSize(); ++i) {
        stateVariableNames[i] = getName()+"."+stateVariableNames[i];
    }
    return stateVariableNames;
}

SimTK::SystemYIndex SimpleMuscle::
getStateVariableSystemIndex(const std::string &stateVariableName) const
{
    unsigned start = (unsigned)stateVariableName.find(".");
    unsigned end   = (unsigned)stateVariableName.length();

    if(start==end) {
        return ModelComponent::getStateVariableSystemIndex(stateVariableName);
    } else {
        string localName = stateVariableName.substr(++start, end-start);
        return ModelComponent::getStateVariableSystemIndex(localName);
    }
}

Vector SimpleMuscle::computeStateVariableDerivatives(const SimTK::State& s) const
{
    updateMuscleVars(s);
    Vector derivs(2);
    double T = 0.0;
    T = _Tact;

    derivs[0] = (muscVars.u-muscVars.a)/T;    // activation dynamics ODE (a_dot)
    derivs[1] = muscVars.fvInv;                // muscle contraction dynamics ODE (lm_dot)
    return derivs;
}


//=============================================================================
// REPORTING
//=============================================================================
//_____________________________________________________________________________
/** 
 * Provide names of the quantities (column labels) of the force value(s) reported
 */
Array<string> SimpleMuscle::getRecordLabels() const 
{
    OpenSim::Array<string> labels("");
    labels.append(getName());
    return labels;
}


//_____________________________________________________________________________
/**
 * Provide the value(s) to be reported that correspond to the labels
 */
Array<double> SimpleMuscle::getRecordValues(const State& s) const 
{
    Array<double> values(1);
    values.append(1);
    return values;
}



//=============================================================================
// VISUALIZER GEOMETRY
//=============================================================================
void SimpleMuscle::generateDecorations(bool fixed, const ModelDisplayHints& hints, 
    const State& s, Array_<SimTK::DecorativeGeometry>& geometry) const
{
    Super::generateDecorations(fixed, hints, s, geometry); 

    // There is no fixed geometry to generate here.
    if (fixed) { return; }

    // Set color of muscle line.
    const SimTK::SimbodyMatterSubsystem& matter = _model->getMatterSubsystem();
    double a = getStateVariable(s, "activation");
    Vec3 col(a, 0, 1-a);        // a=0(blue muscle line); a=1(red muscle line)

    // draw the line
    for (unsigned i=0; i<_drawBody.size()-1; ++i) {
        Vec3 p2InB1 = matter.getMobilizedBody(_drawBody[i+1])
            .findStationLocationInAnotherBody(s, _drawPoint[i+1], 
            matter.getMobilizedBody(_drawBody[i]));

        geometry.push_back(DecorativeLine(_drawPoint[i], p2InB1)
                               .setLineThickness(6)
                               .setBodyId(_drawBody[i])
                               .setColor(col));
    }

}
	
void SimpleMuscle::updateMetabolicRate() const {
	double result = getActivationHeatRate() + getMaintenanceHeatRate();
	double v_ce = muscVars.fvInv;  
	if (v_ce < 0) {
		result += 0.25*muscVars.F_se*(-v_ce);
		result += (muscVars.F_se - muscVars.F_pe + muscVars.F_be)*(-v_ce);  
	}

	muscVars.metabolicrate = result; 
}
