// For z-padding
#include <iomanip>
#include <iostream>
#include <time.h>

#include <OpenSim/OpenSim.h>
#include <OpenSim/Common/IO.h>

// Messaging Passing Interface
#ifdef USE_MPI
#include <mpi.h>
#endif

#include "CMAOptimizer.h"
#include "GeyerHerrController.h"
#include "EventHandlers.h"
#include "SimpleMuscle.h"
#include "SoftCoordinateLimitForce.h"
#include "PredictiveOptimizationSystem.h"
#include "Utils.h"

using namespace std;
using namespace OpenSim;
using namespace SimTK;

void replaySim(Model& model, const string& replayFile);
State constructSimbodyState(const Model& model, const State& sTemplate, 
    const Array_<double>& stateValues, const Array_<string>& stateNames, double time);


string VERSION_NUMBER = "0012";
string VERSION_NAME = "No velocity error margin";

//==============================================================================
// MAIN FUNCTION
//==============================================================================
int main(int argc, char **argv)
{
	// Initiating timer for result_opt_progress.csv
	time_t currTime;
	time_t startTime;

	int secondsPassedTotal = 0;
	int secondsPassedItr = 0;
	int secondsPassedLast = 0;
	int itrTimeLimit = 600; // In seconds = 10min. Quit if iterations take longer than this
	time(&startTime);


    int myrank = 0; 
    int numtasks = 0; 

    // Dummy code to initialize the Lapack libraries.
    SimTK::Matrix junk(1,1); junk(0,0)=1.234;
    SimTK::FactorLU junklu(junk);

#ifdef USE_MPI
    int rc = MPI_Init(&argc, &argv);
    if (rc != MPI_SUCCESS) {
        printf ("Error starting MPI program. Terminating.\n");
        MPI_Abort(MPI_COMM_WORLD, rc);
    }

    numtasks = 0;
    myrank = 0;
    MPI_Comm_size(MPI_COMM_WORLD,&numtasks);
    MPI_Comm_rank(MPI_COMM_WORLD,&myrank);
    printf ("Number of processes = %d; My rank= %d\n", numtasks, myrank);
#endif

	double finalTime = 1000; 
    double targetVel = 1.5;
    double backpackLoad = -1.0;
	double pertAmp = 0;
	double stepSize = 0.005;
	double save_ctrl_at_time = 1.0; // Save controls at time
	bool restart = false;
	bool visualize = false;
    bool printDetailsToFile = false;
	bool doOpt = false; 
	bool optAccel = false;
	bool save_all_sim = false;
	
	bool supraCtrl = false;
	bool load_ctrl_values = false;
	bool save_ctrl_values = false;
	bool blind_random_search = false;

	int popMult = 1;
    int resumeIndex = 0;
	int restartCount = 0;
	int maxOuterIters = 3000;
	
    string replayFile = "Unassigned";
	string modelFile("../Humanoid2D.osim");  
	string ctrlPrefix("../optimization"); 
	string ctrlFile("../optimization_values.sto");

	// Filename for dumping all sim data
	String dump_filename = "sim_dump.csv"; 
		
	for (int i = 0; i < argc; i++) {
        if (strcmp(argv[i], "-m") == 0) {
           	modelFile = argv[i+1];
        }
        
		if (strcmp(argv[i], "-cf") == 0) {
           	ctrlFile = argv[i+1];
        }

        if (strcmp(argv[i], "-t") == 0) {
            finalTime = atof(argv[i+1]);
        }

        if (strcmp(argv[i], "-resume") == 0) {
            resumeIndex = atoi(argv[i+1]);
        }

        if (strcmp(argv[i], "-target") == 0) {
            targetVel = atof(argv[i+1]);
        }

        if (strcmp(argv[i], "-load") == 0) {
            backpackLoad = atof(argv[i+1]);
        }
        
		if (strcmp(argv[i], "-viz") == 0) {
			visualize = true; 
        }

        if (strcmp(argv[i], "-printDetails") == 0) {
			printDetailsToFile = true; 
        }

        if (strcmp(argv[i], "-replay") == 0) {
			replayFile = argv[i+1]; 
        }
        
		if (strcmp(argv[i], "-opt") == 0) {
			doOpt = true; 
            stepSize = atof(argv[i+1]);
        }
		////////////////// Added by JW ////////////////////////
		// If used will optimize for acceleration specified by target
		// [NOT IMPLEMENTED YET]
		if (strcmp(argv[i], "-optAccel") == 0) {
			//doOpt = true; // Should probably set this as well. 
			optAccel = true;
			stepSize = atof(argv[i + 1]);
		}
		// Defining prefix for settings files
		if (strcmp(argv[i], "-ctrlPrefix") == 0) {
			ctrlPrefix = argv[i + 1];
			ctrlFile = ctrlPrefix + "_values.sto";
		}
		// Defining perturbation amplitude
		if (strcmp(argv[i], "-pertAmp") == 0) {
			pertAmp = atof(argv[i + 1]);
		}
		// Printing out current version and quits
		if (strcmp(argv[i], "-version") == 0) {
			cout << "Version: " << VERSION_NUMBER << " (" << VERSION_NAME << ")" << endl;
			return 0;
		}
		// Enables passing supraspinal data to reflex system 
		// (currently sourced from hardcoded files with predetermined timing)
		if (strcmp(argv[i], "-supraCtrl") == 0) {
			supraCtrl = true;
		}
		// Enables loading controller parameters from hardcoded "controllerState/" folder
		if (strcmp(argv[i], "-loadCtrls") == 0) {
			load_ctrl_values = true;
		}
		// Enables saving controller parameters to hardcoded "controllerState/" folder
		if (strcmp(argv[i], "-saveCtrls") == 0) {
			save_ctrl_at_time = atof(argv[i + 1]);
			save_ctrl_values = true;
		}
		// allows to specify larger search space by multiplying population size by this value
		if (strcmp(argv[i], "-popMult") == 0) {
			popMult = (int) atof(argv[i + 1]);
			if (popMult < 1) {
				cout << "popMult needs to be an integer larger than 0" << endl;
			return 0;
			}
		}
		// Enables saving out all simulations as result_coreSim_#.sto files
		if (strcmp(argv[i], "-saveAllSim") == 0) {
			save_all_sim = true;
		}
		// Uses Blind Random Search rather than CMA-ES for optimization
		// specify maximum outer iterations before break
		if (strcmp(argv[i], "-brs") == 0) {
			blind_random_search = true;
			maxOuterIters = (int)atof(argv[i + 1]);
		}
	}
	
	Object::RegisterType( OpenSim::GeyerHerrController() ); 
	Object::RegisterType( OpenSim::SimpleMuscle() ); 
	Object::RegisterType( OpenSim::SoftCoordinateLimitForce() ); 
	IO::SetPrecision(DBL_DIG+6); 
	
    if (replayFile != "Unassigned") {
		//cout << endl << "### main.cpp: Model model(modelFile);" << endl;
        Model model(modelFile);
        replaySim(model, replayFile);
        return 0;
    }

	//cout << endl << "### main.cpp: Storage allCtrls(ctrlFile);" << endl;
	Storage allCtrls(ctrlFile);
	Array<double>& allSimParams = allCtrls.getLastStateVector()->getData();  

	Vector controls; 	
	Vector lb; 	// Lower Bounds
	Vector ub; 	// Upper Bounds

	// Normalizing parameter values by range and setting bounds
	convertToOptInput( allSimParams, ctrlPrefix, controls, lb, ub );
	

	/////////////////////////////////////////////////////////////////////////////////
	// Initiate CSV file for dumping all sim data
	if (save_all_sim) {
		
		std::ofstream out(dump_filename);
		std::ostringstream print_string;

		print_string << "finaltime=" << finalTime << endl;
		print_string << "targetVel=" << targetVel << endl;
		print_string << "backpackLoad=" << backpackLoad << endl;
		print_string << "visualize=" << visualize << endl;
		print_string << "printDetailsToFile=" << printDetailsToFile << endl;
		print_string << "doOpt=" << doOpt << endl;
		print_string << "resumeIndex=" << resumeIndex << endl;
		print_string << "stepSize=" << stepSize << endl;
		print_string << "replayFile=" << replayFile << endl;
		print_string << "modelFile=" << modelFile << endl;
		print_string << "ctrlFile=" << ctrlFile << endl;
		print_string << "ctrlPrefix=" << ctrlPrefix << endl;
		print_string << "numtasks=" << numtasks << endl;
		print_string << "pertAmp=" << pertAmp << endl;
		print_string << "secondsPassedTotal=" << secondsPassedTotal << endl;
		print_string << "secondsPassedItr=" << secondsPassedItr << endl;
		print_string << "blind_random_search=" << blind_random_search << endl;
		print_string << "VERSION_NAME=" << VERSION_NAME << endl;
		print_string << "VERSION_NUMBER=" << VERSION_NUMBER << endl;

		// Finding all active parameters and printing their labels to file
		Storage activeIdx(ctrlPrefix + "_activeparams.sto");
		Array<double>& allActiveIdx = activeIdx.getLastStateVector()->getData();
		for (int i = 0; i < allSimParams.getSize(); i++) {
			if (allActiveIdx.get(i) == 1.0) {
				print_string << activeIdx.getColumnLabels()[i + 1] << "\t";
			}
		}
		print_string << "fitness\n";
		out << print_string.str();
		out.close();
	}
	/////////////////////////////////////////////////////////////////////////////////


	OpenSim::PredictiveOptimizationSystem 
		pdsimSys(controls.size(), 
				modelFile, 
				ctrlPrefix, 
				finalTime, 
				visualize, 
				targetVel, 
				backpackLoad, 
				printDetailsToFile, 
				optAccel, 
				pertAmp, 
				supraCtrl, 
				load_ctrl_values, 
				save_ctrl_values, 
				save_ctrl_at_time, 
				doOpt,
				save_all_sim);
	
	pdsimSys.setParameterLimits(lb, ub);
	
	double f = 0.0;
	double bestVal = 0.0;  
	int noImprovLimit = 30;
	int noImprov = 0;

	if (doOpt) {
		SimTK::CMAOptimizer opt(pdsimSys);
		// Specify settings for the optimizer
		opt.setMaxIterations(10);
		opt.setAdvancedIntOption("lambda", (int)((numtasks * popMult) - 1)); // Original in PredictiveSim
		//opt.setAdvancedIntOption("lambda", 0); // Using CMAES default setting for population size
		opt.setAdvancedRealOption("sigma", stepSize);
		opt.setAdvancedBoolOption("enableMPI", false);

		// Setting Optimizer Algorithm: Using CMA-ES as default
		opt.setAdvancedBoolOption("blindRandomSearch", blind_random_search);
		opt.setAdvancedStrOption("dump_filename", dump_filename);
		opt.setAdvancedStrOption("ctrlPrefix", ctrlPrefix);
		

#ifdef USE_MPI
		if (numtasks > 1)
			opt.setAdvancedBoolOption("enableMPI", true);

		//		if (myrank > 0) 
		//			opt.optimize(controls); 
#endif
//		int status = system("rm resumecmaes.dat");


		// Initial simulation to get fitness of start conditions for comparison
		pdsimSys.objectiveFunc(controls, true, f);
		bestVal = f;

		cout << myrank << " " << "Initial Best Val " << f << endl;

		for (int i = resumeIndex; i < maxOuterIters; i++) {
			opt.setAdvancedBoolOption("resume", i > 0);

			f = opt.optimize(controls);


			// Check time
			time(&currTime);  /* get current time; same as: timer = time(NULL)  */
			secondsPassedTotal = (int) difftime(currTime, startTime);
			secondsPassedItr = secondsPassedTotal - secondsPassedLast;
			secondsPassedLast = secondsPassedTotal;

			if (f < bestVal) {
				bestVal = f;
				cout << bestVal << endl;

				convertFromOptInput( controls, ctrlPrefix, allSimParams );

				// Adding metadata to result file
				std::ostringstream description;
				description << "execFile=" << argv[0] << endl << "execCmd=";
				for (int k = 1; k < argc; k++) {
					description << argv[k] << " ";
				}
				description << endl;
				description << "finaltime=" << finalTime << endl;
				description << "targetVel=" << targetVel << endl;
				description << "backpackLoad=" << backpackLoad << endl;
				description << "visualize=" << visualize << endl;
				description << "printDetailsToFile=" << printDetailsToFile << endl;
				description << "doOpt=" << doOpt << endl;
				description << "resumeIndex=" << resumeIndex << endl;
				description << "stepSize=" << stepSize << endl;
				description << "replayFile=" << replayFile << endl;
				description << "modelFile=" << modelFile << endl;
				description << "ctrlFile=" << ctrlFile << endl;
				description << "ctrlPrefix=" << ctrlPrefix << endl;
				description << "fitness=" << f << endl;
				description << "noImprovLimit=" << noImprovLimit << endl;
				description << "numtasks=" << numtasks << endl;
				description << "pertAmp=" << pertAmp << endl;
				description << "secondsPassedTotal=" << secondsPassedTotal << endl;
				description << "secondsPassedItr=" << secondsPassedItr << endl;
				description << "blind_random_search=" << blind_random_search << endl;
				description << "VERSION_NAME=" << VERSION_NAME << endl;
				description << "VERSION_NUMBER=" << VERSION_NUMBER << endl;

				
				allCtrls.setDescription(description.str());

				allCtrls.print("result_itr" + convertInt(i) + ".sto"); 
				
				// Resetting counter on improvement
				noImprov = 0;
	
            }
			// Counting how many consecutive runs without improvement
			else {
				noImprov += 1;
			}

            // Will return here after each inner loop (e.g. every 10 iterations)
            FILE* pFile;
            pFile = fopen("result_opt_progress.csv", "a");
            fprintf(pFile, "%d,%f,%f,%d\n", i, f, bestVal, secondsPassedTotal);
            fclose(pFile);

			cout << endl << "Outer Iter " << i << ": Val " << f << " BestVal " << bestVal << endl;
			cout << endl << "Iteration time: " << secondsPassedItr / 60 << " min." << endl;

			// Break Loop and finish optimization if noImprov reaches limit
			if (noImprov == noImprovLimit && !blind_random_search) {
				cout << "Reached limit for no improvements in " << noImprovLimit << " consecutive iterations. Quitting" << endl;
				break;
			}
			// Allow 3 iterations and then start checking for exceedingly long iteration times. Quit if they exceed limit
			//if (secondsPassedItr > itrTimeLimit) { 
			//	cout << "Spending too long on iterations: " << secondsPassedItr/60 << "min. - Quitting " << endl;
			//	break;
			//}
		}
	}
	else {
		// Simulate the current ControlFile
        pdsimSys.setPrintResults(true);
		pdsimSys.objectiveFunc(controls, true, f);
		std::cout << "Objective " << f << std::endl;
	}
	
#ifdef USE_MPI
	for (int rank = 1; rank < numtasks; ++rank) {
		MPI_Send(0, 0, MPI_INT, rank, DIETTAG, MPI_COMM_WORLD);
	}
	MPI_Finalize();
#endif
	return 0; 
}



//__________________________________________________________________________
/**
 * Replay a saved simulation for visualization.
 */
void replaySim(Model& model, const string& replayFile) {

    // ------------------------------------------------------
    // ------------------------------------------------------
    // Prepare model and data.
    // ------------------------------------------------------
    // ------------------------------------------------------
    double REPLAY_DATA_RATE = 100;
    double REPLAY_FRAME_RATE = 30;
    double SPEED_INCREMENT = 0.1;
    const double speedMultiplier = REPLAY_DATA_RATE / REPLAY_FRAME_RATE;

    model.updControllerSet().clearAndDestroy();
    model.setUseVisualizer(true);
    model.buildSystem();
    State& s = model.initializeState();
    PredictiveOptimizationSystem::initAllMuscles(model, 0);
    SimbodyMatterSubsystem& matter = model.updMatterSubsystem();

    // Convert data into an array of SimTK::State objects.
    // First need to resample data to 100Hz (specified by REPLAY_DATA_RATE)
	//cout << endl << "### main.cpp new Storage(replayFile);" << endl;
    Storage* results = new Storage(replayFile);
    results->resample(1/REPLAY_DATA_RATE, 4);
    const string sampledFileName = "_resultsSampled.sto";
    results->print(sampledFileName);
    cout << "Saved sampled results (" << 1/REPLAY_DATA_RATE << " Hz) to: " << sampledFileName << endl;

    const int numRows = results->getSize();
    Array<string> resultColNames = results->getColumnLabels();   // does include time
    Array_<string> stateNames;
    for (int j=1; j<51; ++j)
        stateNames.push_back(resultColNames[j]);   
    //cout << "stateNames = " << stateNames.size() << endl;

    Array_<State> savedStates;
    savedStates.clear();
    for (int i=0; i<numRows; ++i) {
        // extract the state from the storage and convert to a SimTK::State.
        StateVector* vState = results->getStateVector(i);    // doesn't include time
        Array_<double> stateValues;
        for (int j=0; j<50; ++j) {
            double val;
            vState->getDataValue(j, val);
            stateValues.push_back(val);
        }

        //for (unsigned j=0; j<stateValues.size(); ++j)
        //    cout << "row=" << i << " col=" << j << "   " << stateNames[j] << " = " << stateValues[j] << endl;
        //system("pause");
        
        State s_Saved = constructSimbodyState(model, s, stateValues, stateNames, vState->getTime());
        //cout << i << endl;
        savedStates.push_back(s_Saved);
    }
    const double finalTime = savedStates[numRows-1].getTime();
    cout << "Constructed array of " << numRows << " SimTK::States from t=0 to t=" << finalTime << endl;
    delete results;



    // ------------------------------------------------------
    // ------------------------------------------------------
    // Set up the visualizer for REPLAYING A SIMULATION.
    // ------------------------------------------------------
    // ------------------------------------------------------
    SimTK::Visualizer& viz = model.updVisualizer().updSimbodyVisualizer();
	viz.setWindowTitle("Predictive Simulation REPLAY");
    viz.setMode(Visualizer::PassThrough);
    viz.setDesiredBufferLengthInSec(0);
    viz.setDesiredFrameRate(REPLAY_FRAME_RATE);        // Hz
    model.updMatterSubsystem().setShowDefaultGeometry(false);
	viz.setBackgroundType(viz.GroundAndSky);
	viz.setGroundHeight(0.0);
	viz.setShowShadows(true);
	viz.setShowFrameRate(true);
	viz.setShowSimTime(true);
	viz.setShowFrameNumber(false);
    


    // Set initial position and orientation of camera.
    // ------------------------------------------------------
    Vec3 cameraPos(4, 1, 10);
    UnitVec3 cameraZ(0,0,1);
    viz.setCameraTransform(Transform(Rotation(cameraZ, ZAxis, UnitVec3(YAxis), YAxis), cameraPos));
    viz.pointCameraAt(Vec3(0,0,0), Vec3(0,1,0));


    // Add frame controllers.
    // ------------------------------------------------------
    const OpenSim::Body& pelvis = model.getBodySet().get("pelvis");
    OpenSim::BodyWatcher* bodyWatch = new OpenSim::BodyWatcher(matter.getMobilizedBody(pelvis.getIndex()), 4, -0.3, 0.0);
    viz.addFrameController(bodyWatch);


    // Add slider bars.
    // ------------------------------------------------------
    int SpeedSlider=1, TimeSlider=2, MIN_SPEED=0, MAX_SPEED=4;
    Visualizer::InputSilo& silo = model.updVisualizer().updInputSilo();
    viz.addSlider("Speed", SpeedSlider, MIN_SPEED, MAX_SPEED, 1.0);
    double speed = 1*speedMultiplier;       // initial speed
    viz.addSlider("Time", TimeSlider, 0, finalTime, 0.0);
    double time = 0.0;                      // initial time


    // ------------------------------------------------------
    // ------------------------------------------------------
    // Playback the simulation.
    // ------------------------------------------------------
    // ------------------------------------------------------
    silo.clear(); // forget earlier input
    double prevSpeed = speed;       // set to initial speed
    int prevFrame = -1;

    while(true) {
        for (double i=0; i<(int)numRows; i+=speed) {
            int whichSlider; Real newSliderValue;
            time = i/numRows;

            // ==================
            // SLIDER INPUT
            // ==================
            if (silo.takeSliderMove(whichSlider, newSliderValue)) {

                // Speed Slider Selected
                if (whichSlider == SpeedSlider) {
                    speed = speedMultiplier*newSliderValue;
                    //cout << "speed = " << speed << endl;
                } 

                // Time Slider Selected
                else if (whichSlider == TimeSlider) {
                    time = newSliderValue;
                    i = time*REPLAY_DATA_RATE;
                    //cout << "time = " << time << endl;
                }
            }

            // ==================
            // USER KEY INPUT
            // ==================
            unsigned key, modifiers;
            if (silo.takeKeyHit(key, modifiers)) {

                // Exit viewer: ESC
                if (key == Visualizer::InputListener::KeyEsc) {
                    cout << "Exiting viewer." << endl;
                    return;
                }

                // Pause viewer: SPACE
                else if (key == ' ') {
                    if (speed != 0.0) {
                        prevSpeed = speed;
                        speed = 0.0;
                    }
                    else
                        speed = prevSpeed;
                }

                // Forward one frame: RIGHT ARROW
                else if (key == Visualizer::InputListener::KeyRightArrow) {
                    if ((int)i != numRows)
                        i = i + 1.0;
                }
                    
                // Back one frame: LEFT ARROW
                else if (key == Visualizer::InputListener::KeyLeftArrow) {
                    if (i<0.5)  i = 0;
                    else if ((int)i != 0)
                        i = i - 1.0;
                }

                // Increase speed: UP ARROW
                else if (key == Visualizer::InputListener::KeyUpArrow) {
                    if (speed > (MAX_SPEED-SPEED_INCREMENT))
                        speed = MAX_SPEED*speedMultiplier;
                    else
                        speed += SPEED_INCREMENT*speedMultiplier;
                }

                // Decrease speed: DOWN ARROW
                else if (key == Visualizer::InputListener::KeyDownArrow) {
                    if (speed < MIN_SPEED+SPEED_INCREMENT)
                        speed = MIN_SPEED*speedMultiplier;
                    else
                        speed -= SPEED_INCREMENT*speedMultiplier;
                }

                // Set player speed to realtime
                else if (key == 'e') {
                    speed = 1.0 * speedMultiplier;
                }

                // Camera view: zoom in camera in pan mode.
                else if (key == Visualizer::InputListener::KeyPageUp) {
                    bodyWatch->incrementZoomDistance(-0.04);
                }

                // Camera view: zoom out camera in pan mode.
                else if (key == Visualizer::InputListener::KeyPageDown) {
                    bodyWatch->incrementZoomDistance(0.04);
                }

                // Camera view: move camera up.
                else if (key == '8') {
                    bodyWatch->incrementVerticalCameraTranslation(0.02);
                }

                // Camera view: move camera down.
                else if (key == '5') {
                    bodyWatch->incrementVerticalCameraTranslation(-0.02);
                }

                // Camera view: rotate camera left.
                else if (key == '6') {
                    bodyWatch->incrementHorizontalCameraTranslation(0.02);
                }

                // Camera view: rotate camera right.
                else if (key == '4') {
                    bodyWatch->incrementHorizontalCameraTranslation(-0.02);
                }

                // Camera view: zoom appropiately
                else if (key == 'r') {
                    viz.zoomCameraToShowAllGeometry();
                }

                // Play at slow speed.
                else if (key == Visualizer::InputListener::KeyF2) {
                    speed = 0.2*speedMultiplier;
                }

                // Play at normal speed.
                else if (key == Visualizer::InputListener::KeyF3) {
                    speed = 1.0*speedMultiplier;
                }

                // Play at fast speed.
                else if (key == Visualizer::InputListener::KeyF4) {
                    speed = 3.0*speedMultiplier;
                }
            }


            // ==================
            // REPORT TO SCREEN
            // ==================
            // Checks (these may be needed because we may have
            // changed the value of i (frame to be displayed) 
            // inside the loop, which may potentially place i
            // out of bounds in an array.
            if (i<0)  i = 0;
            if ((int)i==numRows)  i = numRows-1;
            //cout << (int)i << "   " << numRows << endl;
                
            // Set values to the slider.
            viz.setSliderValue(1, speed/speedMultiplier);
            viz.setSliderValue(2, i/REPLAY_DATA_RATE);

            // Report the scene to the visualizer.
            viz.report(savedStates[(int)i]);
            prevFrame = (int)i;
        }
    }


}



//__________________________________________________________________________
/**
 * Convert a state from storage into a SimTK::State. This will set the
 * q's, u's and z's of a Simbody state.
 */
State constructSimbodyState(const Model& model, const State& sTemplate, 
    const Array_<double>& stateValues, const Array_<string>& stateNames, double time)
{
    State s(sTemplate);         // copy state
    s.setTime(time);
    //cout << "STATES = " << model.getStateVariableNames() << endl;
    if (stateValues.size() != stateNames.size()) {
        cout << stateNames << endl;
        stringstream errorMessage;
        errorMessage << "state sizes do not match! (" << stateNames.size() 
            << " state names and " << stateValues.size() << " states)." << endl;
        throw (OpenSim::Exception(errorMessage.str()));
    }

    // -----------------------------
    // Set continuous states.
    // -----------------------------
    const int numStates = stateNames.size();
    //cout << numStates << endl;
    //cout << model.getStateVariableNames() << endl;
    for (int i=0; i<numStates; ++i) {
        //double val = stateValues[i];
        //cout << "val (" << i << "/" << numStates <<") = " << val << endl;
        // Set the Simbody state.
        model.setStateVariable(s, stateNames[i], stateValues[i]);
        //cout << "set " << stateNames[i] << " to: " << val << endl;
    }

    //model.getMultibodySystem().realize(s, Stage::Dynamics);
    return s;
}
