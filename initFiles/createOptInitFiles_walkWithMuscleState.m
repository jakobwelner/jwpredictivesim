% Create initialization files for predictive optimization.
% For use with Humanoid2D.osim
% 
% Tim Dorn
% 
% bound = real bound for unnormalized design variable
% range (used by CMA) = the un-normalized design values 
%       that corresponds to a normalized value of [0, 1].
% 
% =============================================================

%% Set file names
% -------------------------------------------------------------
close all
clear all
valuesFile = 'withMuscleState_values.sto';
boundsFile = 'withMuscleState_bounds.sto';
rangesFile = 'withMuscleState_ranges.sto';
toOptimizeFile = 'withMuscleState_activeparams.sto';
N = 400;
d2r = pi/180;
r2d = 180/pi;
INIT{N} = [];
unusedCounter(N) = 0;           % used to count the number of unused optimization indices
noOptimizeCounter(N) = 0;       % used to count the number of non-optimizied but specified parameters


%% Initialization parameters -- INITIAL COORDINATE STATES
% bound = real bound for unnormalized design variable
% range = the un-normalized design values that corresponds to a normalized value of [0, 1]
% INIT{X} = {'name',                    init_value,   [bound, bound],        [range, range],     toOptimize}
%                                                      min    max             low    high
% --------------------------------------------------------------------------------------------------------------------------------
DEFAULT   = {'opt_',                   0,            [0 1],                  [0 1],               false};    % DEFAULT
INIT{1}   = {'trunk_extension',       -0.0873,       [-pi pi],               [-pi pi],            false};     % trunk_extension_q
INIT{4}   = {'trunk_tx',               0,            [-pi pi],               [-pi pi],            false};    % trunk_tx_q
INIT{5}   = {'trunk_ty',               1.54,         [0 1.7],                [0 1.7],             false};     % trunk_ty_q
INIT{23}  = {'hip_r_flexion',          0.0873,       [-1.047 2.87],          [-1.047 2.87],       false};     % hip_r_flexion_q
INIT{26}  = {'knee_r_extension',      -0.0873,       [-2.87 0.087],          [-2.87 0.087],       false};     % knee_r_extension_q
INIT{27}  = {'ankle_r_dorsiflexion',   0.0873,       [-0.872 0.523],         [-0.872 0.523],      false};     % ankle_r_dorsiflexion_q
INIT{30}  = {'hip_l_flexion',          0.698,         [-1.047 2.87],         [-1.047 2.87],       false};     % hip_l_flexion_q
INIT{33}  = {'knee_l_extension',      -0.0873,       [-2.87 0.087],          [-2.87 0.087],       false};     % knee_l_extension_q
INIT{34}  = {'ankle_l_dorsiflexion',   0,            [-0.872 0.523],         [-0.872 0.523],      false};     % ankle_l_dorsiflexion_q
INIT{37}  = {'trunk_extension_u',      0,            [-pi pi],               [-pi pi],            false};     % trunk_extension_u
INIT{40}  = {'trunk_tx_u',             1.3,          [0 pi],                 [-pi pi],            false};     % trunk_tx_u
INIT{41}  = {'trunk_ty_u',             0,            [-pi pi],               [-pi pi],            false};     % trunk_ty_u
INIT{59}  = {'hip_r_flexion_u',        0,            [-pi pi],               [-pi pi],            false};     % hip_r_flexion_u
INIT{62}  = {'knee_r_extension_u',     0,            [-pi pi],               [-pi pi],            false};     % knee_r_extension_u
INIT{63}  = {'ankle_r_dorsiflexion_u', 0,            [-pi pi],               [-pi pi],            false};     % ankle_r_dorsiflexion_u
INIT{66}  = {'hip_l_flexion_u',        0,            [-pi pi],               [-pi pi],            false};     % hip_l_flexion_u
INIT{69}  = {'knee_l_extension_u',     0,            [-pi pi],               [-pi pi],            false};     % knee_l_extension_u
INIT{70}  = {'ankle_l_dorsiflexion_u', 0,            [-pi pi],               [-pi pi],            false};     % ankle_l_dorsiflexion_u

% muscle states - Is set by python script
INIT{100}  = {'SOL_r.activation',        0.01,          [0 pi],                   [-pi pi],               false};
INIT{101}  = {'SOL_r.fiber_length',      0.039132,      [0 pi],                   [-pi pi],               false};
INIT{102}  = {'SOL_l.activation',        0.423007,      [0 pi],                   [-pi pi],               false};
INIT{103}  = {'SOL_l.fiber_length',      0.031914,      [0 pi],                   [-pi pi],               false};
INIT{104}  = {'TA_r.activation',         0.149382,      [0 pi],                   [-pi pi],               false};
INIT{105}  = {'TA_r.fiber_length',       0.042778,      [0 pi],                   [-pi pi],               false};
INIT{106}  = {'TA_l.activation',         0.01,          [0 pi],                   [-pi pi],               false};
INIT{107}  = {'TA_l.fiber_length',       0.04808,       [0 pi],                   [-pi pi],               false};
INIT{108}  = {'GAS_r.activation',        0.01436,       [0 pi],                   [-pi pi],               false};
INIT{109}  = {'GAS_r.fiber_length',      0.05331,       [0 pi],                   [-pi pi],               false};
INIT{110}  = {'GAS_l.activation',        0.62907,       [0 pi],                   [-pi pi],               false};
INIT{111}  = {'GAS_l.fiber_length',      0.0383,        [0 pi],                   [-pi pi],               false};
INIT{112}  = {'VAS_r.activation',        0.01001,       [0 pi],                   [-pi pi],               false};
INIT{113}  = {'VAS_r.fiber_length',      0.0447,        [0 pi],                   [-pi pi],               false};
INIT{114}  = {'VAS_l.activation',        0.188828,      [0 pi],                   [-pi pi],               false};
INIT{115}  = {'VAS_l.fiber_length',      0.0494795,     [0 pi],                   [-pi pi],               false};
INIT{116}  = {'HAMS_r.activation',       0.02419,       [0 pi],                   [-pi pi],               false};
INIT{117}  = {'HAMS_r.fiber_length',     0.09598,       [0 pi],                   [-pi pi],               false};
INIT{118}  = {'HAMS_l.activation',       0.0192799,          [0 pi],                   [-pi pi],               false};
INIT{119}  = {'HAMS_l.fiber_length',     0.05473,          [0 pi],                   [-pi pi],               false};
INIT{120}  = {'RF_r.activation',         0.029046,          [0 pi],                   [-pi pi],               false};
INIT{121}  = {'RF_r.fiber_length',       0.0344,          [0 pi],                   [-pi pi],               false};
INIT{122}  = {'RF_l.activation',         0.010,          [0 pi],                   [-pi pi],               false};
INIT{123}  = {'RF_l.fiber_length',       0.06587,          [0 pi],                   [-pi pi],               false};
INIT{124}  = {'GMAX_r.activation',       0.01,          [0 pi],                   [-pi pi],               false};
INIT{125}  = {'GMAX_r.fiber_length',     0.1,          [0 pi],                   [-pi pi],               false};
INIT{126}  = {'GMAX_l.activation',       0.01,          [0 pi],                   [-pi pi],               false};
INIT{127}  = {'GMAX_l.fiber_length',     0.0729,          [0 pi],                   [-pi pi],               false};
INIT{128}  = {'ILPSO_r.activation',      0.0679,          [0 pi],                   [-pi pi],               false};
INIT{129}  = {'ILPSO_r.fiber_length',    0.087,          [0 pi],                   [-pi pi],               false};
INIT{130}  = {'ILPSO_l.activation',      0.1,          [0 pi],                   [-pi pi],               false};
INIT{131}  = {'ILPSO_l.fiber_length',    0.119,          [0 pi],                   [-pi pi],               false};

% Ground contact parameters
INIT{304}  = {'stiffness',             2e7,          [0 1e9],                [1e6 1e8],           false};    
INIT{305}  = {'dissipation',           2,            [0 100],                [0 10],              false};   
INIT{306}  = {'static_friction',       0.8,          [0 10],                 [0 5],               false};    
INIT{307}  = {'dynamic_friction',      0.8,          [0 10],                 [0 1],               false};     
INIT{308}  = {'viscous_friction',      0.5,          [0 10],                 [0 5],               false};   
INIT{309}  = {'transition_velocity',   0.1,          [0 10],                 [0 0.1],             false};    

% MTU controller parameters
INIT{310}  = {'GMAX_p',                0.05,         [0 1],                  [0 1], true};    
INIT{311}  = {'ILPSO_p',               0.05,         [0 1],                  [0 1], true};    
INIT{312}  = {'HAMS_p',                0.05,         [0 1],                  [0 1], true};    
INIT{313}  = {'RF_p',                  0.01,         [0 1],                  [0 1], true};    
INIT{314}  = {'VAS_p',                 0.09,         [0 1],                  [0 1], true};    
INIT{315}  = {'GAS_p',                 0.01,         [0 1],                  [0 1], true};    
INIT{316}  = {'SOL_p',                 0.01,         [0 1],                  [0 1], true};    
INIT{317}  = {'TA_p',                  0.01,         [0 1],                  [0 1], true};    
INIT{318}  = {'GMAX_q',                0.01,         [0 1],                  [0 1], true};    
INIT{319}  = {'ILPSO_q',               0.01,         [0 1],                  [0 1], true};    
INIT{320}  = {'HAMS_q',                0.01,         [0 1],                  [0 1], true};    
INIT{321}  = {'RF_q',                  0.01,         [0 1],                  [0 1], true};    
INIT{322}  = {'VAS_q',                 0.01,         [0 1],                  [0 1], true};    
INIT{323}  = {'GAS_q',                 0.01,         [0 1],                  [0 1], true};    
INIT{324}  = {'SOL_q',                 0.01,         [0 1],                  [0 1], true};    
INIT{325}  = {'TA_q',                  0.01,         [0 1],                  [0 1], true};    
INIT{326}  = {'G_sol',                 1.2,          [0 20],                 [0 10], true};    
INIT{327}  = {'G_ta',                  1.1,          [0 20],                 [0 10], true};    
INIT{328}  = {'l_off_ta',              0.71,         [0 20],                 [0 10], true};    
INIT{329}  = {'G_solta',               0.3,          [0 20],                 [0 10], true};    
INIT{330}  = {'G_gas',                 1.1,          [0 20],                 [0 10], true};    
INIT{331}  = {'G_vas',                 1.15,         [0 20],                 [0 10], true};    
INIT{332}  = {'k_phi',                 2,            [0 20],                 [0 10], true};    
INIT{333}  = {'phi_k_off',             2.97,         [-pi pi],               [0 1], true};    
INIT{334}  = {'k_p_glu',               0.68*1.91,    [0 20],                 [0 10], true};    
INIT{335}  = {'theta_ref',             0.105,        [-pi pi],               [0 1], true};    
INIT{336}  = {'k_d_glu',               0.25,         [0 20],                 [0 10], true};    
INIT{337}  = {'Delta_S_glu',           0.25,         [0 1],                  [0 1], true};    
INIT{338}  = {'G_ham',                 0.65,         [0 20],                 [0 10], true};    
INIT{339}  = {'G_glu',                 0.4,          [0 20],                 [0 10], true};    
INIT{340}  = {'G_hfl',                 0.35,         [0 20],                 [0 10], true};    
INIT{341}  = {'l_off_hfl',             0.6,          [0 20],                 [0 10], true};    
INIT{342}  = {'G_hamhfl',              4,            [0 20],                 [0 10], true};    
INIT{343}  = {'l_off_ham',             0.85,         [0 20],                 [0 10], true};    
INIT{344}  = {'k_lean',                1.15,         [0 20],                 [0 10], true};    
INIT{345}  = {'k_p_hfl',               1.91,         [0 20],                 [0 10], true};    
INIT{346}  = {'k_d_hfl',               0.25,         [0 20],                 [0 10], true};    
INIT{347}  = {'k_p_ham',               1.91,         [0 20],                 [0 10], true};    
INIT{348}  = {'k_d_ham',               0.25,         [0 20],                 [0 10], true};    
INIT{349}  = {'Delta_S_hfl',           0.25,         [0 1],                  [0 1], true};    
INIT{350}  = {'Delta_S_rf',            0.25,         [0 1],                  [0 1], true};    
INIT{351}  = {'Delta_S_vas',           0.25,         [0 1],                  [0 1], true};  

% Additional values for stance prep 
INIT{352}  = {'k_p_glu_sp',     1.0,    [0 20],     [0 10], true};    
INIT{353}  = {'k_d_glu_sp',     0.2,    [0 20],     [0 10], true};    
INIT{354}  = {'k_p_hfl_sp',     1.0,    [0 20],     [0 10], true};    
INIT{355}  = {'k_d_hfl_sp',     0.2,    [0 20],     [0 10], true};    
INIT{356}  = {'k_p_vas_sp',     1.0,    [0 20],     [0 10], true};    
INIT{357}  = {'k_d_vas_sp',     0.2,    [0 20],     [0 10], true};    
INIT{358}  = {'htheta_ref_sp',  2.7,    [-pi pi],   [0 1],  true};    
INIT{359}  = {'phi_ref_sp',     3.0,    [-pi pi],   [0 1],  true};    
INIT{360}  = {'sp_threshold',  -0.15,   [-20 20],   [0 1],  true};    
INIT{362}  = {'simbicon_cd',    0.5,    [-20 20],   [0 2],  true};    
INIT{363}  = {'simbicon_cv',    0.2,    [-20 20],   [0 2],  true};  
 


%% Export data to files
% -------------------------------------------------------------
n = size(INIT,2);
for i = 1:N
    if isempty(INIT{i})
        INIT{i} = DEFAULT;
        %INIT{i}{1} = sprintf('%sUNUSED', INIT{i}{1});
        INIT{i}{1} = 'UNUSED';
        unusedCounter(i) = 1;
    end
    
    colNames{i} = INIT{i}{1};
    values(i) = INIT{i}{2};
    bounds_min(i) = INIT{i}{3}(1);
    bounds_max(i) = INIT{i}{3}(2);
    scale_range(i) = INIT{i}{4}(1); 
    scale_offset(i) = INIT{i}{4}(2); 
    toOptimize(i) = INIT{i}{5};
    if (unusedCounter(i) == 0 && toOptimize(i) == 0)
        noOptimizeCounter(i) = 1;
    end
end

colNames2{1} = 'time'; 
for i = 1:N 
	colNames2{i+1} = colNames{i}; 
end

toOptimize = double(toOptimize);
generateMotFile([0 values], colNames2, valuesFile);
generateMotFile([0 bounds_min; 1 bounds_max], colNames2, boundsFile);
generateMotFile([0 scale_range; 1 scale_offset], colNames2, rangesFile);
generateMotFile([0 toOptimize], colNames2, toOptimizeFile);


%% Reporting
% -------------------------------------------------------------
n_opt1 = sum(toOptimize(1:100));
n_opt2 = sum(toOptimize(101:200));
n_opt3 = sum(toOptimize(201:300));
n_opt4 = sum(toOptimize(301:400));
numOptParams = sum(toOptimize);

n_noopt1 = sum(noOptimizeCounter(1:100));
n_noopt2 = sum(noOptimizeCounter(101:200));
n_noopt3 = sum(noOptimizeCounter(201:300));
n_noopt4 = sum(noOptimizeCounter(301:400));
numNoOptParams = sum(noOptimizeCounter);

n_unused1 = sum(unusedCounter(1:100));
n_unused2 = sum(unusedCounter(101:200));
n_unused3 = sum(unusedCounter(201:300));
n_unused4 = sum(unusedCounter(301:400));
numUnusedParams = sum(unusedCounter);

infoFile = 'optimization_param_info.txt';
if (exist(infoFile, 'file'))
    delete(infoFile);
end
diary('optimization_param_info.txt');

fprintf('======================================================================\n');
fprintf('SP + Balance Feedback\n');
fprintf('======================================================================\n');
fprintf('Number of parameters (INITIAL COORDINATE STATES)    [opt/notopt/unused]: [%d / %d / %d]\n', n_opt1, n_noopt1, n_unused1);
fprintf('Number of parameters (INITIAL MTU STATES)           [opt/notopt/unused]: [%d / %d / %d]\n', n_opt2, n_noopt2, n_unused2);
fprintf('Number of parameters (TORQUE CONTROLLER PARAMETERS) [opt/notopt/unused]: [%d / %d / %d]\n', n_opt3, n_noopt3, n_unused3);
fprintf('Number of parameters (MTU CONTROLLER PARAMETERS)    [opt/notopt/unused]: [%d / %d / %d]\n', n_opt4, n_noopt4, n_unused4);
fprintf('======================================================================\n');
fprintf('Number of parameters (TOTAL)                        [opt/notopt/unused]: [%d / %d / %d]\n', numOptParams, numNoOptParams, numUnusedParams);
fprintf('======================================================================\n');
diary off

