#ifndef SIMPLEMUSCLE_H_
#define SIMPLEMUSCLE_H_

//============================================================================
// INCLUDES
//============================================================================
#include <OpenSim/Simulation/Model/Actuator.h>
#define MUSCLE_DENSITY 1060 
#define TENDON_COMP 0.04

namespace OpenSim {

struct MuscVars {
    double timeLastUpdated;
    double l_ce;          // STATE: fiber length
    double a;             // STATE: activation
    double u;             // INPUT: excitation
    double theta[2];      // gen coords
    double thetadot[2];   // gen speeds
    double momArm[2];     // muscle moment arm
    double mom[2];        // muscle moment
    double l_mtu;         // MTU length
    double l_se;          // tendon length
    double F_se;          // tendon force
    double F_be;          // buffer elasticity
    double F_pe;          // passive force
    double fl;            // force length
    double fv;            // force velocity
    double fvInv;         // inverse force velocity
    double metabolicrate; // metabolic rate
};


//=============================================================================
//=============================================================================

class SimpleMuscle : public OpenSim::Actuator  
{
    OpenSim_DECLARE_CONCRETE_OBJECT(SimpleMuscle, Actuator);
public:
//=======================================================================
// PROPERTIES
//=======================================================================
/** @name Property declarations
    These are the serializable properties associated with this class. **/
/**@{**/
    OpenSim_DECLARE_PROPERTY(Lopt, double,
	        "Optimal length of the muscle fibers.");
    OpenSim_DECLARE_PROPERTY(Fmax, double,
	        "Maximum isometric strength of the muscle fibers.");
    OpenSim_DECLARE_PROPERTY(Lslack, double,
	        "Tendon slack length.");
    OpenSim_DECLARE_PROPERTY(Vmax, double,
	        "Maximum shortening velocity.");
/**@}**/


//=============================================================================
// DATA
//=============================================================================
protected:
    int _numDOF;
    SimTK::Array_<Coordinate*> _coords;

	double _r_0;        // momarm1 params (G&H)
	double _roh; 
	double _phi_ref; 
	double _phi_max;
	 
	double _r_02;       // momarm2 params (G&H)
	double _roh2; 
	double _phi_ref2; 
	double _phi_max2;

    double _a0, _a1, _a2, _a3;  // momarm1 params (polynomial)
    double _a4, _a5, _a6;       // momarm2 params (polynomial)
	
	//double _l_slack;    // muscle params
	//double _Fmax;
	//double _v_max; 
	
	double _epsilon_ref;    // muscle shape params
	double _w;
	double _K; 
	double _N;

	double _Tact;              // activation time constant
    double _Tdeact;            // deactivation time constant

	double _typeI_portion;  // metabolic params
    double _csa;  
    double _mass;

    mutable MuscVars muscVars;      // state dependant muscle variables;

    SimTK::Array_<SimTK::MobilizedBodyIndex> _drawBody;
    SimTK::Array_<SimTK::Vec3> _drawPoint;

	double _signToGF[2]; 

	


//=============================================================================
// METHODS
//=============================================================================
public:
	// Added by JW
	void printInnerValues();

    //------------------------------------------------------------------------
    // Constructor(s) and Setup
    //------------------------------------------------------------------------
    SimpleMuscle();
    SimpleMuscle(const std::string& name);
    

    //------------------------------------------------------------------------
    // Computation
    //------------------------------------------------------------------------
    void setAttachmentParams1(double r_0, double phi_max, double phi_ref, double roh);  // (G&H)
    void setAttachmentParams2(double r_0, double phi_max, double phi_ref, double roh);  // (G&H)
#ifdef POLYMOMARMS
    void setAttachmentParamsPoly1(double a0, double a1, double a2, double a3);   // polynomial
    void setAttachmentParamsPoly2(double a4, double a5, double a6);   // polynomial
#endif
    void setMuscleParams(double csa, double eps_ref, double typeI = 0.5);
    void initStateFromProperties(SimTK::State& s) const;
    void setCoordinates(int numDOF, std::string coord1, std::string coord2);

    void updateMuscleVars(const SimTK::State& s) const;
    double getFbe() const;
	double getFpe() const;
	double getFse() const;
	double getInvfv() const; 
	double getfv( double vce ) const;  // for debugging 
	double getfl(const SimTK::State& s) const; 
    double getMass() const {return _mass; }

    void updateMTULength() const; 
	void updateMomentArms() const;
    double sgn(double num) const;
    int getNumDOF() const {return _numDOF;}

    double getActivationHeatRate() const; 
	double getMaintenanceHeatRate() const; 

    const MuscVars& getMuscVars() {return muscVars;}
    const SimTK::Array_<Coordinate*>& getCoords() {return _coords;}
    double getMomArmSign(int index) {return _signToGF[index];}

    void addDrawPoint(SimTK::MobilizedBodyIndex b, SimTK::Vec3 p);

	void updateMetabolicRate() const; 

    // OpenSim::Force interface
    void computeForce(const SimTK::State& s, 
                              SimTK::Vector_<SimTK::SpatialVec>& bodyForces, 
                              SimTK::Vector& generalizedForces) const OVERRIDE_11;

    // OpenSim::Actuator interface
    double computeActuation(const SimTK::State& s) const OVERRIDE_11;

    // OpenSim::Force interface
    double computePotentialEnergy(const SimTK::State& s) const OVERRIDE_11;

    /** @returns A string array of the state variable names. */
    Array<std::string> getStateVariableNames() const FINAL_11;

    /** @param stateVariableName The name of the state varaible in question.
        @returns The system index of the state variable in question. */
    SimTK::SystemYIndex getStateVariableSystemIndex(
        const std::string &stateVariableName) const FINAL_11;




    //------------------------------------------------------------------------
    // Reporting
    //------------------------------------------------------------------------
    // Provide name(s) of the quantities (column labels) of the force value(s) 
    // to be reported.
    Array<std::string> getRecordLabels() const OVERRIDE_11;
    
    // Provide the value(s) to be reported that correspond to the labels.
    Array<double> getRecordValues(const SimTK::State& s) const OVERRIDE_11;


	// Added by JW
	void setMuscleVars(MuscVars mv) {muscVars = mv;}


private:
    void setNull();
    void constructProperties();

protected:
    void connectToModel(Model& aModel) OVERRIDE_11;
    void addToSystem(SimTK::MultibodySystem& system) const OVERRIDE_11;
    SimTK::Vector computeStateVariableDerivatives(const SimTK::State& s) const OVERRIDE_11;
    void generateDecorations(bool fixed, const ModelDisplayHints& hints, 
        const SimTK::State& s, 
        SimTK::Array_<SimTK::DecorativeGeometry>& geometry) const OVERRIDE_11;

    
//=============================================================================
};	// END of class SimpleMuscle
//=============================================================================
//=============================================================================

} // end of namespace OpenSim

#endif // PREDICTIVESIM_COORDINATE_LIMIT_FORCE_GEYER_HERR_2010_H_
